<?php
	include("seguridad.php");
	if($privilegios < 2){
		header("location: ../");
			
		exit();
		}

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>BANCO RIPLEY - Gestión SIR</title>
	
	<!-- Icono BR -->
	<link href="favicon.ico" type="image/x-icon" rel="shortcut icon" />
	
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/small-business.css" rel="stylesheet">

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="http://www.bancoripley.cl/banco_ripley/index.html">
                    <img src="Banco-ripley.jpg" alt="">
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="administrador.php">Inicio</a>
                    </li>
                    <li>
                        <a href="audiencias/inicio.php">Audiencias</a>
                    </li>
                    <li>
                        <a href="seguimiento clientes/inicio.php">Seguimiento de clientes</a>
                    </li>
					<li>
                        <a href="abogados/inicio.php">Abogados</a>
                    </li>

					
                </ul>
				<ul class="nav navbar-nav navbar-right">
				
					<li>
                        <a href="index.php" <font color= "#FF0101" > <b>Salir</b> </font></a>
                    </li>
				</ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

        <!-- Page Content -->
    <div class="container">

        <!-- Heading Row -->
        <div class="row">
            <div class="col-md-8">
                <img class="img-responsive img-rounded" src="poder-judicial.jpg" alt="">
            </div>
            <!-- /.col-md-8 -->
            <div class="col-md-4">
                <h1>Gestión de casos SIR</h1>
                <p>Mediante esta plataforma, Banco Ripley gestiona aquellos clientes que se han acogido a la Ley 20.720 a través de la Superintendencia de Insolvencia y Reemprendimiento. Aquí encontrará todos los casos, su avance e información.<br><br> 
				Esta base se alimenta principalmente del boletín concursal que dispone la SIR en su página web, a la que puede acceder haciendo click a continuación.</p>
                <a class="btn btn-primary btn-lg" href="http://www.boletinconcursal.cl/boletin/procedimientos">Ir al Boletín Concursal</a>
            </div>
            <!-- /.col-md-4 -->
        </div>
        <!-- /.row -->

        <hr>

        <!-- Content Row -->
        <div class="row">
            <div class="col-md-4">
                <h2>Renegociación de Personas</h2>
                <p>La Ley de Insolvencia y Reemprendimiento le permite a personas como la señora Ana María, secretaria y jefa de hogar, renegociar sus deudas con sus acreedores en una escenario propicio.
					La Ley 20.720 crea una herramienta útil para que ella pueda contar ovejas al dormir y no deudas.</p>
                <a class="btn btn-default" href="http://www.superir.gob.cl/ley-n-20-720/">Mas información</a>
            </div>
            <!-- /.col-md-4 -->
            <div class="col-md-4">
                <h2>Liquidación de Bienes de Personas</h2>
                <p>La Ley de Insolvencia y Reemprendimiento permite a personas como Germán, diseñador industrial, sin trabajo, cumplir con las cuotas del auto, el arriendo de su departamento y sus compromisos comerciales.
					La Ley 20.720 crea la instancia propicia para saldar sus deudas en menos tiempo y así evitar que sus antecedentes comerciales se transformen en un problema para encontrar trabajo.</p>
                <a class="btn btn-default" href="http://www.superir.gob.cl/ley-n-20-720/">Mas información</a>
            </div>
            <!-- /.col-md-4 -->
            <div class="col-md-4">
                <h2>Reorganización o Liquidación de Empresas</h2>
                <p>La Ley de Insolvencia y Reemprendimiento permite a empresarios no cerrar un negocio cuando es viable o bien cerrarlo cuando no lo es.
					Con la Ley 20.720 podrá reorganizar los pasivos y activos de su empresa, ponerse al día y seguir adelante para el primer caso, o liquidar su empresa en poco tiempo para el segundo.</p>
                <a class="btn btn-default" href="http://www.superir.gob.cl/ley-n-20-720/">Mas información</a>
            </div>
            <!-- /.col-md-4 -->
        </div>
        <!-- /.row -->

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Banco Ripley 2015</p>
                </div>
            </div>
        </footer>

    </div>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>