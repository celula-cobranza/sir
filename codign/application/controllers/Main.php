<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Main extends CI_Controller {
 
function __construct()
{
        parent::__construct();
 
/* Standard Libraries of codeigniter are required */
$this->load->database();
$this->load->helper('url');
/* ------------------ */ 
 
$this->load->library('grocery_CRUD');
 
}
 
public function index()
{
echo "<h1>Welcome to the world of Codeigniter</h1>";//Just an example to ensure that we get into the function
die();
}
 
public function renegociaciones()
{
$this->grocery_crud->set_table('vrenegociaciones');
$this->grocery_crud->set_primary_key('RUT_CLIENTE');
$output = $this->grocery_crud->render();

 
$this->_example_output($output);        
}

public function clientes()
{
$this->grocery_crud->set_table('clientes');
$output = $this->grocery_crud->render();
 
$this->_example_output2($output);        
}

public function vaudiencias()
{
$this->grocery_crud->set_table('vaudiencias');
$this->grocery_crud->set_primary_key('RUT_CLIENTE');
$output = $this->grocery_crud->render();
 
$this->_example_output3($output);        
}

public function abogados()
{
$this->grocery_crud->set_table('abogados');
$output = $this->grocery_crud->render();
 
$this->_example_output4($output);        
}






function _example_output($output = null)
 
{
$this->load->view('renegociaciones.php',$output);    
}

function _example_output2($output = null)
 
{
$this->load->view('clientes.php',$output);    
}

function _example_output3($output = null)
 
{
$this->load->view('vaudiencias.php',$output);    
}

function _example_output4($output = null)
 
{
$this->load->view('abogados.php',$output);    
}

}
 
/* End of file Main.php */
/* Location: ./application/controllers/Main.php */