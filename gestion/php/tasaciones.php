﻿<?php
	include("seguridad.php");
	if($privilegios < 2){
		header("location: ../");
			
		exit();
		}

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Modern Business - Start Bootstrap Template</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/modern-business.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="http://192.168.33.171/gestion/php/administrador.php">BANCO RIPLEY</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="services.html" class="dropdown-toggle" data-toggle="dropdown">Gestiones Internas <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="atencionpublico.php">Atención Publico</a>
                            </li>
							
                            <li>
                                <a href="levantamiento.html">Levantamientos del Área</a>
                            </li>

                        </ul>
                    </li>
					<li class="dropdown">
                        <a href="services.html" class="dropdown-toggle" data-toggle="dropdown">Gastos <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="reportes_area/index.html">Panel Control Gastos</a>
                            </li>							
							<li>
                                <a href="reportes_area/gastos.html">Panel Estadisticas Gastos</a>
                            </li>
                        </ul>
                    </li>
					<li class="dropdown">
                        <a href="services.html" class="dropdown-toggle" data-toggle="dropdown">Judicial <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="#">En construcción</a>
                            </li>

                        </ul>
                    </li>
					<li class="dropdown">
                        <a href="services.html" class="dropdown-toggle" data-toggle="dropdown">Cierres Mensuales <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="#">En construccion</a>
                            </li>

                        </ul>
                    </li>
					
					<li>
                        <a href="index.php" <font color= "#FF0101" > Salir </font></a>
                    </li>
					
					                            
                </ul>
                
                
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

        <!--    ESTILO GENERAL   -->
        <link type="text/css" href="css/style.css" rel="stylesheet" />
        <!--    ESTILO GENERAL    -->
        <!--    JQUERY   -->
        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" language="javascript" src="js/funciones.js"></script>
        <!--    JQUERY    -->
        <!--    FORMATO DE TABLAS    -->
        <link type="text/css" href="css/demo_table.css" rel="stylesheet" />
        <script type="text/javascript" language="javascript" src="js/jquery.dataTables.js"></script>
        <!--    FORMATO DE TABLAS    -->

    </head>
    <body>
    <header id="titulo">

        <h3>ATENCIONES DE PUBLICO AREA CONTROL DE GESTION E HIPOTECARIO</h3>
				
    </header>
	<br>

		<head>
			<title> Formulario de inscripcion</title>
		</head>
		
		<body>
		<!-- formulario que recibe datos y los manda a recibetados.php para poder guardar la informacion en una base de datos -->
		<center>
		<form action="insertar/recibedatos.php" method="post" name="frm">
		<input type="date" name="Fecha_Atencion" placeholder="Fecha_Atencion">
		<input type ="time" name="Hora_Inicio" placeholder="Hora_Inicio">
		<select class="selectpicker" name="Ejecutivo" data-style="btn-primary">
		<option>Seleccione Ejecutivo</option>
		<option>CCANCINO</option>
		<option>JOROSTICA</option>
		<option>BTORRES</option>
		<option>MSOTO</option>
		<option>NANDRADRE</option>
		<option>FRODRIGUEZ</option>
		<option>MJIMENEZ</option>
		</select>
		
		<input type ="text" name="Rut_Cliente" placeholder="Rut Cliente">
		<input type ="text" name="Operacion" placeholder="Operacion">
		
		<select class="selectpicker" name="Requerimiento" data-style="btn-primary">
		<option>Seleccione Requerimiento</option>
		<option>Certificado Deuda</option>
		<option>Convenio</option>
		<option>Cupon de Pago</option>
		<option>Cuponeras</option>
		<option>Desbloqueos</option>
		<option>Devolución Corfo</option>
		<option>Otros</option>
		<option>Pago Mora</option>
		<option>Reclamo</option>
		<option>Renegociacion</option>
		<option>Seguros</option>
		<option>Solicitud Alzamiento</option>
		<option>Solicitud Condonación</option>
		<option>Solicitud Prepagos</option>
		</select>
		
		<select class="selectpicker" name="Tipo" data-style="btn-primary">
		<option>Seleccione Tipo</option>
		<option>Consulta</option>
		<option>Solicitud</option>

		</select>
		
		<input type ="text" name="Comentario" placeholder="Comentario">
		<input type ="text" name="Fono_Cliente" placeholder="Fono Cliente">
		<input type ="time" name="hora_Termino" placeholder="Hora Termino">
		
		<!--input type ="submit" name="submit" value="Guardar información"-->
		
		<input type="button" value="Guardar información" onclick="valida_envia()" />
			
			<!--y ahora pongo el script, que ha de hacer el trabajo pesado
				este lo podemos copiar en cualquier parte de la página web
				ya sea entre <body> y </body> o entre <head> y </head>-->
			
			<script language="JavaScript" type="text/JavaScript">

			function valida_envia(){
			if(document.frm.Fecha_Atencion.value == "") {
			alert("Ingrese fecha de atencion");
			document.frm.Fecha_Atencion.focus();
			return 0;
			}

			if(document.frm.Hora_Inicio.value == "") {
			alert("Ingrese hora de inicio de la atención");
			document.frm.Hora_Inicio.focus();
			return 0;
			}


			if(document.frm.Ejecutivo.value == "Seleccione Ejecutivo") {
			alert("Seleccione Ejecutivo");
			document.frm.Ejecutivo.focus();
			return 0;
			}

			if(document.frm.Rut_Cliente.value == "") {
			alert("Ingrese rut del cliente");
			document.frm.Rut_Cliente.focus();
			return 0;
			}

			if(document.frm.Operacion.value == "") {
			alert("Ingrese operacion del cliente");
			document.frm.Operacion.focus();
			return 0;
			}

			if(document.frm.Requerimiento.value == "Seleccione Requerimiento") {
			alert("Ingrese el requerimiento");
			document.frm.Requerimiento.focus();
			return 0;
			}
			
			if(document.frm.Requerimiento.value == "Otros" && document.frm.Comentario.value == "") {
			alert("Ingrese un comentario para detallar Otro requerimiento");
			document.frm.Comentario.focus();
			return 0;
			}
			
			if(document.frm.Tipo.value == "Seleccione Tipo") {
			alert("Ingrese el tipo de requerimiento");
			document.frm.Tipo.focus();
			return 0;
			}

			if(document.frm.hora_Termino.value == "") {
			alert("Ingrese hora de termino de la atencion");
			document.frm.hora_Termino.focus();
			return 0;
			}
			
			if(document.frm.hora_Termino.value < document.frm.Hora_Inicio.value) {
			alert("La hora de termino no puede ser mas temprana a la de inicio de la atencion");
			document.frm.hora_Termino.focus();
			return 0;
			}
			
			document.frm.submit(); 
			return true;
			}
			</script>		

			
					
		</form>
		</center>
		
		</body>
    <article id="contenido"></article>
    <footer>
        AREA<a target="_blank"href="http://www.bancoripley.cl/"> CONTROL GESTION E HIPOTECARIO BANCO RIPLEY</a>
		<p>Copyright &copy;  2015</p>
    </footer>
	

	
	<!-- jQuery -->
    <!--<script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
	
</body>
</html>



