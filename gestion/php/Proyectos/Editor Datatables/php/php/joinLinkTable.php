<?php

// DataTables PHP library
include( "../../php/DataTables.php" );

// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Join,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;


/*
 * Example PHP implementation used for the joinLinkTable.html example
 */

Editor::inst( $db, 'audiencias','id_audiencia' )
	->field( 
		Field::inst( 'audiencias.fecha_audiencia' ),
		Field::inst( 'audiencias.rut_cliente' )
			->options( 'clientes', 'rut_cliente', 'nombre_cliente' ),
		Field::inst( 'clientes.nombre_cliente' )
				
	)
	->leftJoin( 'clientes',     'clientes.rut_cliente',          '=', 'audiencias.rut_cliente' )
	->process($_POST)
	->json();
