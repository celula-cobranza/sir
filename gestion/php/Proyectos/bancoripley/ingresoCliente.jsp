<%@page contentType="text/html" pageEncoding="utf-8"%>
<%@page errorPage="paginaError.jsp" %>
<%@page import ="java.util.*"%>
<%@page import ="java.util.Collections"%>
<%@page import ="beans.*"%>
<jsp:useBean id="datoscliente" scope="request"   class="beans.clienteDAO" />
<jsp:useBean id='datospropiedad' scope='request'   class='beans.propiedadDAO' />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<%  HttpSession hs = request.getSession(true); 
        
if (hs.getAttribute("nombreUsuario")==null)
{
response.sendRedirect("Login.html");                              
}
%>

<title>Banco Ripley</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
</head>

<body>
	
<div id="container">
	<ul> 
            <jsp:include page="menu.jsp" />
                <label><div id="logo"><a href="ingresoCliente.jsp"><img src="images/logoripley.png" width="226" height="50" alt="" /></a></div>                      
        </ul>   
  <br />
  <!-- cierre del "h_navcontainer" -->
  <div id="page_top">

  </div>
  <div id="page" >
    <h1>Ingreso Cliente</h1>     
    <!--Estilo de Notifiacion de ERROR -->
    <div id="drawer">Por favor ingrese correctamente los campos marcados en <samp style="color:red">Rojo.</samp></div>
    <!-- El Formulario -->
    <form name="SignupForm" id="SignupForm" action="ingreso" method="post">
      <div id="wizard">
        <ul id="status">
          <li class="active">1. Cliente</li>
          <li>2. Propiedad</li>
          <li>3. Propiedad/Tasacion</li>
          <li>4. Tasacion/Credito</li>
          <li>5. Credito</li>
        </ul>
        <div class="items" >
          <!-- Pagina de Ingreso Cliente -->
          <div class="page">
            <h2> <strong>Paso 1: </strong> Ingrese Informacion del Cliente</h2>          
            <ul>
              <!-- Ingreso de Rut, Nombre Completo, Telefono, etc... datos del cliente -->
              <li class="required double">
                  <label> Rut:<span>*</span><br />
                      <input type="text" class="text" name="rut" maxlength="10" onblur="esrut(document.getElementById('rut').value)" />                           
                  <em>Ingrese el Rut del Cliente sin "-".</em>       
                  </label>
                  <label> Nombre:<span>*</span><br />
                      <input type="text" class="text" name="nombre" maxlength="50" />
                  <em>Ingrese el Nombre completo del Cliente.</em>
                </label>
                <label>  Telefono:<span>*</span><br />
                    <input type="text" class="text" name="telefono" id="telefono" maxlength="10"/>
                  <em>Ingreso el numero de Telefono (Red Fija).</em> 
                 </label>
                  <label>  Correo: <span>*</span><br />
                  <input type="text" class="text" name="correo" maxlength="30"/>
                  <em>Direccion de Correo Electronico.</em> 
                  </label>         
                  <label>  Sexo: <span>*</span><br />
                      <label>
				<input type="radio"  value="M" name="sexo" checked="true" validate="required:true"/>
				Masculino
			</label>
                     <label>
				<input type="radio"  value="F" name="sexo" class="error"/>
				Femenino
			</label>
                  </label>
                      <label> Dirección Cliente:<span>*</span><br />
                         <input type="text" class="text" name="direccion" id="direccion" maxlength="50" />
                  <em>Ingrese la Direccion del Cliente.</em>
                </label>
                  <label> Estado Civil <span>*</span><br />
                   <select name="civil" class="civil" validate="required:true">	
                  <option value>- Seleccione el Estado Civil -</option>
		         <option value="soltero">SOLTERO/A</option>
		         <option value="casado">CASADO/A</option>
		         <option value="separado">SEPARADO/A</option>
		         <option value="viudo">VIUDO/A</option>
                 </select>
                 </label>
                  </li>
                 <!-- *******************Aqui se importa el jsp regioncliente***************** -->
                 <jsp:include page="regioncliente.jsp" />
                 <!-- ************************************************************************ -->                                         
              <li class="clearfix">
               <button type="button" class="next right">Siguiente&raquo;</button>
              </li>
              <!--Se cierra el "li de ingreso propiedad..."-->
            </ul>
          </div>
          <!-- Se cierra el " Div de Ingreso Cliente" -->
          <!-- Pagina 2 de Ingreso Propiedad -->
          <div class="page">
            <h2> <strong>Paso 2: </strong> Ingrese Informacion Propiedad</h2>
            <ul>
              <!-- Ingreso Propiedad -->
              <li class="required double">
                <label> Rol : <span>*</span><br />
                  <input type="text" class="text" name="rol" maxlength="20" />
                  <em>Ingrese el Rol de la Propiedad.</em> 
                  </label>
                  <label> Tipo:<span>*</span><br />
                  <input type="text" class="text" name="tipo" maxlength="20"/>
                  <em>Ingrese el Tipo de Propiedad.</em>
                </label>
                <label>  Estado:<span>*</span><br />
                  <input type="text" class="text" name="estado" maxlength="20" />
                  <em>Ingreso el estado de la Propiedad.</em> 
                 </label>
                  <label>  Superficie del Terreno: <span>*</span><br />
                  <input type="text" class="text" name="supte" maxlength="5" />
                  <em>Ingrese la Superficie del TERRENO de la Propiedad.</em> 
                  </label>         
                  <label>  Superficie de la Construccion: <span>*</span><br />
                  <input type="text" class="text" name="supconst" maxlength="5" />
                  <em>Ingrese la Superficie de la Construccion.</em> 
                  </label>              
                 <label> Direccion Propiedad:<span>*</span><br />
                  <input type="text" class="text" name="ubicacion" maxlength="50"/>
                  <em>Ingrese la Direccion de la Propiedad.</em>
                </label>
              </li>
                 <!-- *******************Aqui se importa el jsp regionpropiedad***************** -->
                 <jsp:include page="regionpropiedad.jsp" />
              <!-- ************************************************************************++++++ -->
                 <br/>
                 <!-- INGRESO DEL CBR -->
                 <h2>Ingrese informacion del Documento CBR</h2>
                 <li class="required double">           
                 	<label> Numero de Repertorio : <span>*</span><br />
                  <input type="text" class="text" name="nrptio" maxlength="20"/>
                  <em>Ingrese el Numero de Repertorio.</em> 
                  </label>
                   	<label> Fojas : <span>*</span><br />
                  <input type="text" class="text" name="fojas" id="fojas" maxlength="20" />
                  <em>Ingrese  Foja.</em> 
                   </label>                                  
                    <label> Numero de Fojas : <span>*</span><br />
                  <input type="text" class="text" name="nfojas" id="nfojas" maxlength="20" />
                  <em>Ingrese el Numero de Fojas.</em> 
                  </label> 
                     <label> Fecha CBR : <span>*</span><br />
                  <input type="text"  class="text"  name="fcbr" id="fcbr"/>
                  <em>Ingrese la Fecha del CBR.</em> 
                  </label>                         
                      <label> Direccion CBR : <span>*</span><br />
                    <input type="text"  class="text"  name="dircbr" id="dircbr" maxlength="50"/>
                  <em>Ingrese la direccion Fecha del CBR.</em> 
                  </label> 
                        <label>  Escritura: <span>*</span><br />
                      <label>
				<input type="radio"  value="S" name="aplica" checked="true" validate="required:true"/>
				SI
			</label>
                     <label>
				<input type="radio"  value="N" name="aplica"/>
				NO
			</label>
                  </label>
                 </li>                     
                 <!-- Se cierra el "li double de Informacion Propiedad"-->
              <li class="clearfix">
                <button type="button" class="prev" style="float:left">&laquo; Regresar</button>
                <button type="button" class="next right">Siguiente &raquo;</button>
              </li>
              <br clear="all" />
            </ul>
          </div>
          <!-- Se cierra el "div de la informacion CLiente " -->
          <div class="page">
            <h2> <strong>Paso 3: </strong> Ingrese Informacion Tasacion</h2>          
            <ul>       
              <!-- ***************COMPRA VENTA ***************-->                                 	
                 	<h2>Informacion Tasacion</h2>
                 <li class="required double"> 
                  <label> Empresa : <span>*</span><br />
                  <input type="text" class="text" name="empresa" maxlength="30" />
                  <em>Ingrese el Nombre de la Empresa.</em> 
                   </label>
                   <label> Tasador : <span>*</span><br />
                  <input type="text" class="text" name="tsdador" maxlength="30" />
                  <em>Ingrese el Nombre del Tasador.</em> 
                   </label>                    
                   <label> Valor UF : <span>*</span><br />
                  <input type="text" class="text" name="vuf" />
                  <em>Ingrese el valor UF.</em> 
                   </label>   
                   <label> Valor $: <span>*</span><br />
                  <input type="text" class="text" name="vpeso" />
                  <em>Ingrese Valor $.</em> 
                  </label>    
                  <label> Valor Liquidacion: <span>*</span><br />
                  <input type="text" class="text" name="vliquidez" id="vliquidez" maxlength="20" />
                  <em>Ingrese valor Liquidacion.</em> 
                   </label> 
                   <label> Fecha Tasacion: <span>*</span><br />
                  <input type="text" class="text" name="ftasa" id="ftasa" />
                  <em>Ingrese la fecha.</em> 
                   </label>
                   <label> Descripcion: <span>*</span><br />
                  <textarea name="descripcion" id="descripcion"  style="width: 370px; height: 56px;" dir="ltr" maxlength="140"></textarea>
                  <em>Descripcion del Estado de la Propiedad de la Tasacion.</em> 
                   </label>                                                              
                 	</li>           
              <li class="clearfix">
                <button type="button" class="prev" style="float:left">&laquo; Regresar</button>
                <button type="button" class="next right">Siguiente &raquo;</button>
              </li>
              <br clear="all" />
            </ul>
          </div>
          <!-- Se cierra el "Div de Informacion de la Tasacion" -->   
            <div class="page">
            <h2> <strong>Paso 3: </strong> Ingrese Informacion Tasacion</h2>          
            <ul>       
              <!-- ***************COMPRA VENTA ***************-->               
                  <h2>Compra venta</h2>
                  <li class="required double"> 
                  <label> Numero de Repertorio : <span>*</span><br />
                  <input type="text" class="text" name="nrepertorio" id="nrepertorio" maxlength="20" />
                  <em>Ingrese el Numero de Repertorio.</em> 
                   </label>
                   <label> Notaria : <span>*</span><br />
                  <input type="text" class="text" name="notaria" maxlength="20" />
                  <em>Ingrese el Nombre de la Notaria.</em> 
                   </label>                      
                  <label> Direccion Notaria: <span>*</span><br />
                  <input type="text" class="text" name="dircv" maxlength="20"/>
                  <em>Ingrese Direccion Notaria.</em> 
                   </label>
                   <label> Fecha Compra Venta: <span>*</span><br />
                  <input type="text" class="text" name="fecha" id="fecha" />
                  <em>Ingrese Fecha Compra Venta.</em> 
                  </label>    
                 	</li>
                 	<!-- *******SE CIERRA EL "li DEL COMPRAVENTA"****** -->
                 	<!-- *******INFORMACION TASACION"**************** -->               	
                 	<h2>Informacion del Credito</h2>
                 <li class="required double"> 
                  <label> Precio Venta : <span>*</span><br />
                  <input type="text" class="text" name="pventa" maxlength="20" />
                  <em>Ingrese el Precio Venta.</em> 
                   </label>
                   <label> Monto Credito Hipotecario : <span>*</span><br />
                  <input type="text" class="text" name="chipotecario" maxlength="20" />
                  <em>Ingrese el monto Credito Hipotecario.</em> 
                   </label>   
                   <label>Numero de Operacion del Credito Hipteacario  <span>*</span><br />
                  <input type="text" class="text" name="ophip" maxlength="20" />
                  <em>Ingrese el numero Operacion del Credito Hipteacario.</em> 
                  </label>    
                  <label> Estado Hipotecario: <span>*</span><br />
                  <select name="esthipo" class="esthipo" validate="required:true">	
                 <option value>Seleccione el Estado Hipotecario</option>
                 <option value="NO TIENE">NO TIENE</option>
                 <option value="VIGENTE">VIGENTE</option>
                 <option value="VENCIDO">VENCIDO</option>
                 <option value="CASTIGADO">CASTIGADO</option>
                 </select>
                  <em>Seleccione el Estado del credito Hipotecario.</em> 
                   </label> 
                   <label> Monto Credito Complementario: <span>*</span><br />
                  <input type="text" class="text" name="ccomplementario" maxlength="20" />
                  <em>Ingrese el monto del Credito Complementario.</em> 
                   </label>
                  <label> Numero de Operacion del Credito Complementario: <span>*</span><br />
                  <input type="text" class="text" name="opcomp" maxlength="20"/>
                  <em>Ingrese el numero Operacion del Credito Complementario .</em> 
                  </label>  
                   <label> Estado del Credito Complementario: <span>*</span><br />
                   <select name="estcompl" class="estcompl" validate="required:true">	
                   <option value>Seleccione el Estado Complementario</option>
                   <option value="NO TIENE">NO TIENE</option>
                   <option value="VIGENTE">VIGENTE</option>
                   <option value="VENCIDO">VENCIDO</option>
                   <option value="CASTIGADO">CASTIGADO</option>                
                 </select>
                  <em>Seleccione el Estado del Credito Complementario.</em> 
                  </label>  
                  <!-- ******** Se cierra el "li de la informacion de la Tasacion ************-->	                                                             
                 </li>           
              <li class="clearfix">
                <button type="button" class="prev" style="float:left">&laquo; Regresar</button>
                <button type="button" class="next right">Siguiente &raquo;</button>
              </li>
              <br clear="all" />
            </ul>
          </div>            
          <!--************* Se cierra el "DIV del Credito" ***************-->
        <div class="page">
            <h2> <strong>Paso 5: </strong> Ingrese Informacion del Credito</h2>          
            <ul>       
              <!-- ***************INFORMACION CREDITO***************-->               
                  <h2>Relacion Tasacion</h2>
                  <li class="required double"> 
                  <label> Deuda/Precio : <span>*</span><br />
                  <input type="text" class="text" name="dprecio" maxlength="20" />
                  <em>Ingrese la Deuda Precio.</em> 
                   </label>
                   <label> Deuda/Tasacion: <span>*</span><br />
                  <input type="text" class="text" name="dtasacion" maxlength="20" />
                  <em>Ingrese Deuda Tasacion.</em> 
                   </label>   
                   <label> Total Deuda Garanatia: <span>*</span><br />
                  <input type="text" class="text" name="dgarantia" maxlength="20" />
                  <em>Ingrese el total Deuda Garantia.</em> 
                  </label>    
                  <label> Clase de Subsidio: <span>*</span><br />
                  <select name="clase" class="clase" validate="required:true">	
                   <option value>Seleccione la clase de Subsidio</option>
                          <option value="NO TIENE">NO TIENE</option>
		          <option value="ds44">DS44</option>
		          <option value="ds235">DS235</option>
		          <option value="DS62">DS62</option>
		          <option value="ds40">DS40</option>
                 </select>
                  <em>Ingrese la clase del Subsidio.</em> 
                   </label> 
                  <label> Tipo de Seguro:<span>*</span><br />
                  <select name="tiposeg" class="tiposeg" validate="required:true">	
                  <option value>Seleccione el tipo de Seguro</option>
                  <option value="NO TIENE">NO TIENE</option> 
                  <option value="REMATE">REMATE</option> 
                  </select>
                  </label> 
                  <label> Nivel del Seguro:<span>*</span><br />
                  <select name="nivel" class="nivel" validate="required:true">	
                  <option value>Seleccione el nivel del seguro</option>
                  <option value="NO TIENE">NO TIENE</option>
                  <option value="75">75%</option>
                  <option value="100">100%</option>                  
                </select>
                  </label>                                        
                 	</li> 	
              <li class="clearfix">
                <button type="button" class="prev" style="float:left">&laquo; Regresar</button>
                <button type="submit" class="next right">Ingresar Cliente &raquo;</button>
              </li>
              <br clear="all" />
            </ul>
          </div>
          <!-- ******************SE CIERRA EL DIV DE LA INFORMACION DEL CREDITO*******************************-->
        </div>
        <!--items-->
      </div>
      <!--wizard-->
    </form>
     <script src="js/fancybox/jquery.tools.min.js" type="text/javascript"></script>  
            <script type="text/javascript">
	$(document).ready(function(){
		$("#fcbr").calendarioDW();
	})
	</script>
          <script type="text/javascript">
	$(document).ready(function(){
		$("#ftasa").calendarioDW();
	})
	</script>
      <script type="text/javascript">
	$(document).ready(function(){
		$("#fecha").calendarioDW();
	})
	</script>
    <script type="text/javascript">   
     $().ready(function() {
    $("#SignupForm").validate({
		rules: {
		'rut':'required',
                'region':'required',
                'nombre':'required',
		'ciuda':'required',
		'comu':'required',
                'rol':'required',
                'tipo':'required',
                'estado':'required',
                'notaria':'required',
                'dircv':'required',
                'empresa':'required',
                'tsdador':'required',
                'sexo':'required',
                'civil':'required',
                'ciudad':'required',
                'comuna':'required',
                'fcbr':'required',
                'dircbr':'required',
                'ftasa':'required',
                'vliquidez':'required',
                'fecha':'required',
                'esthipo':'required',
                'estcompl':'required',
                'rgion':'required',
                'clase':'required',
                'tiposeg':'required',
                'nivel':'required',
                
                
		telefono: {required: true, number: true,minlength:7},
                supte: {required: true, number: true,minlength:1},
                supconst: {required: true, number: true,minlength:1},
		correo: { required: true, email: true, minlength:6},
		calle: { required: true, minlength:5},
                nrptio: {required: true,minlength:1},
                fojas: {required: true, number: true,minlength:1},
                nfojas: {required: true, number: true,minlength:1},
                nrepertorio: {required: true,minlength:1},
                vuf: {required: true, number: true,minlength:1},
                vpeso: {required: true, number: true,minlength:1},
                descripcion: { required: true, minlength:10, maxlength:140},
                dprecio: {required: true, number: true,minlength:1},
                pventa: {required: true, number: true,minlength:1},
                chipotecario: {required: true, number: true,minlength:1},
                ccomplementario: {required: true, number: true,minlength:1},
                opcomp: {required: true, number: true,minlength:1},
                dtasacion: {required: true, number: true,minlength:1},
                dgarantia: {required: true, number: true,minlength:1}, 
                ophip: {required: true, number: true,minlength:1},
                ubicacion: { required: true, minlength:5},
                direccion:{ required: true, minlength:5}
		},
                
		messages: {
                    
		'rut': 'Debe ingresar el Rut',
                'region': 'Debe ingresar la Region',
                'rgion': 'Debe ingresar la Region',
		'ciuda': 'Debe ingresar la Ciudad',
                'ciudad': 'Debe ingresar la Ciudad',          
		'comu': 'Debe ingresar la Comuna',
                'nombre': 'Debe ingresar el Nombre',
                'rol':'Ingrese el Rol',
                'tipo':'Ingrese el tipo',
                'estado':'Ingrese el estado',
                'notaria':'Ingrese la Notaria',
                'dircv':'Ingrese la Direccion',
                'empresa':'Ingrese la Empresa',
                'tsdador':'Ingrese el nombre del tasador',
                'civil':'Ingrese el Estado Civil',
                'comuna':'Ingrese la Comuna',
                'fcbr':'Ingresa fecha del CBR',
                'dircbr':'Ingresa La Dirreccion del CBR',
                'ftasa':'Ingresa el valor liquidez',
                'fecha':'Ingrese la fecha del Compra Venta',
                'esthipo':'Ingrese el Estado Hipotecario',
                'estcompl':'Ingrese el Estado Hipotecario',
                'clase':'Seleccione la clase de subsidio',
                'tiposeg':'Seleccione el tipo seguro',
                'nivel':'Seleccione el nivel',
 
                dgarantia : {
			required: "Ingrese total deuda garantia",
			number: "Este campo solo recibe Numeros",
			minlength: jQuery.format ("El numero es muy corto!")
		},
                dtasacion : {
			required: "Ingrese la Deuda de Tasacion",
			number: "Este campo solo recibe Numeros",
			minlength: jQuery.format ("El numero es muy corto!")
		},
                opcomp : {
			required: "Ingrese el numero de operacion del Credito Complementario",
			number: "Este campo solo recibe Numeros",
			minlength: jQuery.format ("El numero es muy corto!")
		},
                ccomplementario : {
			required: "Ingrese el monto del Credito Complementario",
			number: "Este campo solo recibe Numeros",
			minlength: jQuery.format ("El numero es muy corto!")
		},
                ophip : {
			required: "Ingrese el numero de operacion del Credito Hipotecario",
			number: "Este campo solo recibe Numeros",
			minlength: jQuery.format ("El numero es muy corto!")
		},
                chipotecario : {
			required: "Ingrese el Monto del Credito Hipotecario",
			number: "Este campo solo recibe Numeros",
			minlength: jQuery.format ("El numero es muy corto!")
		},
                pventa : {
			required: "Ingrese el Valor Precio Venta",
			number: "Este campo solo recibe Numeros",
			minlength: jQuery.format ("El numero es muy corto!")
		},
                dprecio : {
			required: "Ingrese el Valor Deuda/Precio",
			number: "Este campo solo recibe Numeros",
			minlength: jQuery.format ("El numero es muy corto!")
		},
                descripcion : {
			required: "Ingrese la Descripcion",
			minlength: jQuery.format ("Campo muy corto!")
		},
                vliquidez : {
			required: "Ingrese el Valor Liquidez",
			number: "Este campo solo recibe Numeros",
			minlength: jQuery.format ("El numero es muy corto!")
		},
                vpeso : {
			required: "Ingrese el Valor en $",
			number: "Este campo solo recibe Numeros",
			minlength: jQuery.format ("El numero es muy corto!")
		},
                vuf : {
			required: "Ingrese el Valor UF",
			number: "Este campo solo recibe Numeros",
			minlength: jQuery.format ("El numero es muy corto!")
		},
		nrepertorio : {
			required: "Ingrese el Numero de Repertorio del Compa Venta",
			minlength: jQuery.format ("El campo es muy corto!")
		},
                nfojas : {
			required: "Ingrese el Numero de Foja",
			number: "Este campo solo recibe Numeros",
			minlength: jQuery.format ("El numero es muy corto!")
		},
                fojas : {
			required: "Ingrese Fojas",
			number: "Este campo solo recibe Numeros",
			minlength: jQuery.format ("El numero es muy corto!")
		},
                nrptio : {
			required: "Ingrese el numero de repertorio del CBR",
			minlength: jQuery.format ("El campo es muy corto!")
		},
                supconst : {
			required: "Ingrese La Superficie de la Construccion",
			number: "Este campo solo recibe Numeros",
			minlength: jQuery.format ("El numero es muy corto!")
		},
                supte : {
			required: "Ingrese La Superficie del Terreno",
			number: "Este campo solo recibe Numeros",
			minlength: jQuery.format ("El numero es muy corto!")
		},
                
                calle : {
			required: "Ingrese la Direccion de la Propiedad",
			minlength: jQuery.format ("Campo muy corto!")
		},
		
                ubicacion : {
			required: "Ingrese la Direccion del Cliente",
			minlength: jQuery.format ("Campo muy corto!")
		},
		
		telefono: {
			required: "Ingrese el Numero de Telefono",
			number: "Este campo solo recibe Numeros",
			minlength: jQuery.format ("El numero es muy corto!")
		},
                direccion: {
			required: "Ingrese la Direccion",
			minlength: jQuery.format ("El campo es muy corto!")
		},
                
		correo: {
				required: "Ingrese Direccion de Correo Electronico",
				minlength: "Ingrese una Direccion de Correo Electronico Valida",
				email: "Ejemplo: name@domain.com",
				remote: jQuery.format("{0} is already in use")
                                
			},
       debug: true,
       /*errorElement: 'div',*/
       //errorContainer: $('#errores'),
       submitHandler: function(form){
       }			
		}
	}); 	
     var root = $("#wizard").scrollable({
        size: 1,
        clickable: false
     });
    // some variables that we need
    var api = root.scrollable(),
        drawer = $("#drawer");
    // validation logic is done inside the onBeforeSeek callback
    api.onBeforeSeek(function (event, i) {
        // we are going 1 step backwards so no need for validation
        if (api.getIndex() < i) {
            // 1. get current page
            var page = root.find(".page").eq(api.getIndex()),
                // 2. .. and all required fields inside the page
                inputs = page.find(".required :input").removeClass("error"),
                // 3. .. which are empty
                empty = inputs.filter(function () {
                    return $(this).val().replace(/\s*/g, '') == '';
                });
            // if there are empty fields, then
            if (empty.length) {
                // slide down the drawer
                drawer.slideDown(function () {
                    // colored flash effect
                    drawer.css("backgroundColor", "#229");
                    setTimeout(function () {
                        drawer.css("backgroundColor", "#fff");
                    }, 1000);
                });
                // add a CSS class name "error" for empty & required fields
                empty.addClass("error");
                // cancel seeking of the scrollable by returning false
                return false;
                // everything is good
            } else {
                // hide the drawer
                drawer.slideUp();
            }
        }
        // update status bar
        $("#status li").removeClass("active").eq(i).addClass("active");
    });
    // if tab is pressed on the next button seek to next page
    root.find("button.next").keydown(function (e) {
        if (e.keyCode == 9) {
            // seeks to next tab by executing our validation routine
            api.next();
            e.preventDefault();
        }
    });
    
});

</script>
<script src="js/validate/jquery.validate.js" type="text/javascript"></script>
<script type="text/javascript" src="js/calendario/calendario_dw.js"></script>


    <div style="clear:both"></div>     
  </div>
   <!-- close page -->
  <div id="page_bottom"></div>
  
</div>
<!-- close container -->

<!-- close footer -->
</body>
</html>