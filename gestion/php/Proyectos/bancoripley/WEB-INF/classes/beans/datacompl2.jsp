<%-- 
    Document   : datacompl
    Created on : 29-mar-2012, 8:55:14
    Author     : vriveros
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page errorPage="paginaError.jsp" %>
<%@page import ="java.util.*"%>
<%@page import ="java.sql.*"%>
<%@page import ="java.util.Collections"%>
<%@page import ="beans.*"%>

<jsp:useBean id="datos" scope="request" class="beans.alltableDAO"></jsp:useBean>
<%

    //response.setContentType ("application/vnd.ms-excel"); //Tipo de fichero.

//response.setHeader ("Content-Disposition", "attachment;filename=\"report.xls\"");

%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<title>Banco Ripley</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
</head>

<body>

<div id="container">
	<ul>
            <jsp:include page="menu.jsp" />
    <div id="logo"><a href="#"><img src="images/logoripley.png" width="226" height="50" alt="" /></a></div>    
        </ul>
      <br />
  <!-- cierre del "h_navcontainer" -->
  <div id="page_top">
  </div>
  <div id="page" >
    <h1 > Detalle de  Data</h1>
        
        <table   border="1" > 
         
         <tr>
        <td>RUT</td>
        <td>NOMBRE</td>
        <td>TELEFONO</td>
        <td>CORREO</td>
        <td>ESTADO CIVIL</td>
        <td>SEXO</td>  
        <td>REGION</td>
        <td>CIUDAD</td>  
        <td>COMUNA</td>
        <td>CALLE</td>
        
        <td>TIPO PROPIEDAD</td>
        <td>ROL</td>
        <td>ESTADO</td>
        <td>S.TERRENO</td>
        <td>S.CONSTRUCCION</td>
        <td>REGION</td>
        <td>CIUDAD</td>
        <td>COMUNA</td>       
        <td>CALLE</td>
        <td>FOJAS</td>
        <td>N.FOJAS</td>
        <td>F.CBR</td>
        <td>N.REPERTORIO</td>
        <td>CLASE S.</td>
        <td>TIPO</td>
        <td>NIVEL</td>      
        <td>EMPRESA T.</td>
        <td>DESCRIPCION</td>
        <td>VALOR UF </td>
        <td>TASADOR</td>
        <td>VALOR LIQUIDEZ</td>
        <td>F.TASACION</td>
        <td>V.PESO</td>
        <td>NOTARIA</td>
        <td>FECHA </td>
        <td>DIRECCION</td>
        <td>ESCRITURA </td>
        <td>C.HIPOTECARIO</td>
        <td>C.COMPLEMENTARIO</td> 
        <td>PRECIO VENTA</td>
        <td>DEUDA PRECIO</td>
        <td>DEUDA TASACION</td>
        <td>DEUDA GARANT;IA</td> 
        <td>OP.HIPOTECARIO</td>
        <td>OP.COMPLEMENTARIO</td>
        <td>EST. HIPOTECARIO</td>
        <td>EST.COMPLEMNTARIO</td>   
           
      </tr>    
           
      <tr>
             <% 
    Connection cn ;
    PreparedStatement ps ;
    ResultSet rs ;               
           
    
                try{
   cn=conexionBdatos.obtenerConexion();
 
   
      
       ps=cn.prepareStatement("  select distinct * from ripleyf.tb_cliente as cli join ripleyf.tb_propiedad as prop on cli.c_rut = prop.c_rut join ripleyf.tb_dirpropiedad as dirp on prop.c_rol =dirp.c_rol join ripleyf.tb_cbr as cbr on prop.c_rol=cbr.c_rol join ripleyf.tb_subsidio as sub on prop.c_rol = sub.c_rol join ripleyf.tb_tasacion as tasac on prop.c_rol=tasac.c_rol join ripleyf.tb_compraventas as cpv on prop.c_rol=cpv.c_rol join ripleyf.tb_escritura as escritura on prop.c_rol =escritura.c_rol join ripleyf.tb_rcredito as cred on prop.c_rol = cred.c_rol;  ");
       rs=ps.executeQuery();
       
       while(rs.next())
       {
       
      
       
  //datos de cliente      
 out.println("<td> "+ rs.getString("c_rut")+" </td>");
 out.println("<td> "+ rs.getString("c_nombre")+" </td>");
 out.println("<td> "+ rs.getString("c_telefono")+" </td>");
 out.println("<td> "+ rs.getString("c_correo")+" </td>");
 out.println("<td> "+ rs.getString("c_estadocivil")+" </td>");
 out.println("<td> "+  rs.getString("c_sexo")+" </td>");
 out.println("<td> "+  rs.getString("c_rgion")+" </td>");
 out.println("<td> "+  rs.getString("c_ciuda")+" </td>");
  
 out.println("<td> "+  rs.getString("c_comu")+" </td>");
 out.println("<td> "+ rs.getString("c_calle")+" </td>");
  
  
  //datso propiedad 
  
 out.println("<td> "+  rs.getString("c_tipo")+" </td>");
 out.println("<td> "+  rs.getString("c_rol")+" </td>");

 out.println("<td> "+  rs.getString("c_estado")+" </td>");
 out.println("<td> "+  rs.getString("c_supterreno")+" </td>");
 out.println("<td> "+ rs.getString("c_supconstruccion")+" </td>");
  
  //direccion de propiedad 

 out.println("<td> "+  rs.getString("c_region")+" </td>");
 out.println("<td> "+  rs.getString("c_ciudad")+" </td>");
 out.println("<td> "+ rs.getString("c_comuna")+" </td>");
 out.println("<td> "+  rs.getString("c_ubicacion")+" </td>");
  
  //CBR 
  
  
  
 out.println("<td> "+rs.getString("c_fojas")+" </td>");
 out.println("<td> "+ rs.getString("c_nregistro")+" </td>");
 out.println("<td> "+ rs.getDate("c_fcbr")+" </td>");
 out.println("<td> "+ rs.getString("c_nrptio")+" </td>");
  
  
  //subsidio 
  


 out.println("<td> "+ rs.getString("c_clase")+" </td>");
 out.println("<td> "+  rs.getString("c_seguro")+" </td>");
 out.println("<td> "+  rs.getString("c_nivel")+" </td>");
 
 //tasacion 
 

 out.println("<td> "+ rs.getString("c_empresa")+" </td>");
 out.println("<td> "+ rs.getString("c_descripcion")+" </td>");
 out.println("<td> "+ rs.getFloat("c_valoruf")+" </td>");
 out.println("<td> "+ rs.getString("c_tasador")+" </td>");
 out.println("<td> "+ rs.getFloat("c_vliquidez")+" </td>");
 out.println("<td> "+  rs.getDate("c_fechatasa")+" </td>");
 out.println("<td> "+ rs.getFloat("c_valorpeso")+" </td>");

 
 //COMPRAVENTAS 
 
 out.println("<td> "+  rs.getString("c_notaria")+" </td>");
 out.println("<td> "+ rs.getDate("c_fecha")+" </td>");
 out.println("<td> "+ rs.getString("c_direccion")+" </td>");
 

 //ESCRITURA 

 out.println("<td> "+  rs.getString("c_aplica")+" </td>");
 
 //CREDITO 
 out.println("<td> "+ rs.getFloat("c_chip")+" </td>");
 out.println("<td> "+ rs.getFloat("c_ccomp")+" </td>");
 out.println("<td> "+ rs.getFloat("c_pventa")+" </td>");

 out.println("<td> "+ rs.getFloat("c_deudaprecio")+" </td>");
 out.println("<td> "+ rs.getFloat("c_deudatasacion")+" </td>");
 out.println("<td> "+ rs.getFloat("c_tdeudagrtia")+" </td>");


 out.println("<td> "+ rs.getLong("c_opehip")+" </td>");
 out.println("<td> "+ rs.getLong("c_opecomp")+" </td>");
  

 out.println("<td> "+ rs.getString("c_esthipo")+" </td>");
 out.println("<td> "+ rs.getString("c_estcomp")+" </td>");
 




 
       
       
       } //end while 
           
 
  
   }//end try
    catch(Exception ex){
    out.println("error en la carga de la tabla"+ ex.getMessage());
     
      }
             %>
           </tr>
            
        </table>
        
        
        
    </body>

                    
            </form>
          </div>
        </div>
        <!--items-->
        

      
    <div style="clear:both"></div>     
    <br></br>
    <br></br>
  </div>
   <!-- close page -->
  <div id="page_bottom"></div>
  
</div>
<!-- close container -->

<!-- close footer -->
</body>
</html>