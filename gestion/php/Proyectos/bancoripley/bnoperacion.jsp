<%@page contentType="text/html" pageEncoding="utf-8"%>
<%@page errorPage="paginaError.jsp" %>
<%@page import ="java.util.*"%>
<%@page import ="java.util.Collections"%>
<%@page import ="beans.*"%>

<jsp:useBean id='datospropiedad' scope='request'   class='beans.busquedaDAO' />
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
          <%  HttpSession hs = request.getSession(true);
          

if (hs.getAttribute("nombreUsuario")==null)
{
response.sendRedirect("Login.html");                              
}


%>

<title>Banco Ripley</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/jquery.validate.js" type="text/javascript"></script> 

</head>

<body>
      <%  
    
String op=request.getParameter("op");
String nop=request.getParameter("nop");

      

%>
<div id="container">
	<ul>
            <jsp:include page="menu.jsp" />
    <div id="logo"><a href="ingresoCliente.jsp"><img src="images/logoripley.png" width="226" height="50" alt="" /></a></div>    
        </ul>
      <br />
  <!-- cierre del "h_navcontainer" -->
  <div id="page_top">
	
	 <!--	************* implementacion del menu pendiente*************
	<div id="sesion_bg">
    <div id="sesion">
    <div id="sesion_derecha">
      <ul>
        <li class="footer_heading">
          <h4>Usuario:</h4>
        </li>
        <div align="center">
        <li><img src="photos/sesion.jpg" width="40" height="40" alt="" /></li>
        </div>
        <li>Null</li>
        <li><input type="text" class="text" name="Usuario" /></li>
        <li>Logout</li>
        <li><input type="text" class="text" name="Contraseña" /></li>   
    </div>
  </div>
  <!-- cierra sesion_bg 
</div>
-->

  </div>
  <div id="page" >
    <h1>Inspeccionar Clientes</h1> 
    
      <div id="wizard3">
        <div class="items" >       
          <!-- Pagina 2 de Ingreso Propiedad -->
          <div class="page"> 
              <% if (op.equals("hip")) { %>
              <form id="SignupForm" action="busquedaCliente.jsp" method="post">
                <li class="required double"> 
                     <% 

          String rolh=datospropiedad.getrolhip(nop);
       
          datospropiedad.setRol(rolh);
             List listaprop =datospropiedad.getPropiedad();
           
             if (!listaprop.isEmpty())
                                 {
             
             beanPropiedad prop;
               for(int k=0;k<listaprop.size();k++)  {
              prop=(beanPropiedad)listaprop.get(k);
              
              
             %>
                        <label>Rol
                          <input type="text" name="nop" class="text" maxlength="10" value="<%=prop.getRol()%>"/>         
                       </label>
                       <label>Tipo de Propiedad
                          <input type="text" name="nop" class="text" maxlength="10" value="<%=prop.getTipoprop()%>"/>         
                       </label>
                      <li class="clearfix">
                      <a href="busquedaCliente.jsp?rol=<%=prop.getRol()%>&rut=<%=prop.getRut()%>"><button type="submit" class="next right">Ver mas</button></a>     
                      </li>
                    <% } }
         
//AQUI PASAS SI EN LA BUSQUEDA NO ESTA LA GARANTIA DEL CLIENTE ESTO QUIERE DECIR QUE LA LISTA VIENE VACIA 
             


else{
          


String msj="la operacion no se encuentra en el sistema ";

 out.println("<html><head></head><body>");
            //out.println("Error de inserción: " + ex);
                   out.print(" <script language='javascript'>");
                    out.println("location.href='busquedanop.jsp?msj=" +msj
                            + "'</script>");
                   
            out.println("</body></html>");
      
      
      



}%>
                    </li>
<%
         } else if(op.equals("comp"))
             
         {



%>
  <li class="required double">   
       <% 
             
             
          
          String rolh=datospropiedad.getrolcomp(nop);
    
          datospropiedad.setRol(rolh);
             List listaprop =datospropiedad.getPropiedad();
           
             if (!listaprop.isEmpty())
                                 {
             
             beanPropiedad prop;
               for(int k=0;k<listaprop.size();k++)  {
              prop=(beanPropiedad)listaprop.get(k);
              
              
             %>
             <label>Rol
             <input   type="text"  id="rol" name="rol" maxlength="10" class="text" value="<%=prop.getRol()%>" />
             </label>
             <label> Tipo de Propiedad
             <input type="text" id="tipo" name="tipoprop"  maxlength="20" class="text"  value="<%=prop.getTipoprop()%>"
             </label>
             <li class="clearfix">
             <a href="busquedaCliente.jsp?rol=<%=prop.getRol()%>&rut=<%=prop.getRut()%>"><button type="submit" class="next right">Ver mas</button></a>
             </li> 
                 
          <% } }
         
//AQUI PASAS SI EN LA BUSQUEDA NO ESTA LA GARANTIA DEL CLIENTE ESTO QUIERE DECIR QUE LA LISTA VIENE VACIA 
             

else{
          


String msj="La operacion no se encuentra en el sistema ";

 out.println("<html><head></head><body>");
            //out.println("Error de inserción: " + ex);
                   out.print(" <script language='javascript'>");
                    out.println("location.href='busquedanop.jsp?msj=" +msj
                     + "'</script>");
                       out.println("</body></html>");
}
%>
              </li>  
<%}%>
              </li>  
              
   </form>
       
    <script src="js/fancybox/jquery.tools.min.js" type="text/javascript"></script>   
    <script type="text/javascript">   
     $().ready(function() {
    $("#SignupForm").validate({
		rules: {
		 'rut':'required'
		},
                
		messages: {
                    
		'rut': 'Debe ingresar el Rut',                                    
       debug: true,
       /*errorElement: 'div',*/
       //errorContainer: $('#errores'),
       submitHandler: function(form){
       }			
		}
	}); 	
     var root = $("#wizard").scrollable({
        size: 1,
        clickable: false
     });
    // some variables that we need
    var api = root.scrollable(),
        drawer = $("#drawer");
    // validation logic is done inside the onBeforeSeek callback
    api.onBeforeSeek(function (event, i) {
        // we are going 1 step backwards so no need for validation
        if (api.getIndex() < i) {
            // 1. get current page
            var page = root.find(".page").eq(api.getIndex()),
                // 2. .. and all required fields inside the page
                inputs = page.find(".required :input").removeClass("error"),
                // 3. .. which are empty
                empty = inputs.filter(function () {
                    return $(this).val().replace(/\s*/g, '') == '';
                });
            // if there are empty fields, then
            if (empty.length) {
                // slide down the drawer
                drawer.slideDown(function () {
                    // colored flash effect
                    drawer.css("backgroundColor", "#229");
                    setTimeout(function () {
                        drawer.css("backgroundColor", "#fff");
                    }, 1000);
                });
                // add a CSS class name "error" for empty & required fields
                empty.addClass("error");
                // cancel seeking of the scrollable by returning false
                return false;
                // everything is good
            } else {
                // hide the drawer
                drawer.slideUp();
            }
        }
        // update status bar
        $("#status li").removeClass("active").eq(i).addClass("active");
    });
    // if tab is pressed on the next button seek to next page
    root.find("button.next").keydown(function (e) {
        if (e.keyCode == 9) {
            // seeks to next tab by executing our validation routine
            api.next();
            e.preventDefault();
        }
    });
    
});

</script>
<script src="js/validate/jquery.validate.js" type="text/javascript"></script>
          </div>
        </div>
        <!--items-->
        
      </div>
      <!--wizard-->
      
    <div style="clear:both"></div>     
    <br></br>
    <br></br>
  </div>
   <!-- close page -->
  <div id="page_bottom"></div>
  
</div>
<!-- close container -->

<!-- close footer -->
</body>
</html>