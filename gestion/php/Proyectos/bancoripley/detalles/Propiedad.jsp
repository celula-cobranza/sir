<%@page contentType="text/html" pageEncoding="utf-8"%> 
<%@page errorPage="paginaError.jsp" %>
<%@page import ="java.util.*"%>
<%@page import ="java.util.Collections"%>
<%@page import ="beans.*"%>
<jsp:useBean id='datospropiedad' scope='request' class='beans.propiedadDAO' />
<jsp:useBean id="dirprop" scope="request" class="beans.dirpropiedadDAO"> </jsp:useBean>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="stylesheet" type="text/css" href="style.css" />
<script src="jquery.js" type="text/javascript"></script>
<script src="jquery.validate.js" type="text/javascript"></script>
<script src="jquery.metadata.js" type="text/javascript"></script>
<%  

String rol= request.getParameter("rol");
String rut = request.getParameter("rut");

%>
<style type="text/css">
#wizard2{
	background:#fff url(../images/h600.png) repeat scroll 0 0;
	border:5px solid #E9E9E9;
	font-size:12px;
	height:560px;
	float:right;
	width:95%;
	overflow:hidden;
	position: absolute;
	/* rounded corners for modern browsers */
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
}
/* scrollable items */
#wizard2 .items{
	width:20000em;
	clear:both;
	position:absolute;
}
/* single item */
#wizard2 .page{

	width:600px;
	float:left;
}
</style>
<title>Banco Ripley</title>
</head>
<body>
      <div id="wizard2">
         <div class="items" >       
          <div class="page">
            <form id="TablaCliente" action="../uppropiedad" method="post" >
          

                <label>INFORMACION PROPIEDAD</label>
  <br></br>
  <li class="required double">  
       <% 
        
          datospropiedad.setRol(rol);
             List listaprop =datospropiedad.getPropiedad();
           beanPropiedad prop;
               for(int k=0;k<listaprop.size();k++)  {
              prop=(beanPropiedad)listaprop.get(k);
              
              
             %>
      
             <input type="hidden" name="rolprop" value="<%=prop.getRol()  %>" ></input>
             <label>Rut
            <input type="text"  disabled="disabled" id="rut" name="rut" maxlength="20"   value="<%=rut %>" class="text" onblur="esrut(document.getElementById('rut').value)" tabindex="1" />
         </label>
         <label>Rol
             <input type="text" disabled="disabled"  id="rol" name="rol" maxlength="20"  value="<%=prop.getRol() %>" class="text" tabindex="1" />
         </label>            
         <label>Tipo
           <input type="text"    id="tipo" name="tipo" maxlength="50"  class="text" value="<%=prop.getTipoprop() %>" tabindex="1" />      
         </label>
         <label>Estado           
                 <input type="text"  id="estado" name="estado" maxlength="20"   class="text" value="<%=prop.getEstado() %>" tabindex="1" />
         </label>
         <label>
          Superficie del Terreno
          <input type="text"   id="supterreno" name="supterreno" maxlength="50" class="text" value="<%=prop.getSupterreno() %>" tabindex="1"  />
         </label> 
         <label> Superficie de Construccion 
             <input type="text"  id="ciuda" name="supconstr" maxlength="50" class="text" value="<%=prop.getSupconst() %>" tabindex="1"  />
         </label> 
         <% }%>
         
         
         <%
 List lis =dirprop.getdirPropiedad(rol);
           
          
             
             beandPropiedad p1;
               for(int k=0;k<lis.size();k++)  {
              p1=(beandPropiedad)lis.get(k);
              
              


%>
         <label >Informacion Subsidio</label>
         <label>Clase
           <input type="text"    id="clase" name="clase" maxlength="20"  class="text" value="<%=p1.getClase() %>" tabindex="1" />      
         </label>
  <label>Seguro
           <input type="text"    id="seguro" name="seguro" maxlength="20"  class="text" value="<%=p1.getSeguro() %>" tabindex="1" />      
         </label>
  <label>Nivel
           <input type="text"    id="nivel" name="nivel" maxlength="10"  class="text" value="<%=p1.getNivel() %>" tabindex="1" />      
         </label>
  
         <label>Direccion de Propiedad</label>
         
         
           <label>Region:
           <input type="text"    id="region" name="region" maxlength="50"  class="text" value="<%=p1.getRegion()  %>" tabindex="1" />      
         </label>
           <label>Ciudad :
           <input type="text"    id="ciudad" name="ciudad" maxlength="50"  class="text" value="<%=p1.getCiudad() %>" tabindex="1" />      
         </label>
  <label>Comuna:
           <input type="text"    id="comuna" name="comuna" maxlength="50"  class="text" value="<%=p1.getComuna() %>" tabindex="1" />      
         </label>
  <label>Calle:
           <input type="text"    id="calle" name="calle" maxlength="50"  class="text" value="<%=p1.getCalle() %>" tabindex="1" />      
         </label>


<% }%>
          </li>
          <div align="right">
          <li class="clearfix">
        <input type="Submit" value="Guardar" class="submitbutton" align="center" />
          </div>
          </li>
        </form>
          </div>
        </div>
        <!--items-->
      </div>
      <!--wizard-->
    <div style="clear:both"></div>     
    <br></br>
    <br></br>
</body>
</html>
