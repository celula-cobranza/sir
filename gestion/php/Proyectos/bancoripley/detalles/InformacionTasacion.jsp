<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page errorPage="paginaError.jsp" %>
<%@page import ="java.util.*"%>
<%@page import ="java.util.Collections"%>
<%@page import ="beans.*"%>
<jsp:useBean id="datostasacion" scope="request"   class="beans.tasacionDAO" />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="stylesheet" type="text/css" href="style.css" />
<link rel="stylesheet" type="text/css" href="../css/calendario.css" />
<script src="jquery.js" type="text/javascript"></script>
<script src="jquery.validate.js" type="text/javascript"></script>
<script src="jquery.metadata.js" type="text/javascript"></script>
<script type="text/javascript" src="js/calendario/calendario_dw.js"></script>

<%
String rol =request.getParameter("rol");
%>
<style type="text/css">
#wizard2{
	background:#fff url(../images/h600.png) repeat scroll 0 0;
	border:5px solid #E9E9E9;
	font-size:12px;
	height:400px;
	float:right;
	width:95%;
	overflow:hidden;
	position: absolute;
	/* rounded corners for modern browsers */
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
}
/* scrollable items */
#wizard2 .items{
	width:20000em;
	clear:both;
	position:absolute;
}
/* single item */
#wizard2 .page{

	width:600px;
	float:left;
}
</style>
   <script type="text/javascript">
	$(document).ready(function(){
		$("#fechatasa").calendarioDW();
	})
	</script>
<title>Banco Ripley</title>
</head>
<body>
      <div id="wizard2">
         <div class="items" >       
          <div class="page">
            <form  id="TablaCliente" action="../uptasacion" method="post" >
          

  <label>INNFORMACION TASACION</label>
  <br></br>
  
  <li class="required double">  
      <%   
             datostasacion.setRol(rol);
          
              List lista=datostasacion.getTasacion();
             
            beanTasacion tasacion;
           
              
              for(int i=0;i<lista.size();i++)  {
              tasacion=(beanTasacion)lista.get(i);
              
              
             %>
             <input type="hidden" name="rolprop" value="<%=rol%>"></input>
         <label>Rol
             <input type="text" disabled="disabled" value="<%=rol %>"  id="rol" name="rol" maxlength="20"  class="text" tabindex="1" />
         </label>            
         <label>Empresa
           <input type="text"    id="empresa" name="empresa" value="<%=tasacion.getEmpresa()%>" maxlength="50"  class="text" tabindex="1" />
         </label>
         <label>Tasador           
           <input type="text"    id="tasador" name="tasador" value="<%=tasacion.getTasdor()%>"  maxlength="50"  class="text" tabindex="1" />
         </label>
         <label>Fecha Tasacion
          <input type="text"  id="fechatasa" name="fechatasa" value="<%=tasacion.getFtasacion()%>" maxlength="10"  class="text" tabindex="1"  />
         </label> 
         <label> Descripcion 
          <textarea id="ccomment"    name="comment" class="text" maxlength="140" > <%=tasacion.getDesc() %> </textarea>
         </label> 
         <label>Valor UF en $ 
          <input type="text"   name="valoruf" value="<%=tasacion.getVuf()%>" maxlength="50" class="text" value="" tabindex="1"  />
         </label>        
          <label>Valor Peso $
           <input type="text"   name="valorpeso" value="<%=tasacion.getVpeso()%>" maxlength="50" class="text" value="" tabindex="1"  />
         </label>
         <label>Valor Liquidez en UF
           <input type="text" id="vliquidez" name="vliquidez" value="<%=tasacion.getVliquides() %>" maxlength="50" class="text" value="" tabindex="1"  />
         </label>
         <% }%>
          </li>
          <div align="right">
          <li class="clearfix">
        <input type="Submit" value="Guardar" class="submitbutton" align="center" />
                    </li>
          </div>
        </form>
          </div>
        </div>
        <!--items-->
      </div>
      <!--wizard-->
    <div style="clear:both"></div>     
    <br></br>
    <br></br>
</body>
</html>
