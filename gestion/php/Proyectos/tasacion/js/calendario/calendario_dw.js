jQuery.fn.calendarioDW = function() {
   this.each(function(){

		var mostrando = false;

		var calendario;

		var capaDiasMes;

		var capaTextoMesAnoActual = $('<div class="visualmesano"></div>');

		var dias = ["l", "m", "x", "j", "v", "s", "d"];

		var nombresMes = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"]

		var elem = $(this);

		var boton = $("<a class='botoncal' href='#'><span></span></a>");

		elem.after(boton);
		

		boton.click(function(e){
			e.preventDefault();
			mostrarCalendario();
		});

		elem.click(function(e){
			this.blur();
			mostrarCalendario();
		});
		

		function mostrarCalendario(){
			if(!mostrando){
				mostrando = true;

				var capaDiasSemana = $('<div class="diassemana"></div>');
				$(dias).each(function(indice, valor){
					var codigoInsertar = '<span';
					if (indice==0){
						codigoInsertar += ' class="primero"';
					}
					if (indice==6){
						codigoInsertar += ' class="domingo ultimo"';
					}
					codigoInsertar += ">" + valor + '</span>';
					
					capaDiasSemana.append(codigoInsertar);
				});
				

				capaDiasMes = $('<div class="diasmes"></div>');
				

				var objFecha = new Date();

				var textoFechaEscrita = elem.val();
				if (textoFechaEscrita!= ""){
					if (validarFechaEscrita(textoFechaEscrita)){
						var arrayFechaEscrita = textoFechaEscrita.split("/");

						if(arrayFechaEscrita[2].length == 2){
							if (arrayFechaEscrita[2].charAt(0)=="0"){
								arrayFechaEscrita[2] = arrayFechaEscrita[2].substring(1);
							}
							arrayFechaEscrita[2] = parseInt(arrayFechaEscrita[2]);
							if (arrayFechaEscrita[2] < 50)
								arrayFechaEscrita[2] += 2000;
						}
						objFecha = new Date(arrayFechaEscrita[2], arrayFechaEscrita[1]-1, arrayFechaEscrita[0])
					}
				}

				var mes = objFecha.getMonth();
				var ano = objFecha.getFullYear();

				muestraDiasMes(mes, ano);
				

				var botonCerrar = $('<a href="#" class="calencerrar"><span></span></a>');
				botonCerrar.click(function(e){
					e.preventDefault();
					calendario.hide("slow");
				})
				var capaCerrar = $('<div class="capacerrar"></div>');
				capaCerrar.append(botonCerrar)

				var botonMesSiguiente = $('<a href="#" class="botonmessiguiente"><span></span></a>');
				botonMesSiguiente.click(function(e){
					e.preventDefault();
					mes = (mes + 1) % 12;
					if (mes==0)
						ano++;
					capaDiasMes.empty();
					muestraDiasMes(mes, ano);
				})
				var botonMesAnterior = $('<a href="#" class="botonmesanterior"><span></span></a>');
				botonMesAnterior.click(function(e){
					e.preventDefault();
					mes = (mes - 1);
					if (mes==-1){
						ano--;
						mes = 11
					}	
					capaDiasMes.empty();
					muestraDiasMes(mes, ano);
				})
				var botonCambioAno = $('<a href="#" class="botoncambiaano"><span></span></a>')
				botonCambioAno.click(function(e){
					e.preventDefault();
					var botonActivoSelAnos = $(this);

					var capaAnos = $('<div class="capaselanos"></div>');

					for (var i=ano-10; i<=ano+10; i++){
						var codigoEnlace = '<a href="#"';
						if (i==ano)
							codigoEnlace += ' class="seleccionado"';
						if (i==ano+10)
							codigoEnlace += ' class="ultimo"';
						codigoEnlace += '><span>' + i + '</span></a>';
						var opcionAno = $(codigoEnlace);
						opcionAno.click(function(e){
							e.preventDefault();
							ano = parseInt($(this).children().text());
							capaDiasMes.empty();
							muestraDiasMes(mes, ano);
							capaAnos.slideUp();
							capaAnos.detach();
						})
						capaAnos.append(opcionAno);
					}
		
					$(document.body).append(capaAnos);

					capaAnos.css({
						top: (botonActivoSelAnos.offset().top + 12) + "px",
						left: (botonActivoSelAnos.offset().left - 25) + "px"
					})
					capaAnos.slideDown();
				})

				var capaTextoMesAno = $('<div class="textomesano"></div>');
				var capaTextoMesAnoControl = $('<div class="mesyano"></div>')
				capaTextoMesAno.append(botonMesSiguiente);
				capaTextoMesAno.append(botonMesAnterior);
				capaTextoMesAno.append(capaTextoMesAnoControl);
				capaTextoMesAnoControl.append(capaTextoMesAnoActual);

				calendario = $('<div class="capacalendario"></div>');
				var calendarioBorde = $('<div class="capacalendarioborde"></div>');
				calendario.append(calendarioBorde);
				calendarioBorde.append(capaCerrar);
				calendarioBorde.append(capaTextoMesAno);
				calendarioBorde.append(capaDiasSemana);
				calendarioBorde.append(capaDiasMes);
				

				$(document.body).append(calendario);

				calendario.css({
					top: boton.offset().top + "px",
					left: (boton.offset().left + 20) + "px"
				})

				calendario.show("slow");
				
			}else{

				calendario.fadeOut("fast");
				calendario.fadeIn("fast");
				
			}
			
		}
		
		function muestraDiasMes(mes, ano){

			capaTextoMesAnoActual.text(nombresMes[mes] + " " + ano);
			

			var contadorDias = 1;

			var primerDia = calculaNumeroDiaSemana(1, mes, ano);

			var ultimoDiaMes = ultimoDia(mes,ano);
			

			for (var i=0; i<7; i++){
				if (i < primerDia){

					var codigoDia = '<span class="diainvalido';
					if (i == 0)
						codigoDia += " primero";
					codigoDia += '"></span>';
				} else {
					var codigoDia = '<span';
					if (i == 0)
						codigoDia += ' class="primero"';
					if (i == 6)
						codigoDia += ' class="ultimo domingo"';
					codigoDia += '>' + contadorDias + '</span>';
					contadorDias++;
				}
				var diaActual = $(codigoDia);
				capaDiasMes.append(diaActual);
			}
			

			var diaActualSemana = 1;
			while (contadorDias <= ultimoDiaMes){
				var codigoDia = '<span';

				if (diaActualSemana % 7 == 1)
					codigoDia += ' class="primero"';

				if (diaActualSemana % 7 == 0)
					codigoDia += ' class="domingo ultimo"';
				codigoDia += '>' + contadorDias + '</span>';
				contadorDias++;
				diaActualSemana++;
				var diaActual = $(codigoDia);
				capaDiasMes.append(diaActual);
			}
			

			diaActualSemana--;
			if (diaActualSemana%7!=0){

				for (var i=(diaActualSemana%7)+1; i<=7; i++){
					var codigoDia = '<span class="diainvalido';
					if (i==7)
						codigoDia += ' ultimo'
					codigoDia += '"></span>';
					var diaActual = $(codigoDia);
					capaDiasMes.append(diaActual);
				}
			}
			
			capaDiasMes.children().click(function(e){
				var numDiaPulsado = $(this).text();
				if (numDiaPulsado != ""){
					elem.val(numDiaPulsado + "-" + (mes+1) + "-" + ano);
					calendario.fadeOut();
				}
			})
		}
		function calculaNumeroDiaSemana(dia,mes,ano){
			var objFecha = new Date(ano, mes, dia);
			var numDia = objFecha.getDay();
			if (numDia == 0) 
				numDia = 6;
			else
				numDia--;
			return numDia;
		}
		
		function checkdate ( m, d, y ) {

			return m > 0 && m < 13 && y > 0 && y < 32768 && d > 0 && d <= (new Date(y, m, 0)).getDate();
		}

		function ultimoDia(mes,ano){ 
			var ultimo_dia=28; 
			while (checkdate(mes+1,ultimo_dia + 1,ano)){ 
			   ultimo_dia++; 
			} 
			return ultimo_dia; 
		} 
		
		function validarFechaEscrita(fecha){
			var arrayFecha = fecha.split("-");
			if (arrayFecha.length!=3)
				return false;
			return checkdate(arrayFecha[1], arrayFecha[0], arrayFecha[2]);
		}
   });
   return this;
};
