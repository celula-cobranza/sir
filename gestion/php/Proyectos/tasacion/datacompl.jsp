<%-- 
    Document   : datacompl
    Created on : 29-mar-2012, 8:55:14
    Author     : vriveros
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page errorPage="paginaError.jsp" %>
<%@page import ="java.util.*,clases.conexionBdatos"%>
<%@page import ="java.sql.*"%>
<%@page import ="java.util.Collections"%>

<%@page import ="beans.*"%>

<jsp:useBean id="datos" scope="request" class="beans.alltableDAO"></jsp:useBean>

<%
 response.setContentType ("application/vnd.ms-excel"); //Tipo de fichero.

response.setHeader ("Content-Disposition", "attachment;filename=\"report.xls\"");

  
%>


<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1 > Detalle de  Data</h1>
        
        <table   border="1" > 
         
         <tr>
        <td>RUT</td>
        <td>NOMBRE</td>
        <td>TELEFONO</td>
        <td>CORREO</td>
        <td>ESTADO CIVIL</td>
        <td>SEXO</td>  
        <td>REGION</td>
        <td>CIUDAD</td>  
        <td>COMUNA</td>
        <td>CALLE</td>
        
        <td>TIPO PROPIEDAD</td>
        <td>ROL</td>
        <td>ESTADO</td>
        <td>S.TERRENO</td>
        <td>S.CONSTRUCCION</td>
        <td>REGION</td>
        <td>CIUDAD</td>
        <td>COMUNA</td>       
        <td>CALLE</td>
        <td>FOJAS</td>
        <td>N.FOJAS</td>
        <td>F.CBR</td>
        <td>N.REPERTORIO</td>
        <td>CLASE S.</td>
        <td>TIPO</td>
        <td>NIVEL</td>      
        <td>EMPRESA T.</td>
        <td>DESCRIPCION</td>
        <td>VALOR UF </td>
        <td>TASADOR</td>
        <td>VALOR LIQUIDEZ</td>
        <td>F.TASACION</td>
        <td>V.PESO</td>
        <td>NOTARIA</td>
        <td>FECHA </td>
        <td>DIRECCION</td>
        <td>ESCRITURA </td>
        <td>C.HIPOTECARIO</td>
        <td>C.COMPLEMENTARIO</td> 
        <td>PRECIO VENTA</td>
        <td>DEUDA PRECIO</td>
        <td>DEUDA TASACION</td>
        <td>DEUDA GARANT;IA</td> 
        <td>OP.HIPOTECARIO</td>
        <td>OP.COMPLEMENTARIO</td>
        <td>EST. HIPOTECARIO</td>
        <td>EST.COMPLEMNTARIO</td>   
           
      </tr>    
           
      
             <% 
    Connection cn ;
    PreparedStatement ps ;
    ResultSet rs ;               
           
    
                try{
   cn=conexionBdatos.obtenerConexion();
 
   
      
       ps=cn.prepareStatement("  select distinct * from ripleyf.tb_cliente  as cli join ripleyf.tb_propiedad as prop on cli.c_rut = prop.c_rut join ripleyf.tb_dirpropiedad as dirp on prop.c_rol =dirp.c_rol join ripleyf.tb_cbr as cbr on prop.c_rol=cbr.c_rol join ripleyf.tb_subsidio as sub on prop.c_rol = sub.c_rol join ripleyf.tb_tasacion as tasac on prop.c_rol=tasac.c_rol join ripleyf.tb_compraventas as cpv on prop.c_rol=cpv.c_rol join ripleyf.tb_escritura as escritura on prop.c_rol =escritura.c_rol join ripleyf.tb_rcredito as cred on prop.c_rol = cred.c_rol ;  ");
       rs=ps.executeQuery();
       
       while(rs.next())
       {
       
      
       out.print("<tr>");
  //datos de cliente      
 out.print("<td> "+ rs.getString("c_rut")+" </td>");
 out.print("<td> "+ rs.getString("c_nombre")+" </td>");
 out.print("<td> "+ rs.getString("c_telefono")+" </td>");
 out.print("<td> "+ rs.getString("c_correo")+" </td>");
 out.print("<td> "+ rs.getString("c_estadocivil")+" </td>");
 out.print("<td> "+  rs.getString("c_sexo")+" </td>");
 out.print("<td> "+  rs.getString("c_rgion")+" </td>");
 out.print("<td> "+  rs.getString("c_ciuda")+" </td>");
  
 out.print("<td> "+  rs.getString("c_comu")+" </td>");
 out.print("<td> "+ rs.getString("c_calle")+" </td>");
  
  
  //datso propiedad 
  
 out.print("<td> "+  rs.getString("c_tipo")+" </td>");
 out.print("<td> "+  rs.getString("c_rol")+" </td>");

 out.print("<td> "+  rs.getString("c_estado")+" </td>");
 out.print("<td> "+  rs.getString("c_supterreno")+" </td>");
 out.print("<td> "+ rs.getString("c_supconstruccion")+" </td>");
  
  //direccion de propiedad 

 out.print("<td> "+  rs.getString("c_region")+" </td>");
 out.print("<td> "+  rs.getString("c_ciudad")+" </td>");
 out.print("<td> "+ rs.getString("c_comuna")+" </td>");
 out.print("<td> "+  rs.getString("c_ubicacion")+" </td>");
  
  //CBR 
  
  
  
 out.print("<td> "+rs.getString("c_fojas")+" </td>");
 out.print("<td> "+ rs.getString("c_nregistro")+" </td>");
 out.print("<td> "+ rs.getDate("c_fcbr")+" </td>");
 out.print("<td> "+ rs.getString("c_nrptio")+" </td>");
  
  
  //subsidio 
  


 out.print("<td> "+ rs.getString("c_clase")+" </td>");
 out.print("<td> "+  rs.getString("c_seguro")+" </td>");
 out.print("<td> "+  rs.getString("c_nivel")+" </td>");
 
 //tasacion 
 

 out.print("<td> "+ rs.getString("c_empresa")+" </td>");
 out.print("<td> "+ rs.getString("c_descripcion")+" </td>");
 out.print("<td> "+ rs.getFloat("c_valoruf")+" </td>");
 out.print("<td> "+ rs.getString("c_tasador")+" </td>");
 out.print("<td> "+ rs.getFloat("c_vliquidez")+" </td>");
 out.print("<td> "+  rs.getDate("c_fechatasa")+" </td>");
 out.print("<td> "+ rs.getInt("c_valorpeso")+" </td>");

 
 //COMPRAVENTAS 
 
 out.print("<td> "+  rs.getString("c_notaria")+" </td>");
 out.print("<td> "+ rs.getDate("c_fecha")+" </td>");
 out.print("<td> "+ rs.getString("c_direccion")+" </td>");
 

 //ESCRITURA 

 out.print("<td> "+  rs.getString("c_aplica")+" </td>");
 
 //CREDITO 
 out.print("<td> "+ rs.getFloat("c_chip")+" </td>");
 out.print("<td> "+ rs.getFloat("c_ccomp")+" </td>");
 out.print("<td> "+ rs.getFloat("c_pventa")+" </td>");

 out.print("<td> "+ rs.getFloat("c_deudaprecio")+" </td>");
 out.print("<td> "+ rs.getFloat("c_deudatasacion")+" </td>");
 out.print("<td> "+ rs.getFloat("c_tdeudagrtia")+" </td>");


 out.print("<td> "+ rs.getLong("c_opehip")+" </td>");
 out.print("<td> "+ rs.getLong("c_opecomp")+" </td>");
  

 out.print("<td> "+ rs.getString("c_esthipo")+" </td>");
 out.println("<td> "+ rs.getString("c_estcomp")+" </td>");
 



out.print("</tr>");
 
       
       
       } //end while 
           
 
  
   }//end try
    catch(Exception ex){
    out.println("error en la carga de la tabla"+ ex.getMessage());
     
      }
             %>
                         
            
  
       
       
  
     
       
            
        </table>

        
        
        
        
    </body>
</html>
