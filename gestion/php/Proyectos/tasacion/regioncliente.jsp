<li class="required double">           
                 <!-- Direccion Cliente -->    
<label>Region:<span>*</span><br />
    
<select name="reg" class="rgion" validate="required:true">	

<option value>- Seleccione una Region -</option>

<option value="Arica y Parinacota">Arica y Parinacota</option>
<option value="Tarapaca">Tarapaca</option>
<option value="Antofagasta">Antofagasta</option>
<option value="Atacama">Atacama</option>
<option value="Coquimbo">Coquimbo</option>
<option value="Valparaiso">Valparaiso</option>
<option value="Region del Libertador Gral. Bernardo O'Higgins">Region del Libertador Gral. Bernardo O'Higgins</option>
<option value="Region del Maule">Region del Maule</option>
<option value="Region del Biobio">Region del Biobio</option>
<option value="Region de la Araucania">Region de la Araucania</option>
<option value="Region de Los Lagos">Region de Los Lagos</option>
<option value="Region Aisen del Gral. Carlos Iba�ez del Campo">Region Aisen del Gral. Carlos Iba�ez del Campo</option>
<option value="Region de Magallanes y de la Antartica Chilena">Region de Magallanes y de la Antartica Chilena</option>
<option value="Region Metropolitana de Santiago">Region Metropolitana de Santiago</option>
<option value="Region de Los Rios">Region de Los Rios</option>

                  </select>
                  <em>Seleccione la Region.</em> 
                  </label>                 
           <label>Ciudades o Provincias:<span>*</span><br/>                     
           
          <select name="ciuda" class="ciuda" validate="required:true">  
          <option value>- Seleccione una Provincia o Ciudad -</option>
             
<option value="Parinacota" class="sub_15">Parinacota</option>
<option value="Arica" class="sub_15">Arica</option>
<option value="Iquique" class="sub_1">Iquique</option>
<option value="Tamarugal" class="sub_1">Tamarugal</option>					
<option value="Antofagasta" class="sub_2">Antofagasta</option>
<option value="El Loa" class="sub_2">El Loa</option>
<option value="Tocopilla" class="sub_2">Tocopilla</option>
<option value="Copiapo" class="sub_3">Copiapo</option>
<option value="Cha�aral" class="sub_3">Cha�aral</option>
<option value="Huasco" class="sub_3">Huasco</option>
<option value="Elqui" class="sub_4">Elqui</option>
<option value="Choapa" class="sub_4">Choapa</option>
<option value="Limari" class="sub_4">Limari</option>
<option value="Valparaiso" class="sub_5">Valparaiso</option>
<option value="Isla de Pascua" class="sub_5">Isla de Pascua</option>
<option value="Los Andes" class="sub_5">Los Andes</option>
<option value="Petorca" class="sub_5">Petorca</option>
<option value="Quillota" class="sub_5">Quillota</option>
<option value="San Antonio" class="sub_5">San Antonio</option>
<option value="San Felipe de Aconcagua" class="sub_5">San Felipe de Aconcagua</option>
<option value="Marga Marga" class="sub_5">Marga Marga</option>
<option value="Cachapoal" class="sub_6">Cachapoal</option>
<option value="Cardenal Caro" class="sub_6">Cardenal Caro</option>
<option value="Colchagua" class="sub_6">Colchagua</option>
<option value="Talca" class="sub_7">Talca</option>
<option value="Cauquenes" class="sub_7">Cauquenes</option>
<option value="Curico" class="sub_7">Curico</option>
<option value="Linares" class="sub_7">Linares</option>
<option value="Concepcion" class="sub_8">Concepcion</option>
<option value="Arauco" class="sub_8">Arauco</option>
<option value="Bio-bio" class="sub_8">Bio-bio</option>
<option value="�uble" class="sub_8">�uble</option>
<option value="Cautin" class="sub_9">Cautin</option>
<option value="Malleco" class="sub_9">Malleco</option>
<option value="Llanquihue" class="sub_10">Llanquihue</option>
<option value="Chiloe" class="sub_10">Chiloe</option>
<option value="Osorno" class="sub_10">Osorno</option>
<option value="Palena" class="sub_10">Palena</option>
<option value="Coihaique" class="sub_11">Coihaique</option>
<option value="Aisen" class="sub_11">Aisen</option>
<option value="Capitan Prat" class="sub_11">Capitan Prat</option>
<option value="General Carrera" class="sub_11">General Carrera</option>
<option value="Magallanes" class="sub_12">Magallanes</option>
<option value="Antartica Chilena" class="sub_12">Antartica Chilena</option>
<option value="Tierra del Fuego" class="sub_12">Tierra del Fuego</option>
<option value="Ultima Esperanza" class="sub_12">Ultima Esperanza</option>
<option value="Santiago" class="sub_13">Santiago</option>
<option value="Cordillera" class="sub_13">Cordillera</option>
<option value="Chacabuco" class="sub_13">Chacabuco</option>
<option value="Maipo" class="sub_13">Maipo</option>
<option value="Melipilla" class="sub_13">Melipilla</option>
<option value="Talagante" class="sub_13">Talagante</option>
<option value="Valdivia" class="sub_14">Valdivia</option>
<option value="Ranco" class="sub_14">Ranco</option>
                    </select>
               <em>Seleccione una Provincia o Ciudad.</em>
                </label> 
                 <label>Comuna:<span>*</span><br />                	 
        
     <select name="comu" class="comu" validate="required:true">  
     <option value>- Seleccione una Comuna-</option>
        
<option value="Arica" class="sub_15">Arica</option>
<option value="Camarones" class="sub_15">Camarones</option>
<option value="Putre" class="sub_15">Putre</option>
<option value="General Lagos" class="sub_15">General Lagos</option>
<option value="Iquique" class="sub_1">Iquique</option>
<option value="Alto Hospicio" class="sub_1">Alto Hospicio</option>
<option value="Pozo Almonte" class="sub_1">Pozo Almonte</option>
<option value="Cami�a" class="sub_1">Cami�a</option>
<option value="Colchane" class="sub_1">Colchane</option>
<option value="Huara" class="sub_1">Huara</option>
<option value="Pica" class="sub_1">Pica</option>
<option value="Antofagasta" class="sub_2">Antofagasta</option>
<option value="Mejillones" class="sub_2">Mejillones</option>
<option value="Sierra Gorda" class="sub_2">Sierra Gorda</option>
<option value="Taltal" class="sub_2">Taltal</option>
<option value="Calama" class="sub_2">Calama</option>
<option value="Ollag�e" class="sub_2">Ollag�e</option>
<option value="San Pedro de Atacama" class="sub_2">San Pedro de Atacama</option>
<option value="Tocopilla" class="sub_2">Tocopilla</option>
<option value="Maria Elena" class="sub_2">Maria Elena</option>
<option value="Copiapo" class="sub_3">Copiapo</option>
<option value="Caldera" class="sub_3">Caldera</option>
<option value="Tierra Amarilla" class="sub_3">Tierra Amarilla</option>
<option value="Cha�aral" class="sub_3">Cha�aral</option>
<option value="Diego de Almagro" class="sub_3">Diego de Almagro</option>
<option value="Vallenar" class="sub_3">Vallenar</option>
<option value="Alto del Carmen" class="sub_3">Alto del Carmen</option>
<option value="Freirina" class="sub_3">Freirina</option>
<option value="Huasco" class="sub_3">Huasco</option>
<option value="La Serena" class="sub_4">La Serena</option>
<option value="Coquimbo" class="sub_4">Coquimbo</option>
<option value="Andacollo" class="sub_4">Andacollo</option>
<option value="La Higuera" class="sub_4">La Higuera</option>
<option value="Paiguano" class="sub_4">Paiguano</option>
<option value="Vicu�a" class="sub_4">Vicu�a</option>
<option value="Illapel" class="sub_4">Illapel</option>
<option value="Canela" class="sub_4">Canela</option>
<option value="Los Vilos" class="sub_4">Los Vilos</option>
<option value="Salamanca" class="sub_4">Salamanca</option>
<option value="Ovalle" class="sub_4">Ovalle</option>
<option value="Combarbala" class="sub_4">Combarbala</option>
<option value="Monte Patria" class="sub_4">Monte Patria</option>
<option value="Punitaqui" class="sub_4">Punitaqui</option>
<option value="Rio Hurtado" class="sub_4">Rio Hurtado</option>
<option value="Valparaiso" class="sub_5">Valparaiso</option>
<option value="Casablanca" class="sub_5">Casablanca</option>
<option value="Concon" class="sub_5">Concon</option>
<option value="Juan Fernandez" class="sub_5">Juan Fernandez</option>
<option value="Puchuncavi" class="sub_5">Puchuncavi</option>
<option value="Quintero" class="sub_5">Quintero</option>
<option value="Vi�a del Mar" class="sub_5">Vi�a del Mar</option>
<option value="Isla de Pascua" class="sub_5">Isla de Pascua</option>
<option value="Los Andes" class="sub_5">Los Andes</option>
<option value="Calle Larga" class="sub_5">Calle Larga</option>
<option value="Rinconada" class="sub_5">Rinconada</option>
<option value="San Esteban" class="sub_5">San Esteban</option>
<option value="La Ligua" class="sub_5">La Ligua</option>
<option value="Cabildo" class="sub_5">Cabildo</option>
<option value="Papudo" class="sub_5">Papudo</option>
<option value="Petorca" class="sub_5">Petorca</option>
<option value="Zapallar" class="sub_5">Zapallar</option>
<option value="Quillota" class="sub_5">Quillota</option>
<option value="Calera" class="sub_5">Calera</option>
<option value="Hijuelas" class="sub_5">Hijuelas</option>
<option value="La Cruz" class="sub_5">La Cruz</option>
<option value="Nogales" class="sub_5">Nogales</option>
<option value="San Antonio" class="sub_5">San Antonio</option>
<option value="Algarrobo" class="sub_5">Algarrobo</option>
<option value="Cartagena" class="sub_5">Cartagena</option>
<option value="El Quisco" class="sub_5">El Quisco</option>
<option value="El Tabo" class="sub_5">El Tabo</option>
<option value="Santo Domingo" class="sub_5">Santo Domingo</option>
<option value="San Felipe" class="sub_5">San Felipe</option>
<option value="Catemu" class="sub_5">Catemu</option>
<option value="Llaillay" class="sub_5">Llaillay</option>
<option value="Panquehue" class="sub_5">Panquehue</option>
<option value="Putaendo" class="sub_5">Putaendo</option>
<option value="Santa Maria" class="sub_5">Santa Maria</option>
<option value="Quilpue" class="sub_5">Quilpue</option>
<option value="Limache" class="sub_5">Limache</option>
<option value="Olmue" class="sub_5">Olmue</option>
<option value="Villa Alemana" class="sub_5">Villa Alemana</option>
<option value="Rancagua" class="sub_6">Rancagua</option>
<option value="Codegua" class="sub_6">Codegua</option>
<option value="Coinco" class="sub_6">Coinco</option>
<option value="Coltauco" class="sub_6">Coltauco</option>
<option value="Do�ihue" class="sub_6">Do�ihue</option>
<option value="Graneros" class="sub_6">Graneros</option>
<option value="Las Cabras" class="sub_6">Las Cabras</option>
<option value="Machali" class="sub_6">Machali</option>
<option value="Malloa" class="sub_6">Malloa</option>
<option value="Mostazal" class="sub_6">Mostazal</option>
<option value="Olivar" class="sub_6">Olivar</option>
<option value="Peumo" class="sub_6">Peumo</option>
<option value="Pichidegua" class="sub_6">Pichidegua</option>
<option value="Quinta de Tilcoco" class="sub_6">Quinta de Tilcoco</option>
<option value="Rengo" class="sub_6">Rengo</option>
<option value="Requinoa" class="sub_6">Requinoa</option>
<option value="San Vicente" class="sub_6">San Vicente</option>
<option value="Pichilemu" class="sub_6">Pichilemu</option>
<option value="La Estrella" class="sub_6">La Estrella</option>
<option value="Litueche" class="sub_6">Litueche</option>
<option value="Marchihue" class="sub_6">Marchihue</option>
<option value="Navidad" class="sub_6">Navidad</option>
<option value="Paredones" class="sub_6">Paredones</option>
<option value="San Fernando" class="sub_6">San Fernando</option>
<option value="Chepica" class="sub_6">Chepica</option>
<option value="Chimbarongo" class="sub_6">Chimbarongo</option>
<option value="Lolol" class="sub_6">Lolol</option>
<option value="Nancagua" class="sub_6">Nancagua</option>
<option value="Palmilla" class="sub_6">Palmilla</option>
<option value="Peralillo" class="sub_6">Peralillo</option>
<option value="Placilla" class="sub_6">Placilla</option>
<option value="Pumanque" class="sub_6">Pumanque</option>
<option value="Santa Cruz" class="sub_6">Santa Cruz</option>
<option value="Talca" class="sub_7">Talca</option>
<option value="Constitucion" class="sub_7">Constitucion</option>
<option value="Curepto" class="sub_7">Curepto</option>
<option value="Empedrado" class="sub_7">Empedrado</option>
<option value="Maule" class="sub_7">Maule</option>
<option value="Pelarco" class="sub_7">Pelarco</option>
<option value="Pencahue" class="sub_7">Pencahue</option>
<option value="Rio Claro" class="sub_7">Rio Claro</option>
<option value="San Clemente" class="sub_7">San Clemente</option>
<option value="San Rafael" class="sub_7">San Rafael</option>
<option value="Cauquenes" class="sub_7">Cauquenes</option>
<option value="Chanco" class="sub_7">Chanco</option>
<option value="Pelluhue" class="sub_7">Pelluhue</option>
<option value="Curico" class="sub_7">Curico</option>
<option value="Huala�e" class="sub_7">Huala�e</option>
<option value="Licanten" class="sub_7">Licanten</option>
<option value="Molina" class="sub_7">Molina</option>
<option value="Rauco" class="sub_7">Rauco</option>
<option value="Romeral" class="sub_7">Romeral</option>
<option value="Sagrada Familia" class="sub_7">Sagrada Familia</option>
<option value="Teno" class="sub_7">Teno</option>
<option value="Vichuquen" class="sub_7">Vichuquen</option>
<option value="Linares" class="sub_7">Linares</option>
<option value="Colbun" class="sub_7">Colbun</option>
<option value="Longavi" class="sub_7">Longavi</option>
<option value="Parral" class="sub_7">Parral</option>
<option value="Retiro" class="sub_7">Retiro</option>
<option value="San Javier" class="sub_7">San Javier</option>
<option value="Villa Alegre" class="sub_7">Villa Alegre</option>
<option value="Yerbas Buenas" class="sub_7">Yerbas Buenas</option>
<option value="Concepcion" class="sub_8">Concepcion</option>
<option value="Coronel" class="sub_8">Coronel</option>
<option value="Chiguayante" class="sub_8">Chiguayante</option>
<option value="Florida" class="sub_8">Florida</option>
<option value="Hualqui" class="sub_8">Hualqui</option>
<option value="Lota" class="sub_8">Lota</option>
<option value="Penco" class="sub_8">Penco</option>
<option value="San Pedro de la Paz" class="sub_8">San Pedro de la Paz</option>
<option value="Santa Juana" class="sub_8">Santa Juana</option>
<option value="Talcahuano" class="sub_8">Talcahuano</option>
<option value="Tome" class="sub_8">Tome</option>
<option value="Hualpen" class="sub_8">Hualpen</option>
<option value="Lebu" class="sub_8">Lebu</option>
<option value="Arauco" class="sub_8">Arauco</option>
<option value="Ca�ete" class="sub_8">Ca�ete</option>
<option value="Contulmo" class="sub_8">Contulmo</option>
<option value="Curanilahue" class="sub_8">Curanilahue</option>
<option value="Los Alamos" class="sub_8">Los Alamos</option>
<option value="Tirua" class="sub_8">Tirua</option>
<option value="Los Angeles" class="sub_8">Los Angeles</option>
<option value="Antuco" class="sub_8">Antuco</option>
<option value="Cabrero" class="sub_8">Cabrero</option>
<option value="Laja" class="sub_8">Laja</option>
<option value="Mulchen" class="sub_8">Mulchen</option>
<option value="Nacimiento" class="sub_8">Nacimiento</option>
<option value="Negrete" class="sub_8">Negrete</option>
<option value="Quilaco" class="sub_8">Quilaco</option>
<option value="Quilleco" class="sub_8">Quilleco</option>
<option value="San Rosendo" class="sub_8">San Rosendo</option>
<option value="Santa Barbara" class="sub_8">Santa Barbara</option>
<option value="Tucapel" class="sub_8">Tucapel</option>
<option value="Yumbel" class="sub_8">Yumbel</option>
<option value="Alto Bio-bio" class="sub_8">Alto Bio-bio</option>
<option value="Chillan" class="sub_8">Chillan</option>
<option value="Bulnes" class="sub_8">Bulnes</option>
<option value="Cobquecura" class="sub_8">Cobquecura</option>
<option value="Coelemu" class="sub_8">Coelemu</option>
<option value="Coihueco" class="sub_8">Coihueco</option>
<option value="Chillan Viejo" class="sub_8">Chillan Viejo</option>
<option value="El Carmen" class="sub_8">El Carmen</option>
<option value="Ninhue" class="sub_8">Ninhue</option>
<option value="�iquen" class="sub_8">�iquen</option>
<option value="Pemuco" class="sub_8">Pemuco</option>
<option value="Pinto" class="sub_8">Pinto</option>
<option value="Portezuelo" class="sub_8">Portezuelo</option>
<option value="Quillon" class="sub_8">Quillon</option>
<option value="Quirihue" class="sub_8">Quirihue</option>
<option value="Ranquil" class="sub_8">Ranquil</option>
<option value="San Carlos" class="sub_8">San Carlos</option>
<option value="San Fabian" class="sub_8">San Fabian</option>
<option value="San Ignacio" class="sub_8">San Ignacio</option>
<option value="San Nicolas" class="sub_8">San Nicolas</option>
<option value="Treguaco" class="sub_8">Treguaco</option>
<option value="Yungay" class="sub_8">Yungay</option>
<option value="Temuco" class="sub_9">Temuco</option>
<option value="Carahue" class="sub_9">Carahue</option>
<option value="Cunco" class="sub_9">Cunco</option>
<option value="Curarrehue" class="sub_9">Curarrehue</option>
<option value="Freire" class="sub_9">Freire</option>
<option value="Galvarino" class="sub_9">Galvarino</option>
<option value="Gorbea" class="sub_9">Gorbea</option>
<option value="Lautaro" class="sub_9">Lautaro</option>
<option value="Loncoche" class="sub_9">Loncoche</option>
<option value="Melipeuco" class="sub_9">Melipeuco</option>
<option value="Nueva Imperial" class="sub_9">Nueva Imperial</option>
<option value="Padre las Casas" class="sub_9">Padre las Casas</option>
<option value="Perquenco" class="sub_9">Perquenco</option>
<option value="Pitrufquen" class="sub_9">Pitrufquen</option>
<option value="Pucon" class="sub_9">Pucon</option>
<option value="Saavedra" class="sub_9">Saavedra</option>
<option value="Teodoro Schmidt" class="sub_9">Teodoro Schmidt</option>
<option value="Tolten" class="sub_9">Tolten</option>
<option value="Vilcun" class="sub_9">Vilcun</option>
<option value="Villarrica" class="sub_9">Villarrica</option>
<option value="Cholchol" class="sub_9">Cholchol</option>
<option value="Angol" class="sub_9">Angol</option>
<option value="Collipulli" class="sub_9">Collipulli</option>
<option value="Curacautin" class="sub_9">Curacautin</option>
<option value="Ercilla" class="sub_9">Ercilla</option>
<option value="Lonquimay" class="sub_9">Lonquimay</option>
<option value="Los Sauces" class="sub_9">Los Sauces</option>
<option value="Lumaco" class="sub_9">Lumaco</option>
<option value="Puren" class="sub_9">Puren</option>
<option value="Renaico" class="sub_9">Renaico</option>
<option value="Traiguen" class="sub_9">Traiguen</option>
<option value="Victoria" class="sub_9">Victoria</option>
<option value="Puerto Montt" class="sub_10">Puerto Montt</option>
<option value="Calbuco" class="sub_10">Calbuco</option>
<option value="Cochamo" class="sub_10">Cochamo</option>
<option value="Fresia" class="sub_10">Fresia</option>
<option value="Frutillar" class="sub_10">Frutillar</option>
<option value="Los Muermos" class="sub_10">Los Muermos</option>
<option value="Llanquihue" class="sub_10">Llanquihue</option>
<option value="Maullin" class="sub_10">Maullin</option>
<option value="Puerto Varas" class="sub_10">Puerto Varas</option>
<option value="Castro" class="sub_10">Castro</option>
<option value="Ancud" class="sub_10">Ancud</option>
<option value="Chonchi" class="sub_10">Chonchi</option>
<option value="Curaco de Velez" class="sub_10">Curaco de Velez</option>
<option value="Dalcahue" class="sub_10">Dalcahue</option>
<option value="Puqueldon" class="sub_10">Puqueldon</option>
<option value="Queilen" class="sub_10">Queilen</option>
<option value="Quellon" class="sub_10">Quellon</option>
<option value="Quemchi" class="sub_10">Quemchi</option>
<option value="Quinchao" class="sub_10">Quinchao</option>
<option value="Osorno" class="sub_10">Osorno</option>
<option value="Puerto Octay" class="sub_10">Puerto Octay</option>
<option value="Purranque" class="sub_10">Purranque</option>
<option value="Puyehue" class="sub_10">Puyehue</option>
<option value="Rio Negro" class="sub_10">Rio Negro</option>
<option value="San Juan de la Costa" class="sub_10">San Juan de la Costa</option>
<option value="San Pablo" class="sub_10">San Pablo</option>
<option value="Chaiten" class="sub_10">Chaiten</option>
<option value="Futaleufu" class="sub_10">Futaleufu</option>
<option value="Hualaihue" class="sub_10">Hualaihue</option>
<option value="Palena" class="sub_10">Palena</option>
<option value="Coihaique" class="sub_11">Coihaique</option>
<option value="Lago Verde" class="sub_11">Lago Verde</option>
<option value="Aisen" class="sub_11">Aisen</option>
<option value="Cisnes" class="sub_11">Cisnes</option>
<option value="Guaitecas" class="sub_11">Guaitecas</option>
<option value="Cochrane" class="sub_11">Cochrane</option>
<option value="O'Higgins" class="sub_11">O'Higgins</option>
<option value="Tortel" class="sub_11">Tortel</option>
<option value="Chile Chico" class="sub_11">Chile Chico</option>
<option value="Rio Iba�ez" class="sub_11">Rio Iba�ez</option>
<option value="Punta Arenas" class="sub_12">Punta Arenas</option>
<option value="Laguna Blanca" class="sub_12">Laguna Blanca</option>
<option value="Rio Verde" class="sub_12">Rio Verde</option>
<option value="San Gregorio" class="sub_12">San Gregorio</option>
<option value="Cabo de Hornos" class="sub_12">Cabo de Hornos</option>
<option value="Antartica" class="sub_12">Antartica</option>
<option value="Porvenir" class="sub_12">Porvenir</option>
<option value="Primavera" class="sub_12">Primavera</option>
<option value="Timaukel" class="sub_12">Timaukel</option>
<option value="Natales" class="sub_12">Natales</option>
<option value="Torres del Paine" class="sub_12">Torres del Paine</option>
<option value="Santiago" class="sub_13">Santiago</option>
<option value="Cerrillos" class="sub_13">Cerrillos</option>
<option value="Cerro Navia" class="sub_13">Cerro Navia</option>
<option value="Conchali" class="sub_13">Conchali</option>
<option value="El Bosque" class="sub_13">El Bosque</option>
<option value="Estacion Central" class="sub_13">Estacion Central</option>
<option value="Huechuraba" class="sub_13">Huechuraba</option>
<option value="Independencia" class="sub_13">Independencia</option>
<option value="La Cisterna" class="sub_13">La Cisterna</option>
<option value="La Florida" class="sub_13">La Florida</option>
<option value="La Granja" class="sub_13">La Granja</option>
<option value="La Pintana" class="sub_13">La Pintana</option>
<option value="La Reina" class="sub_13">La Reina</option>
<option value="Las Condes" class="sub_13">Las Condes</option>
<option value="Lo Barnechea" class="sub_13">Lo Barnechea</option>
<option value="Lo Espejo" class="sub_13">Lo Espejo</option>
<option value="Lo Prado" class="sub_13">Lo Prado</option>
<option value="Macul" class="sub_13">Macul</option>
<option value="Maipu" class="sub_13">Maipu</option>
<option value="�u�oa" class="sub_13">�u�oa</option>
<option value="Pedro Aguirre Cerda" class="sub_13">Pedro Aguirre Cerda</option>
<option value="Pe�alolen" class="sub_13">Pe�alolen</option>
<option value="Providencia" class="sub_13">Providencia</option>
<option value="Pudahuel" class="sub_13">Pudahuel</option>
<option value="Quilicura" class="sub_13">Quilicura</option>
<option value="Quinta Normal" class="sub_13">Quinta Normal</option>
<option value="Recoleta" class="sub_13">Recoleta</option>
<option value="Renca" class="sub_13">Renca</option>
<option value="San Joaquin" class="sub_13">San Joaquin</option>
<option value="San Miguel" class="sub_13">San Miguel</option>
<option value="San Ramon" class="sub_13">San Ramon</option>
<option value="Vitacura" class="sub_13">Vitacura</option>
<option value="Puente Alto" class="sub_13">Puente Alto</option>
<option value="Pirque" class="sub_13">Pirque</option>
<option value="San Jose de Maipo" class="sub_13">San Jose de Maipo</option>
<option value="Colina" class="sub_13">Colina</option>
<option value="Lampa" class="sub_13">Lampa</option>
<option value="Tiltil" class="sub_13">Tiltil</option>
<option value="San Bernardo" class="sub_13">San Bernardo</option>
<option value="Buin" class="sub_13">Buin</option>
<option value="Calera de Tango" class="sub_13">Calera de Tango</option>
<option value="Paine" class="sub_13">Paine</option>
<option value="Melipilla" class="sub_13">Melipilla</option>
<option value="Alhue" class="sub_13">Alhue</option>
<option value="Curacavi" class="sub_13">Curacavi</option>
<option value="Maria Pinto" class="sub_13">Maria Pinto</option>
<option value="San Pedro" class="sub_13">San Pedro</option>
<option value="Talagante" class="sub_13">Talagante</option>
<option value="El Monte" class="sub_13">El Monte</option>
<option value="Isla de Maipo" class="sub_13">Isla de Maipo</option>
<option value="Padre Hurtado" class="sub_13">Padre Hurtado</option>
<option value="Pe�aflor" class="sub_13">Pe�aflor</option>
<option value="Valdivia" class="sub_14">Valdivia</option>
<option value="Corral" class="sub_14">Corral</option>
<option value="Lanco" class="sub_14">Lanco</option>
<option value="Los Lagos" class="sub_14">Los Lagos</option>
<option value="Mafil" class="sub_14">Mafil</option>
<option value="Mariquina" class="sub_14">Mariquina</option>
<option value="Paillaco" class="sub_14">Paillaco</option>
<option value="Panguipulli" class="sub_14">Panguipulli</option>
<option value="La Union" class="sub_14">La Union</option>
<option value="Futrono" class="sub_14">Futrono</option>
<option value="Lago Ranco" class="sub_14">Lago Ranco</option>
<option value="Rio Bueno" class="sub_14">Rio Bueno</option>
                    </select>
                     <em>Seleccione una comuna.</em>
                </label> 
</li>



