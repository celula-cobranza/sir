/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author cabezon
 */
public class tramos {
    
    
    

   
    public float cuenta_hip (int a ,int b, double x, double  y)
    { 
        
            Connection cn ;
   
        
        cn = conexionBdatos.obtenerConexion();
    
    
    
    float hip=0 ;
    
    
 try {
  PreparedStatement ps ;
  ResultSet rs ;
   // ps=cn.prepareStatement("select count(nop) from ripleyf.quiz where credito between ? and ? and (credito/tasacion)between ? and ? ;");
  ps=cn.prepareStatement(" select count(*) from ripley.quizdata where saldo_total > ? and saldo_total <? and (saldo_total/c_valoruf)>? and (saldo_total/c_valoruf)< ? and comp=0;");
  ps.setInt(1,a);
  ps.setInt(2, b);
  ps.setDouble(3,x);
  ps.setDouble(4, y);
  rs=ps.executeQuery();
 
    
    
  rs.next();{
  int rpt= rs.getInt(1);
    

  hip = rpt;
   cn.close();
    
    }   
     
    }catch (SQLException ex ){
    
    
    
    System.out.println("error en la consulta de contar operaciones" + ex.getMessage());
    
 
    
    }
    
    
    
    
    
    return hip ;
  
    
    
    
   
    
    
    
    
      }
    
    public float cuenta_comp(int a ,int b, double x, double  y)
    {
          
      Connection cs;  
        cs = conexionBdatos.obtenerConexion();
    float comp =0;
    
       
 try {
  PreparedStatement ps ;
  ResultSet rs ;
   // ps=cn.prepareStatement("select count(nop) from ripleyf.quiz where credito between ? and ? and (credito/tasacion)between ? and ? ;");
  ps=cs.prepareStatement(" select count(*) from ripley.quizdata where saldo_total > ? and saldo_total <? and (saldo_total/c_valoruf)>? and (saldo_total/c_valoruf)< ? and comp=1;");
  ps.setInt(1,a);
  ps.setInt(2, b);
  ps.setDouble(3,x);
  ps.setDouble(4, y);
  rs=ps.executeQuery();
 
    
    
  rs.next();{
  int rpt= rs.getInt(1);
    

  comp = rpt;
   
    
    }   
     cs.close();
    }catch (SQLException ex ){
    
    
    
    System.out.println("error en la consulta de contar operaciones comp" + ex.getMessage());
    
 
    
    }
   
 return comp;
    
    
    
    }
    
    
    
    public  float suma_tasahip (int a ,int b,double x , double y )
            
    {
    
   float sumhip=0;
     Connection cx;      
       cx=conexionBdatos.obtenerConexion();     
     try {
    
    PreparedStatement ps ;
    ResultSet rs ;
    ps=cx.prepareStatement(" select sum(c_valoruf) from ripley.quizdata where saldo_total >= ? and saldo_total <= ? and (saldo_total/c_valoruf)> ? and (saldo_total/c_valoruf)< ? and comp=0;");
    ps.setInt(1,a);
    ps.setInt(2, b);
    ps.setDouble(3,x);
    ps.setDouble(4, y);
    rs=ps.executeQuery();
 
    
    
    rs.next();{
  sumhip= rs.getInt(1);
    


    }   
    cx.close();
    }catch (SQLException ex ){
    
    
    
    System.out.println("error en la consulta sumar tasaciones" + ex.getMessage());
    
    
    }
    
    
    
    
    return  sumhip;
    
    
    
    }
    
             
    public  float suma_tasacomp (int a ,int b,double x , double y )
            
    {
    
   float sumcomp=0;
    Connection c;     
      c= conexionBdatos.obtenerConexion();      
     try {
    
    PreparedStatement ps ;
    ResultSet rs ;
    ps=c.prepareStatement(" select sum(c_valoruf) from ripley.quizdata where saldo_total >= ? and saldo_total <= ? and (saldo_total/c_valoruf)> ? and (saldo_total/c_valoruf)< ? and comp=1;");
    ps.setInt(1,a);
    ps.setInt(2, b);
    ps.setDouble(3,x);
    ps.setDouble(4, y);
    rs=ps.executeQuery();
 
    
    
    rs.next();
    
  sumcomp= rs.getInt(1);
    
System.out.print("este es la suma comp"+sumcomp);

   c.close();
    
    }catch (SQLException ex ){
    
    
    
    System.out.println("error en la consulta sumar tasaciones" + ex.getMessage());
    sumcomp=0;
    
    }
    
    
    
    
    return  sumcomp;
    
    
    
    }
    
    

  
    public float sum_saldosh(int a ,int b,double x, double y )
       
    {
 float r =0;
 Connection xc;
 xc=conexionBdatos.obtenerConexion();
  try {
    
    PreparedStatement ps ;
    ResultSet rs ;
    ps=xc.prepareStatement(" select sum(saldo_total) from ripley.quizdata where saldo_total >= ? and saldo_total <= ? and (saldo_total/c_valoruf)> ? and (saldo_total/c_valoruf)< ?and comp=0;");
    ps.setInt(1,a);
    ps.setInt(2, b);
    ps.setDouble(3,x);
    ps.setDouble(4, y);
    rs=ps.executeQuery();
 
    
    
    rs.next();{
    r= rs.getInt(1);
    

    
 
    }   
    xc.close();
    }catch (SQLException ex ){
    
    
    
    System.out.println("error en la consulta de sumar creditos" + ex.getMessage());
    
    
    }
 
 
 
 
 return r;
    
    
    }
               
    
    
     public float sum_saldocom(int a ,int b,double x, double y )
       
    {
 float r =0;
 Connection sd=conexionBdatos.obtenerConexion();
 
  try {
    
    PreparedStatement ps ;
    ResultSet rs ;
    ps=sd.prepareStatement(" select sum(saldo_total) from ripley.quizdata where saldo_total >= ? and saldo_total <= ? and (saldo_total/c_valoruf)> ? and (saldo_total/c_valoruf)< ? and comp=1;");
    ps.setInt(1,a);
    ps.setInt(2, b);
    ps.setDouble(3,x);
    ps.setDouble(4, y);
    rs=ps.executeQuery();
 
    
    
    rs.next();{
    r= rs.getInt(1);
    

    
 
    }   
    sd.close();
    }catch (SQLException ex ){
    
    
    
    System.out.println("error en la consulta de sumar saldos" + ex.getMessage());
    
    
    }
 
 
 
 
 return r;
    
    
    }
    
   
}// fin de clases
