<%-- 
    Document   : quiz
    Created on : 23-abr-2012, 12:15:33
    Author     : cabezon
--%>
<%@page import="java.sql.*"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import ="clases.tramos1" %>
<%@page  import="beans.conexionBdatos"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Cartera Hipotecaria Residencial </h1>
        <%
        Connection cn ;
        cn = conexionBdatos.obtenerConexion();
        PreparedStatement ps, as, as2, as3 ;
        ResultSet rs, ra, ra2, r ;
        Statement o=cn.createStatement();
        ps=cn.prepareStatement(" truncate ripleyf.sal_comp;");
        as=cn.prepareStatement(" truncate ripleyf.sal_hip;");
        as2=cn.prepareStatement(" truncate ripleyf.shipcomp;");
        as3=cn.prepareStatement(" truncate ripleyf.quizdata;");
        // as3=cn.prepareStatement("insert into ripleyf.sal_comp (C_ROL, operacion , C_OPEHIP , C_VALORUF, saldo, rut)(SELECT   tb_propiedad.C_ROL, bb.operacion, tb_rcredito.C_OPEHIP,tb_tasacion.C_VALORUF,     bb.saldo, bb.rut FROM    (   (   ripleyf.tb_propiedad tb_propiedad INNER JOIN ripleyf.tb_tasacion tb_tasacion ON (tb_propiedad.C_ROL = tb_tasacion.C_ROL)) INNER JOIN ripleyf.tb_rcredito tb_rcredito (tb_propiedad.C_ROL = tb_rcredito.C_ROL)) INNER JOIN ripleyf.bb bb ON (bb.rut = tb_propiedad.C_RUT) WHERE bb.operacion LIKE '640%')");
        //as=cn.prepareStatement(" insert into sal_comp(C_ROL,operacion,C_OPEHIP,C_VALORUF,saldo,rut)(SELECT tb_propiedad.C_ROL;");
        rs=ps.executeQuery();
        ra=as.executeQuery();
        ra2=as2.executeQuery();
        r=as3.executeQuery();
        o.executeUpdate("insert into ripleyf.sal_comp (C_ROL, operacion , C_OPEHIP , C_VALORUF, saldo, rut)(SELECT   tb_propiedad.C_ROL, bb.operacion, tb_rcredito.C_OPEHIP,tb_tasacion.C_VALORUF,bb.saldo,bb.rut FROM    (   (   ripleyf.tb_propiedad tb_propiedad INNER JOIN ripleyf.tb_tasacion tb_tasacion ON (tb_propiedad.C_ROL = tb_tasacion.C_ROL)) INNER JOIN ripleyf.tb_rcredito tb_rcredito ON (tb_propiedad.C_ROL = tb_rcredito.C_ROL)) INNER JOIN ripleyf.bb bb ON (bb.rut = tb_propiedad.C_RUT) WHERE bb.operacion LIKE '640%');");
        o.executeUpdate("insert into ripleyf.sal_hip (C_ROL, operacion , C_OPEHIP , C_VALORUF, saldo, rut)(SELECT   tb_propiedad.C_ROL, bb.operacion, tb_rcredito.C_OPEHIP,tb_tasacion.C_VALORUF,     bb.saldo, bb.rut FROM    (   (   ripleyf.tb_propiedad tb_propiedad INNER JOIN ripleyf.tb_tasacion tb_tasacion ON (tb_propiedad.C_ROL = tb_tasacion.C_ROL)) INNER JOIN ripleyf.tb_rcredito tb_rcredito ON (tb_propiedad.C_ROL = tb_rcredito.C_ROL)) INNER JOIN ripleyf.bb bb ON (bb.rut = tb_propiedad.C_RUT) WHERE bb.operacion LIKE '603%');");
        o.executeUpdate("delete from ripleyf.sal_hip where operacion<>C_OPEHIP");
        o.executeUpdate("Insert  into ripleyf.shipcomp (sal,op) (select (sal_hip.saldo + sal_comp.saldo), sal_hip.operacion from ripleyf.sal_comp ,ripleyf.sal_hip where sal_comp.C_ROL = sal_hip.C_ROL);");
        o.executeUpdate("create temporary table ripleyf.tempTablaAsi (sal float, op varchar(45));");
        o.executeUpdate("insert into ripleyf.tempTablaAsi select * from ripleyf.shipcomp group by op;");
        o.executeUpdate("delete from ripleyf.shipcomp;");
        o.executeUpdate("insert into ripleyf.shipcomp select * from ripleyf.tempTablaAsi;");
        o.executeUpdate("drop temporary table ripleyf.tempTablaAsi;");
        o.executeUpdate("INSERT INTO ripleyf.quizdata (saldo, operacion, C_VALORUF, rut, saldo_total, comp)( SELECT bb.saldo, bb.operacion, tb_tasacion.C_VALORUF, tb_propiedad.C_RUT, bb.saldo, bb.operacion FROM    (   ripleyf.tb_propiedad tb_propiedad INNER JOIN ripleyf.tb_tasacion tb_tasacion ON (tb_propiedad.C_ROL = tb_tasacion.C_ROL)) INNER JOIN ripleyf.bb bb ON (bb.rut = tb_propiedad.C_RUT));");
        o.executeUpdate("update ripleyf.quizdata set comp =0;");
        o.executeUpdate("update ripleyf.quizdata set saldo_total=0;");
        o.executeUpdate("update ripleyf.quizdata set comp=3 where  operacion like '640%';");
        o.executeUpdate("update ripleyf.quizdata set saldo_total=(select sal from ripleyf.shipcomp where shipcomp.op=quizdata.operacion);");
        o.executeUpdate("update ripleyf.quizdata set comp=1  where  saldo_total >1;");
        o.executeUpdate("update ripleyf.quizdata set saldo_total = (select saldo from ripleyf.sal_hip where sal_hip.operacion=quizdata.operacion) where comp =0;");
%>
        
        <a href="quizexcel.jsp" >Exportar a excel</a>
        <p><a href="Dquiz.jsp" >Detalle quiz</a></p>
        <table border="1" bordercolor="black">
            <thead>
                <tr>
                    <th colspan="2">Tramo Loan to Value %</th>
                    <th>Información </th>
                    <th>0-500</th>
                    <th>500-1000</th>
                    <th>1000-2000</th>
                    <th>2000-3000</th>
                    <th>3000-5000</th>
                    <th>>5000</th>
                   
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td >0%</td>
                    <td>20%</td>
                    <td>Valor Total de la última Tasación disponible de la garantía (UF)<bR>
                    Colocación Total (UF)<br>
                    Número de operaciones<br>
                    </td>
                    <td>
                        <%
 tramos1 tr = new tramos1();   
 
 float t500h=tr.suma_tasahip(0,500, 0.0, 0.2);
 float t500c=tr.suma_tasacomp(0,500, 0.0, 0.2);
 float t1500= t500c+t500h; //suma tasaciones tramo 0-500 y 0.0 a 0.2
 float c500h=tr.sum_saldosh(0, 500, 0.0, 0.2);
 float c500c=tr.sum_saldocom(0, 500, 0.0, 0.2);
  float c1500= c500c+ c500h; //suma creditos 
 float o500h=tr.cuenta_hip(0, 500, 0.0, 0.2);
 float o500c=tr.cuenta_comp(0, 500, 0.0, 0.2);
 o500c = o500c*2;
 float o1500= o500c +  o500h; // suma creditos 
 
 


out.println(t1500+"<br>"); 
out.println(c1500+"<br>"); 
out.println(o1500);      
%>
    
              
                    
                    </td>
                    <td>                        <%
 
 
 float t1000h=tr.suma_tasahip(500, 1000, 0, 0.2);
 float t1000c=tr.suma_tasacomp(500, 1000, 0, 0.2);
 float t1milt=  t1000c + t1000h;
 float c1000h=tr.sum_saldosh(500, 1000, 0, 0.2);
 float c1000c=tr.sum_saldocom(500, 1000, 0, 0.2);
  float t1milc= c1000c + c1000h;
 float o1000h = tr.cuenta_hip(500, 1000, 0, 0.2);
 float o1000c = tr.cuenta_comp(500, 1000, 0, 0.2);
 o1000c=o1000c*2;
 float t1milop= o1000c + o1000h ;
    


out.println(t1milt+"<br>"); 
out.println(t1milc+"<br>"); 
out.println(t1milop);         
%></td>
                    <td>
                        
                        
                <%        
                     
 float t2000h=tr.suma_tasahip(1000, 2000, 0, 0.2);
 float t2000c=tr.suma_tasacomp(1000, 2000, 0, 0.2);
  float t2000t= t2000c +  t2000h;
 float c2000h=tr.sum_saldosh(1000, 2000, 0, 0.2);
 float c2000c= tr.sum_saldocom(1000, 2000, 0, 0.2);
 float c2000= c2000c +  c2000h;
 float o2000h=tr.cuenta_hip(1000, 2000, 0, 0.2);
 float o2000c=tr.cuenta_comp(1000, 2000, 0, 0.2);
 o2000c= o2000c*2;
float o2000= o2000c+ o2000h;


out.println(t2000t+"<br>"); 
out.println(c2000+"<br>"); 
out.println(o2000);         
%>
                        
                        
                        
                        
                        
                    </td>
                    
     <td>
         
         
             
                <%        
                       
 float t3000h=tr.suma_tasahip(2000, 3000, 0, 0.2);
 float t3000c=tr.suma_tasacomp(2000, 3000, 0, 0.2);
  float t3000= t3000c + t3000h;
 float c3000h=tr.sum_saldosh(2000, 3000, 0, 0.2);
 float c3000c=tr.sum_saldocom(2000, 3000, 0, 0.2);
  float c3000 = c3000c + c3000h;
 float o3000h=tr.cuenta_hip(2000, 3000, 0, 0.2);
 float o3000c=tr.cuenta_comp(2000, 3000, 0, 0.2);
 float o3000=o3000c*2;
  o3000= o3000c + o3000h;    


out.println(t3000+"<br>"); 
out.println(c3000+"<br>"); 
out.println(o3000);         
%>
                         
         
         
         
         
         
         
         
     </td>
                    <td>
  
                <%        
                    
 float t5000h=tr.suma_tasahip(3000, 5000, 0, 0.2);
 float t5000c=tr.suma_tasacomp(3000, 5000, 0, 0.2);
  float t5000= t5000c + t5000h;
 float c5000h = tr.sum_saldosh(3000, 5000, 0, 0.2);
 float c5000c= tr.sum_saldocom(3000, 5000, 0, 0.2);
 
 float c5000 = c5000c + c5000h;
float o5000h=tr.cuenta_hip(3000, 5000, 0, 0.2);
float o5000c=tr.cuenta_comp(3000, 5000, 0, 0.2);
o5000c=o5000c*2;
float o5000= o5000c+ o5000h ;

out.println(t5000+"<br>"); 
out.println( c5000+"<br>"); 
out.println( o5000);         
%>
                         
                                
                        
                        
                        
                        
                        
                        
                        
                        
                        
                    </td>
                    <td></td>
                    
                </tr>
                <tr>
                    <td>20%</td>
                    <td>40%</td>
                    <td>Valor Total de la última Tasación disponible de la garantía (UF)<bR>
                    Colocación Total (UF)<br>
                    Número de operaciones<br>
                   </td>
                    <td>
                        <%
float t2500h=tr.suma_tasahip(0, 500, 0.2, 0.4) ;  
float t2500c=tr.suma_tasacomp(0, 500, 0.2, 0.4) ; 

float t2500= t2500c + t2500h;
float c2500h=tr.sum_saldosh(0, 500, 0.2, 0.4);
float c2500c=tr.sum_saldocom(0, 500, 0.2, 0.4);
float c2500= c2500c+ c2500h;
float o2500h=tr.cuenta_hip(0, 500, 0.2, 0.4);
float o2500c=tr.cuenta_comp(0, 500, 0.2, 0.4);
o2500c=o2500c*2;
float o2500= o2500c+ o2500h;


out.println(t2500+"<br>"); 
   out.println(c2500+"<br>");  
out.println( o2500);  

%>
                        
                        
                    </td>
                    <td>
                <%        
    float t2milh=tr.suma_tasahip(500, 1000, 0.2, 0.4);  
float t2milc=tr.suma_tasacomp(500, 1000, 0.2, 0.4);

float t2mil= t2milc + t2milh;
float c2milh=tr.sum_saldosh(500, 1000, 0.2, 0.4);
float c2milc=tr.sum_saldocom(500, 1000, 0.2, 0.4);
float c2mil= c2milc+ c2milh;
float o2milh=tr.cuenta_hip(500, 1000, 0.2, 0.4);
float o2milc=tr.cuenta_comp(500, 1000, 0.2, 0.4);
o2milc=o2milc*2;
float o2mil= o2milc+ o2milh;

    


out.println(t2mil+"<br>"); 
out.println(c2mil+"<br>"); 
out.println(o2mil );   
                        
                        %>
                        
                    </td>
                    <td>
                        
                       
                         <%
               float     t2dosmilh=tr.suma_tasahip(1000, 2000, 0.2, 0.4)  ;   
               float     t2dosmilc=tr.suma_tasacomp(1000, 2000, 0.2, 0.4)  ; 
                 float t2dosmil= t2dosmilc+  t2dosmilh;
               
  float c2dosmilh=tr.sum_saldosh(1000, 2000, 0.2, 0.4);
   float c2dosmilc=tr.sum_saldosh(1000, 2000, 0.2, 0.4);
   
   float c2dosmil = c2dosmilc + c2dosmilh;
   
   float o2dosmilh=tr.cuenta_hip(1000, 2000, 0.2, 0.4);
    float o2dosmilc=tr.cuenta_comp(1000, 2000, 0.2, 0.4);
    o2dosmilc=o2dosmilc*2; 
    float o2dosmil= o2dosmilc+ o2dosmilh;
       

out.println(t2dosmil+"<br>"); 
out.println( c2dosmil+"<br>"); 
out.println(o2dosmil );   
                        
                        %>
                           
                        
                        
                    </td>
    <td>
        
  
                         <%
                         
       float     t2tresmilh=tr.suma_tasahip(2000, 3000, 0.2, 0.4);    
               float     t2tresmilc=tr.suma_tasacomp(2000, 3000, 0.2, 0.4); 
                 float t2tresmil=  t2tresmilh+  t2tresmilc;
               
  float c2tresmilh=tr.sum_saldosh(2000, 3000, 0.2, 0.4); 
   float c2tresmilc=tr.sum_saldosh(2000, 3000, 0.2, 0.4); 
   
   float c2tresmil = c2tresmilh +c2tresmilc;
   
   float o2tresmilh=tr.cuenta_hip(2000, 3000, 0.2, 0.4); 
    float o2tresmilc=tr.cuenta_comp(2000, 3000, 0.2, 0.4); 
    o2tresmilc=o2tresmilc*2;
    float o2tresmil= o2tresmilh+ o2tresmilc;
            


out.println(t2tresmil+"<br>"); 
out.println(c2tresmil+"<br>"); 
out.println(o2tresmil );   
                        
                        %>
                                 
        
        
        
        
    </td>
                    <td>
                        
         
                         <%
float t25milh=tr.suma_tasahip(3000,5000, 0.2, 0.4);
float t25milc=tr.suma_tasacomp(3000,5000, 0.2, 0.4);
float t25mil= t25milh+ t25milc;
float c25milh=tr.sum_saldosh(3000,5000, 0.2, 0.4);
float c25milc=tr.sum_saldocom(3000,5000, 0.2, 0.4);
float c25mil= c25milc+ c25milh;
float o25milh=tr.cuenta_hip(3000,5000, 0.2, 0.4);
float o25milc=tr.cuenta_comp(3000,5000, 0.2, 0.4);
o25milc=o25milc*2;
float o25mil= o25milc+ o25milh;

out.println(t25mil+"<br>"); 
out.println(c25mil+"<br>"); 
out.println(o25mil );   
                        
                        %>
                                 
        
                       
                        
                        
                        
                        
                        
                        
                        
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>40%</td>
                    <td>60%</td>
                   <td>Valor Total de la última Tasación disponible de la garantía (UF)<bR>
                    Colocación Total (UF)<br>
                    Número de operaciones<br>
                   </td>
                   <td>
                       <%
               float     t3500h=tr.suma_tasahip(0, 500, 0.4, 0.6);
               float t3500c=tr.suma_tasacomp(0, 500, 0.4, 0.6);
               float t3500= t3500c+ t3500h;
               float c3500h=tr.sum_saldosh(0, 500, 0.4, 0.6);
               float c3500c=tr.sum_saldocom(0, 500, 0.4, 0.6);
               float c3500= c3500c+c3500h;
               float o3500h=tr.cuenta_hip(0, 500, 0.4, 0.6);
               float o3500c=tr.cuenta_comp(0, 500, 0.4, 0.6);
               
               o3500c=o3500c*2;
               float o3500= o3500c+o3500h;
               

out.println(t3500+"<br>");
out.println(c3500+"<br>");
out.println(o3500); 
                       
                       %>  
</td>
                    <td>
           <%             
            
               float t3milh=tr.suma_tasahip(500, 1000, 0.4, 0.6);
               float t3milc=tr.suma_tasacomp(500, 1000, 0.4, 0.6);
               float t3mil= t3milc+ t3milh;
               float c3milh=tr.sum_saldosh(500, 1000, 0.4, 0.6);
               float c3milc=tr.sum_saldocom(500, 1000, 0.4, 0.6);
               float c3mil= c3milc+c3milh;
               float o3milh=tr.cuenta_hip(500, 1000, 0.4, 0.6);
               float o3milc=tr.cuenta_comp(500, 1000, 0.4, 0.6);
               o3milc=o3milc*2;
               float o3mil= o3milc+o3milh;
               
                                
  
out.println( t3mil+"<br>"); 
out.println(c3mil+"<br>"); 
out.println( o3mil);              
                        
                  %>      
                        
                        
                    </td>
                    <td>
                        
                  <%             
                  float t32milh=tr.suma_tasahip(1000, 2000, 0.4, 0.6);
               float t32milc=tr.suma_tasacomp(1000,2000,0.4,0.6) ;
               float t32mil= t32milc+ t32milh;
               float c32milh=tr.sum_saldosh(1000,2000,0.4,0.6) ;
               float c32milc=tr.sum_saldocom(1000,2000,0.4,0.6) ;
               float c32mil= c32milc+c32milh;
               float o32milh=tr.cuenta_hip(1000,2000,0.4,0.6) ;
               float o32milc=tr.cuenta_comp(1000,2000,0.4,0.6) ;
               o32milc=o32milc*2;
               float o32mil= o32milc+o32milh;
                       
         

out.println(t32mil+"<br>"); 
out.println(c32mil+"<br>"); 
out.println(o32mil );              
                        
                  %>          
                        
                        
                        
                        
                        
                    </td>
                    <td>
                    <%             
                        
               float t33milh=tr.suma_tasahip(2000, 3000, 0.4, 0.6);
               float t33milc=tr.suma_tasacomp(2000, 3000, 0.4, 0.6);
               float t33mil= t33milc+ t33milh;
               float c33milh=tr.sum_saldosh(2000, 3000, 0.4, 0.6);
               float c33milc=tr.sum_saldocom(2000, 3000, 0.4, 0.6);
               float c33mil= c33milc+c33milh;
               float o33milh=tr.cuenta_hip(2000, 3000, 0.4, 0.6);
               float o33milc=tr.cuenta_comp(2000, 3000, 0.4, 0.6);
               o33milc=o33milc*2;
               float o33mil= o33milc+o33milh;
                       
           
                    
          


out.println(t33mil+"<br>"); 
out.println(c33mil+"<br>"); 
out.println(o33mil );              
                        
                  %>          
                        
                             
                        
                        
                        
                        
                        
                        
                        
                        
                        
                    </td>
                    <td>
                        
                        
                         <%             
              


               float t35milh=tr.suma_tasahip(3000,5000, 0.4, 0.6);
               float t35milc=tr.suma_tasacomp(3000,5000, 0.4, 0.6);
               float t35mil= t35milc+ t35milh;
               float c35milh=tr.sum_saldosh(3000,5000, 0.4, 0.6);
               float c35milc=tr.sum_saldocom(3000,5000, 0.4, 0.6);
               float c35mil= c35milc+c35milh;
               float o35milh=tr.cuenta_hip(3000,5000, 0.4, 0.6);
               float o35milc=tr.cuenta_comp(3000,5000, 0.4, 0.6);
               o35milc= o35milc*2;
               float o35mil= o35milc+o35milh;



out.println( t35mil+"<br>"); 
out.println(c35mil+"<br>"); 
out.println( o35mil);              
                        
                  %>          
                           
                        
                        
                        
                        
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>60%</td>
                    <td>70%</td>
                    <td>Valor Total de la última Tasación disponible de la garantía (UF)<bR>
                    Colocación Total (UF)<br>
                    Número de operaciones<br>
                   </td>
                    <td>
                       <% 
                       
         float   t4500h=tr.suma_tasahip(0, 500, 0.6, 0.7);         
         float   t4500c=tr.suma_tasacomp(0, 500, 0.6, 0.7);   
         float t4500= t4500h+t4500c;
       float c4500h=tr.sum_saldosh(0, 500, 0.6, 0.7);
       float c4500c=tr.sum_saldocom(0, 500, 0.6, 0.7);
       
       float c4500=c4500h+c4500c;
       float o4500h=tr.cuenta_hip(0, 500, 0.6, 0.7);
       float o4500c=tr.cuenta_comp(0, 500, 0.6, 0.7);
         o4500c= o4500c*2;
       float o4500=o4500h+o4500c;
      
   out.println(  t4500+"<br>");      
   out.println( c4500+"<br>" );                                                  
out.println( o4500);  
  %> 
                        
                        
                        
                    </td>
                    <td>
                        
                        <%             
            float   t4milh=tr.suma_tasahip(500, 1000, 0.6, 0.7);        
         float   t4milc=tr.suma_tasacomp(500, 1000, 0.6, 0.7);  
         float t4mil= t4milh+t4milc;
       float c4milh=tr.sum_saldosh(500, 1000, 0.6, 0.7);
       float c4milc=tr.sum_saldocom(500, 1000, 0.6, 0.7);
       
       float c4mil=c4milh+c4milc;
       float o4milh=tr.cuenta_hip(500, 1000, 0.6, 0.7);
       float o4milc=tr.cuenta_comp(500, 1000, 0.6, 0.7);
       o4milc= o4milc*2;
       float o4mil=o4milh+o4milc;
 

out.println( t4mil+"<br>"); 
out.println(c4mil+"<br>"); 
out.println(o4mil );              
                        
                  %> 
                        
                        
                    </td>
                    <td>
                       <%             
                                          
            float   t42milh=tr.suma_tasahip(1000, 2000, 0.6, 0.7);       
         float   t42milc=tr.suma_tasacomp(1000, 2000, 0.6, 0.7); 
         float t42mil= t42milh+t42milc;
       float c42milh=tr.sum_saldosh(1000, 2000, 0.6, 0.7);
       float c42milc=tr.sum_saldocom(1000, 2000, 0.6, 0.7);
       
       float c42mil=c42milh+c42milc;
       float o42milh=tr.cuenta_hip(1000, 2000, 0.6, 0.7);
       float o42milc=tr.cuenta_comp(1000, 2000, 0.6, 0.7);
       o42milc= o42milc*2;
       float o42mil=o42milh+o42milc;
 

out.println( t42mil+"<br>"); 
out.println(c42mil+"<br>"); 
out.println(o42mil );              
                            
                    
                        
                  %>              
                        
                        
                        
                        
                        
                        
                    </td>
                    <td>
                        
                <%             
                                                  
            float   t43milh=tr.suma_tasahip(2000, 3000, 0.6, 0.7);      
         float   t43milc=tr.suma_tasacomp(2000, 3000, 0.6, 0.7);
         float t43mil= t43milh+t43milc;
       float c43milh=tr.sum_saldosh(2000, 3000, 0.6, 0.7);
       float c43milc=tr.sum_saldocom(2000, 3000, 0.6, 0.7);
       
       float c43mil=c43milh+c43milc;
       float o43milh=tr.cuenta_hip(2000, 3000, 0.6, 0.7);
       float o43milc=tr.cuenta_comp(2000, 3000, 0.6, 0.7);
       o43milc= o43milc*2;
       float o43mil=o43milh+o43milc;
 

out.println( t43mil+"<br>"); 
out.println(c43mil+"<br>"); 
out.println(o43mil );              
                                    
                      
                        
                  %>              
                        
                    </td>
                    <td>
             <%             
                                                          
            float   t45milh=tr.suma_tasahip(3000,5000, 0.6, 0.7);      
         float   t45milc=tr.suma_tasacomp(3000,5000, 0.6, 0.7);
         float t45mil= t45milh+t45milc;
       float c45milh=tr.sum_saldosh(3000,5000, 0.6, 0.7);
       float c45milc=tr.sum_saldocom(3000,5000, 0.6, 0.7);
       
       float c45mil=c45milh+c45milc;
       float o45milh=tr.cuenta_hip(3000,5000, 0.6, 0.7);
       float o45milc=tr.cuenta_comp(3000,5000, 0.6, 0.7);
       o45milc=  o45milc*2;
       float o45mil=o45milh+o45milc;
 

out.println( t45mil+"<br>"); 
out.println(c45mil+"<br>"); 
out.println(o45mil );              
                                    
                 
                  %>                 
                        
                        
                        
                        
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>70%</td>
                    <td>80%</td>
                    <td>Valor Total de la última Tasación disponible de la garantía (UF)<bR>
                    Colocación Total (UF)<br>
                    Número de operaciones<br>
                   </td>
                    <td>
                       <%
                       
                       
                       
       float t5500h=tr.suma_tasahip(0, 500, 0.7, 0.8);
       float t5500c=tr.suma_tasacomp(0, 500, 0.7, 0.8);
       float t5500= t5500c +t5500h;
       float c5500h=tr.sum_saldosh(0, 500, 0.7, 0.8);
       float c5500c=tr.sum_saldocom(0, 500, 0.7, 0.8);
       float c5500=c5500c+c5500h;
       float o5500h=tr.cuenta_hip(0, 500, 0.7, 0.8);
       float o5500c=tr.cuenta_comp(0, 500, 0.7, 0.8);
       o5500c=o5500c*2;
       float o5500=o5500h+o5500c;

    out.println(  t5500+"<br>");      
    out.println(  c5500+"<br>" );                                                  
    out.println(  o5500);  




  
                       



%> 
                        
                        
                        
                    </td>
                    <td>
                        
                <%        
                   
       float t5milh=tr.suma_tasahip(500, 1000, 0.7, 0.8);
       float t5milc=tr.suma_tasacomp(500, 1000, 0.7, 0.8);
       float t5mil= t5milh +t5milc;
       float c5milh=tr.sum_saldosh(500, 1000, 0.7, 0.8);
       float c5milc=tr.sum_saldocom(500, 1000, 0.7, 0.8);
       float c5mil=c5milh+c5milc;
       float o5milh=tr.cuenta_hip(500, 1000, 0.7, 0.8);
       float o5milc=tr.cuenta_comp(500, 1000, 0.7, 0.8);
       o5milc=o5milc*2;
       float o5mil=o5milh+o5milc;

    out.println( t5mil+"<br>");      
    out.println(  c5mil+"<br>" );                                                  
    out.println(  o5mil);  

     

                  %> 
                        
                        
                    </td>
                    <td>
                        
                              
                <%        
                                           
                          
       float t52milh=tr.suma_tasahip(1000, 2000, 0.7, 0.8);
       float t52milc=tr.suma_tasacomp(1000, 2000, 0.7, 0.8);
       float t52mil= t52milh +t52milc;
       float c52milh=tr.sum_saldosh(1000, 2000, 0.7, 0.8);
       float c52milc=tr.sum_saldocom(1000, 2000, 0.7, 0.8);
       float c52mil=c52milh+c52milc;
       float o52milh=tr.cuenta_hip(1000, 2000, 0.7, 0.8);
       float o52milc=tr.cuenta_comp(1000, 2000, 0.7, 0.8);
       o52milc=o52milc*2;
       float o52mil=o52milh+o52milc;

    out.println( t52mil+"<br>");      
    out.println(  c52mil+"<br>" );                                                  
    out.println(  o52mil);  
           %> 
                           
                        
                        
                        
                    </td>
                    <td>
                        
                                    
                <%        
                    
                          
       float t53milh=tr.suma_tasahip(2000, 3000, 0.7, 0.8);
       float t53milc=tr.suma_tasacomp(2000, 3000, 0.7, 0.8);
       float t53mil= t53milh +t53milc;
       float c53milh=tr.sum_saldosh(2000, 3000, 0.7, 0.8);
       float c53milc=tr.sum_saldocom(2000, 3000, 0.7, 0.8);
       float c53mil=c53milh+c53milc;
       float o53milh=tr.cuenta_hip(2000, 3000, 0.7, 0.8);
       float o53milc=tr.cuenta_comp(2000, 3000, 0.7, 0.8);
       o53milc=o53milc*2;
       float o53mil=o53milh+o53milc;

    out.println( t53mil+"<br>");      
    out.println(  c53mil+"<br>" );                                                  
    out.println(  o53mil);  
            
                        
                  %>     
                        
                        
                        
                    </td>
                    <td>
                        
                        
         <%



                     
       float t55milh=tr.suma_tasahip( 3000,5000, 0.7, 0.8);
       float t55milc=tr.suma_tasacomp( 3000,5000, 0.7, 0.8);
       float t55mil= t55milh +t55milc;
       float c55milh=tr.sum_saldosh( 3000,5000, 0.7, 0.8);
       float c55milc=tr.sum_saldocom( 3000,5000, 0.7, 0.8);
       float c55mil=c55milh+c55milc;
       float o55milh=tr.cuenta_hip( 3000,5000, 0.7, 0.8);
       float o55milc=tr.cuenta_comp( 3000,5000, 0.7, 0.8);
       o55milc=o55milc*2;
       float o55mil=o55milh+o55milc;

    out.println( t55mil+"<br>");      
    out.println(  c55mil+"<br>" );                                                  
    out.println(  o55mil);  
                                                    
    %>                    
                        
                        
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>80%</td>
                    <td>90%</td>
                    <td>Valor Total de la última Tasación disponible de la garantía (UF)<bR>
                    Colocación Total (UF)<br>
                    Número de operaciones<br>
                   </td>
                    <td>
                   <%   
float t6500h=tr.suma_tasahip(0, 500, 0.8, 0.9) ;
float t6500c=tr.suma_tasacomp(0, 500, 0.8, 0.9) ;
float t6500= t6500c+ t6500h;
float c6500h=tr.sum_saldosh(0, 500, 0.8, 0.9) ;
float c6500c=tr.sum_saldocom(0, 500, 0.8, 0.9) ;
float c6500= c6500c+ c6500h;
float o6500h=tr.cuenta_hip(0, 500, 0.8, 0.9) ;
float o6500c=tr.cuenta_comp(0, 500, 0.8, 0.9) ;
 o6500c= o6500c*2;
float o6500= o6500c + o6500h;

           
   out.println(   t6500+"<br>");      
   out.println( c6500+"<br>" );   
   out.println(o6500);  
                       
                       %> 
                    </td>
                    <td>
                        
                        
                     <%        
                
float t6milh=tr.suma_tasahip(500, 1000, 0.8, 0.9);
float t6milc=tr.suma_tasacomp(500, 1000, 0.8, 0.9);
float t6mil= t6milc+ t6milh;
float c6milh=tr.sum_saldosh(500, 1000, 0.8, 0.9);
float c6milc=tr.sum_saldocom(500, 1000, 0.8, 0.9);
float c6mil= c6milc+ c6milh;
float o6milh=tr.cuenta_hip(500, 1000, 0.8, 0.9);
float o6milc=tr.cuenta_comp(500, 1000, 0.8, 0.9);

o6milc=o6milc*2;
float o6mil= o6milc + o6milh;

           
   out.println(   t6mil+"<br>");      
   out.println( c6mil+"<br>" );   
   out.println(o6mil);  
       
                  %>        
                        
                        
                    </td>
                    <td>   <%        
                                           
                  
float t62milh=tr.suma_tasahip(1000, 2000, 0.8, 0.9);
float t62milc=tr.suma_tasacomp(1000, 2000, 0.8, 0.9);
float t62mil= t62milc+ t62milh;
float c62milh=tr.sum_saldosh(1000, 2000, 0.8, 0.9);
float c62milc=tr.sum_saldocom(1000, 2000, 0.8, 0.9);
float c62mil= c62milc+ c62milh;
float o62milh=tr.cuenta_hip(1000, 2000, 0.8, 0.9);
float o62milc=tr.cuenta_comp(1000, 2000, 0.8, 0.9);
o62milc=o62milc*2;
float o62mil= o62milc + o62milh;

           
   out.println(   t62mil+"<br>");      
   out.println( c62mil+"<br>" );   
   out.println(o62mil);  
                        
                  %>     </td>
                    <td>
                        
                        
                         <%        
                   
float t63milh=tr.suma_tasahip(2000, 3000, 0.8, 0.9);
float t63milc=tr.suma_tasacomp(2000, 3000, 0.8, 0.9);
float t63mil= t63milc+ t63milh;
float c63milh=tr.sum_saldosh(2000, 3000, 0.8, 0.9);
float c63milc=tr.sum_saldocom(2000, 3000, 0.8, 0.9);
float c63mil= c63milc+ c63milh;
float o63milh=tr.cuenta_hip(2000, 3000, 0.8, 0.9);
float o63milc=tr.cuenta_comp(2000, 3000, 0.8, 0.9);
float o63mil= o63milc + o63milh;

           
   out.println(   t63mil+"<br>");      
   out.println( c63mil+"<br>" );   
   out.println(o63mil);                                            
           
                  %>  
                        
                        
                        
                        
                        
                        
                        
                    </td>
                    <td>
                        
  <%        
 
                   
float t65milh=tr.suma_tasahip(3000,5000, 0.8, 0.9);
float t65milc=tr.suma_tasacomp(3000,5000, 0.8, 0.9);
float t65mil= t65milc+ t65milh;
float c65milh=tr.sum_saldosh(3000,5000, 0.8, 0.9);
float c65milc=tr.sum_saldocom(3000,5000, 0.8, 0.9);
float c65mil= c65milc+ c65milh;
float o65milh=tr.cuenta_hip(3000,5000, 0.8, 0.9);
float o65milc=tr.cuenta_comp(3000,5000, 0.8, 0.9);
float o65mil= o65milc + o65milh;

           
   out.println(   t65mil+"<br>");      
   out.println( c65mil+"<br>" );   
   out.println(o65mil);                  
       %>  
                        
                                               
                        
                        
                        
                        
                        
                    </td>
                    <td></td>
                </tr>
                
                <tr>
                    <td colspan="2">>90%</td>
                    <td>Valor Total de la última Tasación disponible de la garantía (UF)<bR>
                    Colocación Total (UF)<br>
                    Número de operaciones<br>
                  </td>
                    <td>
                          <%    

float t7500h=tr.suma_tasahip(0, 500, 0.9, 1.7);
     float t7500c=tr.suma_tasacomp(0, 500, 0.9, 1.7);   
float t7500= t7500c+ t7500h;
float c7500h=tr.sum_saldosh(0, 500, 0.9, 1.7); 
float c7500c=tr.sum_saldocom(0, 500, 0.9, 1.7); 
float c7500 = c7500c+ c7500h;
float o7500h=tr.cuenta_hip(0, 500, 0.9, 1.7); 
float o7500c=tr.cuenta_comp(0, 500, 0.9, 1.7); 
 float o7500 = o7500c + o7500h;
                  
   out.println(  t7500+"<br>");      
   out.println(  c7500+"<br>" );   
   out.println(o7500);  
                       
                       %> 
                        
                    </td>
                    <td>
                        
                        
                              
                     <%        
           


float t7milh=tr.suma_tasahip(500, 1000, 0.9, 1.7);
     float t7milc=tr.suma_tasacomp(500, 1000, 0.9, 1.7);  
float t7mil= t7milc+ t7milh;
float c7milh=tr.sum_saldosh(500, 1000, 0.9, 1.7);
float c7milc=tr.sum_saldocom(500, 1000, 0.9, 1.7);
float c7mil = c7milc+ c7milh;
float o7milh=tr.cuenta_hip(500, 1000, 0.9, 1.7); 
float o7milc=tr.cuenta_comp(500, 1000, 0.9,1.7);
 float o7mil = o7milc + o7milh;
                  
   out.println(  t7mil+"<br>");      
   out.println(  c7mil+"<br>" );   
   out.println(o7mil);  

 
                  %>     
                        
                        
                        
                    </td>
                    <td>
                        
                                   
                     <%        
     

float t72milh=tr.suma_tasahip(1000, 2000, 0.9, 1.7);
     float t72milc=tr.suma_tasacomp(1000, 2000, 0.9, 1.7);
float t72mil= t72milc+ t72milh;
float c72milh=tr.sum_saldosh(1000, 2000, 0.9, 1.7);
float c72milc=tr.sum_saldocom(1000, 2000, 0.9, 1.7);
float c72mil = c72milc+ c72milh;
float o72milh=tr.cuenta_hip(1000, 2000, 0.9, 1.7);
float o72milc=tr.cuenta_comp(1000, 2000, 0.9,1.7);
 float o72mil = o72milc + o72milh;
                  
   out.println(  t72mil+"<br>");      
   out.println(  c72mil+"<br>" );   
   out.println(o72mil);  

                 %>     
                        
                        
                    </td>
                    <td>                  
                     <%        
  


float t73milh=tr.suma_tasahip(2000,3000, 0.9, 1.7);
     float t73milc=tr.suma_tasacomp(2000,3000, 0.9,1.7);
float t73mil= t73milc+ t73milh;
float c73milh=tr.sum_saldosh(2000,3000, 0.9, 1.7);
float c73milc=tr.sum_saldocom(2000,3000, 0.9, 1.7);
float c73mil = c73milc+ c73milh;
float o73milh=tr.cuenta_hip(2000,3000, 0.9, 1.7);
float o73milc=tr.cuenta_comp(2000,3000, 0.9,1.7);
 float o73mil = o73milc + o73milh;
                  
   out.println(  t73mil+"<br>");      
   out.println(  c73mil+"<br>" );   
   out.println(o73mil);  

 
                        
                  %>  </td>
                    <td>  <%        
    

float t75milh=tr.suma_tasahip(3000,5000, 0.9, 1.7);
     float t75milc=tr.suma_tasacomp(3000,5000, 0.9, 1.7);
float t75mil= t75milc+ t75milh;
float c75milh=tr.sum_saldosh(3000,5000, 0.9, 1.7);
float c75milc=tr.sum_saldocom(3000,5000, 0.9, 1.7);
float c75mil = c75milc+ c75milh;
float o75milh=tr.cuenta_hip(3000,5000, 0.9, 1.7);
float o75milc=tr.cuenta_comp(3000,5000, 0.9,1.7);
 float o75mil = o75milc + o75milh;
                  
   out.println(  t75mil+"<br>");      
   out.println(  c75mil+"<br>" );   
   out.println(o75mil);  
                
                  %></td>
                    
                    <td></td>
                </tr>
            </tbody>
        </table>

        
        
        
    </body>
</html>
         
     