<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import ="clases.tramos1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<title>Banco Ripley</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="css/style.css" />
</head>

<body>

<div id="container">
	<ul>
            <jsp:include page="menu.jsp" />
    <div id="logo"><a href="#"><img src="images/logoripley.png" width="226" height="50" alt="" /></a></div>    
        </ul>
      <br />
  <!-- cierre del "h_navcontainer" -->
  <div id="page_top">
  </div>
  <div id="page" >
    <h1>Cartera Hipotecaria Residencial</h1>     
        <div class="items" >       
          <!-- Pagina 2 de Ingreso Propiedad -->
          <div class="page">
            <form id="SignupForm" action="" method="post">
             
             <table border="1" bordercolor="black">
            <thead>
                <tr>
                    <th colspan="2">Tramo Loan to Value %</th>
                    <th>Informarcion </th>
                    <th>0-500</th>
                    <th>500-1000</th>
                    <th>1000-2000</th>
                    <th>2000-3000</th>
                    <th>3000-5000</th>
                    <th>>5000</th> 
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td >0%</td>
                    <td>20%</td>
                    <td>Valor Total de la ultima Tasacion disponibkle de la garantia (UF)<bR>
                    Colocacion Total (UF)<br>
                    Numero de operacionez<br>
                    </td>
                    <td>
                        <%
 tramos1 tr = new tramos1();   
 
 float sts1=tr.sum_tasaciones(0, 500, 0, 0.2);
 float sc1=tr.sum_creditos(0, 500, 0, 0.2);
 float  cop1=tr.contar_op(0,500,0,0.2)  ;    


out.println(sts1+"<br>"); 
out.println(sc1+"<br>"); 
out.println(cop1);         
%>
                    </td>
                    <td>                        <%
 
 
 float sts1_1=tr.sum_tasaciones(500, 1000, 0, 0.2);
 float sc1_1=tr.sum_creditos(500, 1000, 0, 0.2);
 float  cop1_1=tr.contar_op(500,1000,0,0.2)  ;    


out.println(sts1_1+"<br>"); 
out.println(sc1_1+"<br>"); 
out.println(cop1_1);         
%></td>
                    <td>
                        
                        
                <%        
                        float sts2_1=tr.sum_tasaciones(1000, 2000, 0, 0.2);
 float sc2_1=tr.sum_creditos(1000, 2000, 0, 0.2);
 float  cop2_1=tr.contar_op(1000,2000,0,0.2)  ;    


out.println(sts2_1+"<br>"); 
out.println(sc2_1+"<br>"); 
out.println(cop2_1);         
%>
                        
                        
                        
                        
                        
                    </td>
                    
     <td>
         
         
             
                <%        
                        float sts3_1=tr.sum_tasaciones(2000, 3000, 0, 0.2);
 float sc3_1=tr.sum_creditos(2000, 3000, 0, 0.2);
 float  cop3_1=tr.contar_op(2000, 3000,0,0.2)  ;    


out.println(sts3_1+"<br>"); 
out.println(sc3_1+"<br>"); 
out.println(cop3_1);         
%>
                         
         
         
         
         
         
         
         
     </td>
                    <td>
  
                <%        
                        float ttasa1=tr.sum_tasaciones(3000, 5000, 0, 0.2);
 float tccred1=tr.sum_creditos(3000, 5000, 0, 0.2);
 float  cuentaop1=tr.contar_op(3000, 5000,0,0.2)  ;    


out.println(ttasa1+"<br>"); 
out.println(tccred1+"<br>"); 
out.println(cuentaop1);         
%>
                         
                                
                        
                        
                        
                        
                        
                        
                        
                        
                        
                    </td>
                    <td></td>
                    
                </tr>
                <tr>
                    <td>20%</td>
                    <td>40%</td>
                    <td>Valor Total de la ultima Tasacion disponibkle de la garantia (UF)<bR>
                    Colocacion Total (UF)<br>
                    Numero de operacionez<br>
                   </td>
                    <td>
                        <%
float cop2=tr.contar_op(0, 500, 0.2, 0.4) ;  
float sts2=tr.sum_tasaciones(0, 500, 0.2, 0.4);
float sc2=tr.sum_creditos( 0,500, 0.2, 0.4);
out.println(sts2+"<br>"); 
   out.println(sc2+"<br>");  
out.println(cop2);  

%>
                        
                        
                    </td>
                    <td>
                        
                         <%
 float sts1_2=tr.sum_tasaciones(500, 1000, 0.2, 0.4);
 float sc1_2=tr.sum_creditos(500, 1000, 0.2, 0.4);
 float  cop1_2=tr.contar_op(500,1000,0.2,0.4)  ;    


out.println(sts1_2+"<br>"); 
out.println(sc1_2+"<br>"); 
out.println(cop1_2 );   
                        
                        %>
                        
                    </td>
                    <td>
                        
                       
                         <%
 float sts2_2=tr.sum_tasaciones(1000, 2000, 0.2, 0.4);
 float sc2_2=tr.sum_creditos(1000, 2000, 0.2, 0.4);
 float  cop2_2=tr.contar_op(1000,2000,0.2,0.4)  ;    


out.println(sts2_2+"<br>"); 
out.println(sc2_2+"<br>"); 
out.println(cop2_2 );   
                        
                        %>
                           
                        
                        
                    </td>
    <td>
        
  
                         <%
 float sts4_2=tr.sum_tasaciones(2000, 3000, 0.2, 0.4);
 float sc4_2=tr.sum_creditos(2000, 3000, 0.2, 0.4);
 float  cop4_2=tr.contar_op(2000, 3000,0.2,0.4)  ;    


out.println(sts4_2+"<br>"); 
out.println(sc4_2+"<br>"); 
out.println(cop4_2 );   
                        
                        %>
                                 
        
        
        
        
    </td>
                    <td>
                        
         
                         <%
 float t1tasa2=tr.sum_tasaciones( 3000,5000, 0.2, 0.4);
 float t1cred2=tr.sum_creditos( 3000,5000, 0.2, 0.4);
 float  t1cop2=tr.contar_op( 3000,5000,0.2,0.4)  ;    


out.println(t1tasa2+"<br>"); 
out.println(t1cred2+"<br>"); 
out.println(t1cop2 );   
                        
                        %>
                                 
        
                       
                        
                        
                        
                        
                        
                        
                        
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>40%</td>
                    <td>60%</td>
                   <td>Valor Total de la ultima Tasacion disponibkle de la garantia (UF)<bR>
                    Colocacion Total (UF)<br>
                    Numero de operacionez<br>
                   </td>
                   <td><%
                       float cop3=tr.contar_op(0, 500, 0.4, 0.6) ;   
float sts3=tr.sum_tasaciones( 0, 500, 0.4, 0.6);  
float sc3 =tr.sum_creditos(0, 500, 0.4, 0.6)   ;


out.println(sts3+"<br>");
out.println(sc3+"<br>");
                           
out.println(cop3); %>  
</td>
                    <td>
           <%             
                        
             float sts1_3=tr.sum_tasaciones(500, 1000, 0.4, 0.6);
 float sc1_3=tr.sum_creditos(500, 1000, 0.4, 0.6);
 float  cop1_3=tr.contar_op(500,1000,0.4,0.6)  ;    


out.println(sts1_3+"<br>"); 
out.println(sc1_3+"<br>"); 
out.println(cop1_3 );              
                        
                  %>      
                        
                        
                    </td>
                    <td>
                        
                  <%             
                        
             float sts2_3=tr.sum_tasaciones(1000, 2000, 0.4, 0.6);
 float sc2_3=tr.sum_creditos(1000, 2000, 0.4, 0.6);
 float  cop2_3=tr.contar_op(1000,2000,0.4,0.6)  ;    


out.println(sts2_3+"<br>"); 
out.println(sc2_3+"<br>"); 
out.println(cop2_3 );              
                        
                  %>          
                        
                        
                        
                        
                        
                    </td>
                    <td>
                    <%             
                        
             float sumatasa=tr.sum_tasaciones(2000, 3000, 0.4, 0.6);
 float sumac=tr.sum_creditos(2000, 3000, 0.4, 0.6);
 float  cuentac=tr.contar_op(2000, 3000,0.4,0.6)  ;    


out.println(sumatasa+"<br>"); 
out.println(sumac+"<br>"); 
out.println(cuentac );              
                        
                  %>          
                        
                             
                        
                        
                        
                        
                        
                        
                        
                        
                        
                    </td>
                    <td>
                        
                        
                         <%             
                        
             float ts3=tr.sum_tasaciones(3000,5000, 0.4, 0.6);
 float cr3=tr.sum_creditos(3000,5000, 0.4, 0.6);
 float  cta3=tr.contar_op(3000,5000,0.4,0.6)  ;    


out.println(ts3+"<br>"); 
out.println(cr3+"<br>"); 
out.println(cta3 );              
                        
                  %>          
                           
                        
                        
                        
                        
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>60%</td>
                    <td>70%</td>
                    <td>Valor Total de la ultima Tasacion disponibkle de la garantia (UF)<bR>
                    Colocacion Total (UF)<br>
                    Numero de operacionez<br>
                   </td>
                    <td>
                       <% float cop4=tr.contar_op(0, 500, 0.6, 0.7) ;  
float sts4=tr.sum_tasaciones( 0, 500, 0.6, 0.7);  
float sc4 =tr.sum_creditos(0, 500, 0.6, 0.7)   ;
   out.println(  sts4+"<br>");      
   out.println( sc4+"<br>" );                                                  
out.println( cop4);  
  %> 
                        
                        
                        
                    </td>
                    <td>
                        
                        <%             
                        
             float sts1_4=tr.sum_tasaciones(500, 1000, 0.6, 0.7);
 float sc1_4=tr.sum_creditos(500, 1000, 0.6, 0.7);
 float  cop1_4=tr.contar_op(500,1000,0.6,0.7)  ;    


out.println(sts1_4+"<br>"); 
out.println(sc1_4+"<br>"); 
out.println(cop1_4 );              
                        
                  %> 
                        
                        
                    </td>
                    <td>
                       <%             
                        
             float sts2_4=tr.sum_tasaciones(1000, 2000, 0.6, 0.7);
 float sc2_4=tr.sum_creditos( 1000,2000, 0.6, 0.7);
 float  cop2_4=tr.contar_op(1000,2000,0.6,0.7)  ;    


out.println(sts2_4+"<br>"); 
out.println(sc2_4+"<br>"); 
out.println(cop2_4 );              
                        
                  %>              
                        
                        
                        
                        
                        
                        
                    </td>
                    <td>
                        
                <%             
                        
             float sumatasa2=tr.sum_tasaciones(2000, 3000, 0.6, 0.7);
 float sumac2=tr.sum_creditos( 2000,3000, 0.6, 0.7);
 float cuentac2=tr.contar_op(2000,3000,0.6,0.7)  ;    


out.println(sumatasa2+"<br>"); 
out.println(sumac2+"<br>"); 
out.println(cuentac2 );              
                        
                  %>              
                        
                    </td>
                    <td>
             <%             
                        
             float ts4=tr.sum_tasaciones(3000,5000, 0.6, 0.7);
 float cr4=tr.sum_creditos( 3000,5000, 0.6, 0.7);
 float cta4=tr.contar_op(3000,5000,0.6,0.7)  ;    


out.println(ts4+"<br>"); 
out.println(cr4+"<br>"); 
out.println(cta4 );              
                        
                  %>                 
                        
                        
                        
                        
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>70%</td>
                    <td>80%</td>
                    <td>Valor Total de la ultima Tasacion disponibkle de la garantia (UF)<bR>
                    Colocacion Total (UF)<br>
                    Numero de operacionez<br>
                   </td>
                    <td>
                       <%
float cop5=tr.contar_op(0, 500, 0.7, 0.8) ;                     

float sts5=tr.sum_tasaciones( 0, 500, 0.7, 0.8);  
float sc5 =tr.sum_creditos(0, 500, 0.7, 0.8)   ;
   out.println(  sts5+"<br>");      
   out.println( sc5+"<br>" );                                                  
out.println( cop5);  




  
                       



%> 
                        
                        
                        
                    </td>
                    <td>
                        
                <%        
                                           
             float sts1_5=tr.sum_tasaciones(500, 1000, 0.7, 0.8);
 float sc1_5=tr.sum_creditos(500, 1000, 0.7, 0.8);
 float  cop1_5=tr.contar_op(500,1000,0.7,0.8)  ;    


out.println(sts1_5+"<br>"); 
out.println(sc1_5+"<br>"); 
out.println(cop1_5 );              
                        
                  %> 
                        
                        
                    </td>
                    <td>
                        
                              
                <%        
                                           
             float sts2_5=tr.sum_tasaciones(1000, 2000, 0.7, 0.8);
 float sc2_5=tr.sum_creditos(1000, 2000, 0.7, 0.8);
 float  cop2_5=tr.contar_op(1000,2000,0.7,0.8)  ;    


out.println(sts2_5+"<br>"); 
out.println(sc2_5+"<br>"); 
out.println(cop2_5 );              
                        
                  %> 
                           
                        
                        
                        
                    </td>
                    <td>
                        
                                    
                <%        
                                           
             float sumatasa3=tr.sum_tasaciones(2000, 3000, 0.7, 0.8);
 float sumac3=tr.sum_creditos(2000, 3000, 0.7, 0.8);
 float  cuentaop3=tr.contar_op(2000,3000,0.7,0.8)  ;    


out.println(sumatasa3+"<br>"); 
out.println(sumac3+"<br>"); 
out.println(cuentaop3 );              
                        
                  %>     
                        
                        
                        
                    </td>
                    <td>
                        
                        
         <%        
                                           
 float ts5=tr.sum_tasaciones(5000,3000, 0.7, 0.8);
 float cr5=tr.sum_creditos(5000,3000, 0.7, 0.8);
 float cta5=tr.contar_op(5000,3000,0.7,0.8)  ;    


out.println(ts5+"<br>"); 
out.println(cr5+"<br>"); 
out.println(cta5 );              
                        
                  %>                    
                        
                        
                    </td>
                    <td></td>
                </tr>
                <tr>
                    <td>80%</td>
                    <td>90%</td>
                    <td>Valor Total de la ultima Tasacion disponibkle de la garantia (UF)<bR>
                    Colocacion Total (UF)<br>
                    Numero de operacionez<br>
                   </td>
                    <td>
                   <%     
                   float cop6=tr.contar_op(0, 500, 0.8, 0.9) ;


                   

float sts6=tr.sum_tasaciones( 0, 500, 0.8, 0.9);  
float sc6 =tr.sum_creditos(0, 500, 0.8, 0.9)   ;
   out.println(  sts6+"<br>");      
   out.println( sc6+"<br>" );   


    out.println(cop6);  
                       
                       %> 
                    </td>
                    <td>
                        
                        
                     <%        
                                           
             float sts1_6=tr.sum_tasaciones(500, 1000, 0.8, 0.9);
 float sc1_6=tr.sum_creditos(500, 1000, 0.8, 0.9);
 float  cop1_6=tr.contar_op(500,1000,0.8,0.9)  ;    


out.println(sts1_6+"<br>"); 
out.println(sc1_6+"<br>"); 
out.println(cop1_6 );              
                        
                  %>        
                        
                        
                    </td>
                    <td>   <%        
                                           
             float sts2_6=tr.sum_tasaciones(1000, 2000, 0.8, 0.9);
 float sc2_6=tr.sum_creditos(1000, 2000, 0.8, 0.9);
 float  cop2_6=tr.contar_op(1000,2000,0.8,0.9)  ;    


out.println(sts2_6+"<br>"); 
out.println(sc2_6+"<br>"); 
out.println(cop2_6 );              
                        
                  %>     </td>
                    <td>
                        
                        
                         <%        
                                           
             float sumatasac4=tr.sum_tasaciones(2000, 3000, 0.8, 0.9);
 float sumac4=tr.sum_creditos(2000, 3000, 0.8, 0.9);
 float  cuenta4=tr.contar_op(2000,3000,0.8,0.9)  ;    


out.println(sumatasac4+"<br>"); 
out.println(sumac4+"<br>"); 
out.println(cuenta4 );              
                        
                  %>  
                        
                        
                        
                        
                        
                        
                        
                    </td>
                    <td>
                        
  <%        
                                           
             float ts6=tr.sum_tasaciones(3000,5000, 0.8, 0.9);
 float cr6=tr.sum_creditos(3000,5000, 0.8, 0.9);
 float  cta6=tr.contar_op(3000,5000,0.8,0.9)  ;    


out.println(ts6+"<br>"); 
out.println(cr6+"<br>"); 
out.println( cta6 );              
                        
                  %>  
                        
                                               
                        
                        
                        
                        
                        
                    </td>
                    <td></td>
                </tr>
                
                <tr>
                    <td colspan="2">>90%</td>
                    <td>Valor Total de la ultima Tasacion disponibkle de la garantia (UF)<bR>
                    Colocacion Total (UF)<br>
                    Numero de operacionez<br>
                  </td>
                    <td>
                         <%     
                   float cop7=tr.contar_op(0, 500, 0.9, 1.0) ;  
                 

                   
float sts7=tr.sum_tasaciones( 0, 500, 0.9, 1.0);  
float sc7 =tr.sum_creditos(0, 500, 0.9, 1.0)   ;
   out.println(  sts7+"<br>");      
   out.println( sc7+"<br>" );   
   out.println(cop7);  
                       
                       %> 
                        
                    </td>
                    <td>
                        
                        
                              
                     <%        
                                           
             float sts1_7=tr.sum_tasaciones(500, 1000, 0.9, 1.0);
 float sc1_7=tr.sum_creditos(500, 1000, 0.9, 1.0);
 float  cop1_7=tr.contar_op(500,1000,0.9,1.0)  ;    


out.println(sts1_7+"<br>"); 
out.println(sc1_7+"<br>"); 
out.println(cop1_7 );              
                        
                  %>     
                        
                        
                        
                    </td>
                    <td>
                        
                                   
                     <%        
                                           
             float sts2_7=tr.sum_tasaciones(1000, 2000, 0.9, 1.0);
 float sc2_7=tr.sum_creditos(1000, 2000, 0.9, 1.0);
 float  cop2_7=tr.contar_op(1000,2000,0.9,1.0)  ;    


out.println(sts2_7+"<br>"); 
out.println(sc2_7+"<br>"); 
out.println(cop2_7 );              
                        
                  %>     
                        
                        
                    </td>
                    <td>                  
                     <%        
                                           
             float sumatasac5=tr.sum_tasaciones(1000, 2000, 0.9, 1.0);
 float sumac5=tr.sum_creditos(1000, 2000, 0.9, 1.0);
 float  cuenta5=tr.contar_op(1000,2000,0.9,1.0)  ;    


out.println( sumatasac5+"<br>"); 
out.println(sumac5+"<br>"); 
out.println(cuenta5 );              
                        
                  %>  </td>
                    <td>  <%        
                                           
             float ts7=tr.sum_tasaciones(1000, 2000, 0.9, 1.0);
 float cr7=tr.sum_creditos(1000, 2000, 0.9, 1.0);
 float  cta7=tr.contar_op(1000,2000,0.9,1.0)  ;    


out.println( ts7+"<br>"); 
out.println(cr7+"<br>"); 
out.println( cta7 );              
                        
                  %></td>
                    
                    <td></td>
                </tr>
            </tbody>
        </table>

        
        
        
    </body>

                    
            </form>
          </div>
        </div>
        <!--items-->
        

      
    <div style="clear:both"></div>     
    <br></br>
    <br></br>
  </div>
   <!-- close page -->
  <div id="page_bottom"></div>
  
</div>
<!-- close container -->

<!-- close footer -->
</body>
</html>