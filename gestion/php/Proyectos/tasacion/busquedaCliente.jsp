<%@page contentType="text/html" pageEncoding="utf-8"%>
<%@page errorPage="paginaError.jsp" %>
<%@page import ="java.util.*"%>
<%@page import="java.sql.*" %> 
<%@page import ="java.util.Collections"%>
<%@page import ="beans.*"%>
<jsp:useBean id="datoscliente" scope="request"   class="beans.clienteDAO" />
<jsp:useBean id='datospropiedad' scope='request'   class='beans.propiedadDAO' />
<jsp:useBean id="datoscbr" scope="request" class="beans.cbrDAO" />
<jsp:useBean id="escritura" scope="request" class="beans.escrituraDAO" ></jsp:useBean>
<jsp:useBean id="cpventa" scope="request" class="beans.cpravtaDAO" ></jsp:useBean>
<jsp:useBean id="tasacion" scope="request" class="beans.tasacionDAO"></jsp:useBean>
<jsp:useBean id="credito" scope="request" class="beans.creditoDAO"></jsp:useBean>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
          <%  HttpSession hs = request.getSession(true);
          

if (hs.getAttribute("nombreUsuario")==null)
{
response.sendRedirect("Login.html");                              
}


%>

<title>Banco Ripley</title>

<link rel="stylesheet" type="text/css" href="css/style.css" />
<link rel="stylesheet" type="text/css" href="detalles/js/jquery.fancybox.css" />   
<script src="js/jquery.js" type="text/javascript"></script> 
<%      
String rut=request.getParameter("rut");
String rol = request.getParameter("rol");

%>
</head>

<body>

<div id="container">
	<ul>
            <jsp:include page="menu.jsp" />
    <div id="logo"><a href="ingresoCliente.jsp"><img src="images/logoripley.png" width="226" height="50" alt="" /></a></div>    
        </ul>
      <br />
  <!-- cierre del "h_navcontainer" -->
  <div id="page_top">
	
	 <!--	************* implementacion del menu pendiente*************
	<div id="sesion_bg">
    <div id="sesion">
    <div id="sesion_derecha">
      <ul>
        <li class="footer_heading">
          <h4>Usuario:</h4>
        </li>
        <div align="center">
        <li><img src="photos/sesion.jpg" width="40" height="40" alt="" /></li>
        </div>
        <li>Null</li>
        <li><input type="text" class="text" name="Usuario" /></li>
        <li>Logout</li>
        <li><input type="text" class="text" name="Contraseña" /></li>   
    </div>
  </div>
  <!-- cierra sesion_bg 
</div>
-->

  </div>
  <div id="page" >
    <h1>Inspeccionar Clientes</h1>     
      <div id="wizard2">
          
        <div class="items" >       
          <!-- Pagina 2 de Ingreso Propiedad -->
          <div class="page">
            <form id="SignupForm" action=""> 
        
      
              <li class="required double">
                    <% 
        
             datoscliente.setRut(rut);
          
              List lista=datoscliente.getLista();
            

                                 
            beanCliente cliente;
           
              
              for(int i=0;i<lista.size();i++)  {
              cliente=(beanCliente)lista.get(i);
              
              
             %>
                  <legend><font color="#990000"><strong>Informacion Cliente</strong></font></legend>      
             <label> Nombre:         
            <input  type="text" class="text"  id="nombre" name="nombre" maxlength="10"  value="<%=cliente.getNombre() %>"/>
             </label>
                 <% } %>
                 <br/>
          <li class="clearfix">
                <div align="right"> <a class="Cliente" href="detalles/Cliente.jsp?rut=<%=rut%>"> <input type= "submit" value="Ver Detalle" class="ver detalle"/></a></div>
            </li>
          </li>         
            <li class="required double"> 
                 <%          
          datospropiedad.setRol(rol);
             List listaprop =datospropiedad.getPropiedad();
           beanPropiedad prop;
               for(int k=0;k<listaprop.size();k++)  {
              prop=(beanPropiedad)listaprop.get(k);              
             %>
           <legend><font color="#990000"><strong>Informacion Propiedad</strong></font></legend>
        <label>Rol:
        <input type="text" class="text"  id="rol" name="rol" maxlength="10" value="<%=prop.getRol()%>" />
        </label>
        <label> Tipo de Propiedad
        <input type="text" id="tipo" class="text" name="tipoprop" value="<%=prop.getTipoprop()%>" maxlength="20">   
          </label> 
          <% } %>
            <li class="clearfix">
                <div align="right"> <a class="Propiedad" href="detalles/Propiedad.jsp?rol=<%=rol%>&rut=<%=rut%>"> <input type= "submit" id="SaveAccount2" value="Ver Detalle" class="ver detalle"/></a></div>
            </li>
            </li>
       <li class="required double"> 
            <% 
         
            // String r =datoscbr.getRol(rut);
          datoscbr.setRol(rol);
         
              List listacbr =datoscbr.getCBR();
             
            beanCBR cb ;
           
              
              for(int j=0;j<listacbr.size();j++)  {
              cb=(beanCBR)listacbr.get(j);
              
              
             %>
       <legend><font color="#990000"><strong>Informacion CBR</strong></font></legend>
        <label> Fojas:
        <input type="text" class="text" name="folio" maxlength="10" value="<%=cb.getCfojas() %>">
        </label>
        <label> Numero de Fojas:
        <input type="text"  class="text" name="nfojas"  maxlength="20" value="<%=cb.getNregistro()%>"> 
        </label> 
        <label> Numero de Repertorio:
        <input type="text"  class="text" name="rptiocbr"  maxlength="20" value="<%=cb.getNrptiof()  %>">   
        </label> 
           <label> Fecha CBR:
        <input type="text" id="nfojas" class="text" name="fcbr"  maxlength="20" value="<%=cb.getFecha() %>">   
        </label> 
        <% } %>     
        <li class="clearfix">
        <div align="right"><a  class="CBR" href="detalles/CBR.jsp?rol=<%=rol%>"   > <input  type="submit" value=" Ver Detalle " /></a></div>
        </li>
        </li>
        <li class="required double">
            <%   
            //  String rl =datoscbr.getRol(rut);
              escritura.setRol(rol);
              List li =escritura.getEscritura();
             
              beanEscritura es ;
           
              
              for(int x=0;x<li.size();x++)  {
              es=(beanEscritura)li.get(x);
              String  var =es.getAplica();
              if (var.equals("S"))
                         {
            
         %>
             
         <legend><font color="#990000"><strong>Escritura</strong></font></legend>
        <label for="Aplica">Aplica: Si<input type="radio" checked="checked" disabled="disabled" name="aplica" value="si"> &nbsp;&nbsp;No<input type="radio" name="aplica"  disabled="disabled" value="no" />
         </label>
              <% }else{ %>
         <legend><font color="#990000"><strong>Escritura</strong></font></legend> 
         <label for="Aplica">Aplica:
         Si<input type="radio"  disabled="disabled" name="aplica" value="si"> &nbsp;&nbsp;No<input type="radio" name="aplica" checked="checked" disabled="disabled" value="no" />
         </label>
             <%
         
             }
                 }
     %>
        </li>
        
       <li class="required double"> 
            <% 
             
             cpventa.setRol(rol);
          
              List lcpavta=cpventa.getCpraventa();
             
            beanCompraventa cvta;
           
              
              for(int i=0;i<lcpavta.size();i++)  {
              cvta=(beanCompraventa)lcpavta.get(i);
              
              
             %>
       <legend><font color="#990000"><strong>Compra Venta</strong></font></legend>
  <label>Notaria :
         <input type="text" class="text"  id="nrepertorio"name="nrepertorio" maxlength="10" value="<%=cvta.getC_notaria() %>"/>
         </label>
         <label>Fecha Notaria:
         <input type="text" class="text" name="fcompraventa"  value="<%=cvta.getFcompra() %>" >
         </label> 
         <% } %>
         <li class="clearfix">
             <div align="right"><a  class="cpvta" href="detalles/CompraVenta.jsp?rol=<%=rol%>"><input id="SaveAccount" type="submit" value=" Ver Detalle " /> </a></div>
         </li>
         </li>
        <li class="required double"> 
            <% 
             
             tasacion.setRol(rol);
          
              List ltasacion=tasacion.getTasacion();
             
            beanTasacion tasa;
           
              
              for(int i=0;i<ltasacion.size();i++)  {
              tasa=(beanTasacion)ltasacion.get(i);
              
              
             %>
        <legend><font color="#990000"><strong>Infomación Tasación</strong></font></legend>
        <label>Empresa:
         <input  type="text" id="empresa" name="empresa" maxlength="10" class="text" value="<%=tasa.getEmpresa() %>" />
         </label>
         <label>Tasador:
         <input  type="text" class="text"  id="tasador" name="tasador" maxlength="10"  value="<%=tasa.getTasdor() %>" />
         </label>
             <label>Valor de Liquidez:   
             <input  type="text"    name="vliquidez" class="text"   value="<%=tasa.getVliquides() %>"/>
             </label>  
             <label>Fecha de Tasacion:
             <input type="text" name="ftasacion" class="text" value="<%=tasa.getFtasacion() %>">
             </label>
             <li class="clearfix">
                 <div align="right"><a class="inftasacion" href="detalles/InformacionTasacion.jsp?rol=<%=rol %>"><input name="SaveAccount2" type= "submit" id="SaveAccount2" value="Ver Detalle" class="ver detalle"/></a></div>
             </li>
            <% } %>
        </li>
         <li class="required double"> 
              <% 
             
             credito.setRol(rol);
          
              List lcredito =credito.getCredito();
             
            beanCredito cred;
           
              
              for(int ik=0;ik<lcredito.size();ik++)  {
              cred=(beanCredito)lcredito.get(ik);
              
              
             %>
        <legend><font color="#990000"><strong>Información Credito</strong></font></legend>
        
         <label>Credito Hipotecario:
         <input type="text"  id="chip" name="chip" maxlength="10"  class="text" value="<%=cred.getChipote() %>" />
         </label>
         <label>Estado Hipotecario:
         <input type="text" id="estahip" name="estahip" class="text" value="<%=cred.getEstadohip()%>"/>
         </label>
         <label>N° Operacion:
             <input type="text" id="nhip" name="nhip" class="text" value="<%=cred.getNhip() %>"/>
        </label>
        <label>Credito Complementario:
        <input type="text"  id="ccomp" name="ccomp" class="text" value="<%=cred.getCcomple() %>"/>
        </label>
        <label>Estado Complementario:
            <input type="text" id="estacomp" name="estacomp" class="text" value="<%=cred.getEstadocomp() %>"/>
        </label>
        <label>N° Operacion:
        <input type="text" id="ncomp" name="ncomp" class="text" value="<%=cred.getNcompl()%>"/>
        </label> 
        <li class="clearfix">
            <div align="right"><a class="infcredito" href="detalles/InformacionCredito.jsp?rol=<%=rol%>"> <input name="SaveAccount2" type= "submit" id="SaveAccount2" value="Ver Detalle" class="ver detalle"/></a></div>
        </li>
         <%}%>
        </li>
        </form>
        <script type="text/javascript" src="detalles/js/jquery-1.4.3.min.js"></script>  
<script type="text/javascript" src="detalles/js/jquery.fancybox.pack.js"></script>  
<link rel="stylesheet" type="text/css" href="detalles/js/jquery.fancybox.css" /> 


<script type="text/javascript">  
$(document).ready(function(){ 		   
    $(".Cliente").fancybox({  
        'overlayShow'	: false,
        'transitionIn'	: 'elastic',
        'transitionOut'	: 'elastic',  
        'width'         : 700,  
        'height'        : 480,  
        'type'          : 'iframe'
    });  
});  
</script>   
  <script type="text/javascript">  
$(document).ready(function(){ 		   
    $(".Propiedad").fancybox({  
        'overlayShow'	: false,
        'transitionIn'	: 'elastic',
        'transitionOut'	: 'elastic',  
        'width'         : 700,  
        'height'        : 600,  
        'type'          : 'iframe'
    });  
});  
</script> 

<script type="text/javascript">  
$(document).ready(function(){ 		   
    $(".cpvta").fancybox({  
        'overlayShow'	: false,
        'transitionIn'	: 'elastic',
        'transitionOut'	: 'elastic',  
        'width'         : 700,  
        'height'        : 330,  
        'type'          : 'iframe'
       
    });  
});  
</script> 


<script type="text/javascript">  
$(document).ready(function(){ 		   
    $(".CBR").fancybox({  
       'overlayShow'	: false,
        'transitionIn'	: 'elastic',
        'transitionOut'	: 'elastic',  
        'width'         : 700,  
        'height'        : 330,  
        'type'          : 'iframe'
      
    });  
});  
</script>
<script type="text/javascript">  
$(document).ready(function(){ 		   
    $(".inftasacion").fancybox({  
		'overlayShow'	: false,
		'transitionIn'	: 'elastic',
		'transitionOut'	: 'elastic',  
        'width'         : 680,  
        'height'        : 450,  
        'type'          : 'iframe',
        'scrolling'      : 'no'
    });  
});  
</script> 
<script type="text/javascript">  
$(document).ready(function(){ 		   
    $(".infcredito").fancybox({  
	'overlayShow'	: false,
	'transitionIn'	: 'elastic',
        'transitionOut'	: 'elastic',  
        'width'         : 680,  
        'height'        : 480,  
        'type'          : 'iframe'
    });  
});  
</script> 
          </div>
          <!-- ******************SE CIERRA EL DIV DE LA INFORMACION DEL CREDITO*******************************-->
        </div>
        <!--items-->
      </div>
      <!--wizard-->
    <div style="clear:both"></div>     
  </div>
   <!-- close page -->
  <div id="page_bottom">
      
  </div>
  
</div>
<!-- close container -->

<!-- close footer -->
</body>
</html>