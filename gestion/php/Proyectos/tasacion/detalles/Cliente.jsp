<%@page contentType="text/html" pageEncoding="utf-8"%>
<%@page import ="java.util.*"%>
<%@page import ="java.util.Collections"%>
<%@page import ="beans.*"%>
<jsp:useBean id="datoscliente" scope="request"   class="beans.clienteDAO" />


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
<% 
String rut=request.getParameter("rut");        
%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="stylesheet" type="text/css" href="style.css" />
<script src="jquery.js" type="text/javascript"></script>
<script src="jquery.validate.js" type="text/javascript"></script>
<script src="jquery.metadata.js" type="text/javascript"></script>

<style type="text/css">
#wizard2{
	background:#fff url(../images/h600.png) repeat scroll 0 0;
	border:5px solid #E9E9E9;
	font-size:12px;
	height:450px;
	float:left;
	width:95%;
	overflow:hidden;
	position: absolute;
	/* rounded corners for modern browsers */
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
}
/* scrollable items */
#wizard2 .items{
	width:20000em;
	clear:both;
	position:absolute;
}
/* single item */
#wizard2 .page{

	width:600px;
	float:left;
}
</style>
<title>Banco Ripley</title>
</head>
<body>
      <div id="wizard2">
         <div class="items" >       
          <div class="page">
            <form  id="TablaCliente" action="../upcliente" method="post" >
                
                <label> INFORMACION CLIENTE</label>
  <br></br>
  
  <li class="required double">  
       <%      
             datoscliente.setRut(rut);
              List lista=datoscliente.getLista();
            beanCliente cliente;
              for(int i=0;i<lista.size();i++)  {
              cliente=(beanCliente)lista.get(i);             
             %>
             <input type="hidden" value="<%=cliente.getRut() %>" name="rutcli"></input>
         <label>Rut
             
           <input 
               type="text"  id="rut" name="rut" maxlength="10"  disabled="disabled"  value="<%=cliente.getRut() %>" class="text" onblur="esrut(document.getElementById('rut').value)" tabindex="1" />
         </label>            
         <label>Nombre
           <input type="text"  id="nombre" name="nombre" maxlength="50"  class="text" value="<%=cliente.getNombre()  %>"   tabindex="1" />
         </label>
         <label>   
             <% out.print(cliente.getSexo());
                 if(cliente.getSexo().equals("M")) {     
             %>             		 
             <label>Sexo 
                 M<input type="radio" checked="checked" align="right"  name="sexo" value="M"> &nbsp;&nbsp;F<input type ="radio" name= "sexo" value="F"   />
             </label>
                     <% } else {%>
  
              <label>Sexo 
              M<input type="radio"  align="right"  name="sexo" value="M"> &nbsp;&nbsp;F<input type ="radio" name= "sexo" value="F" checked="checked"   />
              </label> 
             <%  } %>
         </label>
            <%  
         if (cliente.getEstadocivil().equals("soltero"))
            {
            %>
         <label>
         Estado Civil <select  name="civil">
         
		<OPTION VALUE= "soltero" selected="soltero">SOLTERO(A)</OPTION>
		<OPTION VALUE="casado">CASADO(A)</OPTION>
		<OPTION VALUE="separado">SEPARADO(A)</OPTION>
		<OPTION VALUE="viudo">VIUDO(A)</OPTION>

            </select>
			</label>
			 <% }else if(cliente.getEstadocivil().equals("casado")) {%>			
         <label>
         <Strong>Estado Civil:</Strong></td><td width="414" align="right"><select  name="civil">
         
		<OPTION VALUE= "soltero" >SOLTERO(A)</OPTION>
		<OPTION VALUE="casado"  selected="casado">CASADO(A)</OPTION>
		<OPTION VALUE="separado">SEPARADO(A)</OPTION>
		<OPTION VALUE="viudo">VIUDO(A)</OPTION>
                  </select> 
                      </label> 
			<%}else if(cliente.getEstadocivil().equals("separado") ) {%>
			   
                        <label>Estado Civil <select  name="civil">
         
		<OPTION VALUE= "soltero" >SOLTERO(A)</OPTION>
		<OPTION VALUE="casado"  >CASADO(A)</OPTION>
		<OPTION VALUE="separado"  selected="separado" >SEPARADO(A)</OPTION>
		<OPTION VALUE="viudo">VIUDO(A)</OPTION>
              </select> 
            </label> 
			<% } else if (cliente.getEstadocivil().equals("viudo")){ %>			
			   <label> Estado Civil <select  name="civil">
         
		<OPTION VALUE= "soltero" >SOLTERO(A)</OPTION>
		<OPTION VALUE="casado"  >CASADO(A)</OPTION>
		<OPTION VALUE="separado"   >SEPARADO(A)</OPTION>
		<OPTION VALUE="viudo" selected="viudo"  >VIUDO(A)</OPTION>
                 </select> 
            </label> 
			<% }%>
         <label> Telefono
              <input type="text"  id="fono" name="fono"  maxlength="50"class="text" value="<%=cliente.getTelefono() %>" tabindex="1" />
         </label> 
         <label> Region
              <input type="text"  id="rgion" name="rgion" maxlength="60" class="text" value="<%=cliente.getReg()   %>" tabindex="1"  />
         </label> 
         <label> Ciudad
              <input type="text"  id="ciuda" name="ciuda" maxlength="60" class="text" value="<%=cliente.getCiuda()%>" tabindex="1"  />
         </label> 
         <label> Comuna
              <input type="text"  id="comu" name="comu" maxlength="60" class="text" value="<%=cliente.getComu()  %>" tabindex="1"  />
         </label> 
         <label> Calle
              <input type="text"  id="calle" name="calle" maxlength="60" class="text" value="<%=cliente.getDir() %> " tabindex="1"  />
         </label> 
         <label> Correo:
              <input type="text"  id="email" name="email" maxlength="60" class="text" value="<%=cliente.getCorreo()  %>" tabindex="1"  />
         </label> 
         <% }%>
          </li>
          <div align="right">
          <li class="clearfix">
        <input type="Submit" value="Guardar" class="submitbutton" align="center" />
        </li>
          </div>
        </form>
          </div>
        </div>
        <!--items-->
      </div>
      <!--wizard-->
    <div style="clear:both"></div>     
    <br></br>
    <br></br>
</body>
</html>
