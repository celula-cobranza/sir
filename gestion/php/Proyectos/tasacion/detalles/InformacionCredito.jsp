<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page errorPage="paginaError.jsp" %>
<%@page import ="java.util.*"%>
<%@page import ="java.util.Collections"%>
<%@page import ="beans.*"%>
<jsp:useBean id="credito" scope="request" class="beans.creditoDAO"></jsp:useBean>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="stylesheet" type="text/css" href="style.css" />
<script src="jquery.js" type="text/javascript"></script>
<script src="jquery.validate.js" type="text/javascript"></script>
<script src="jquery.metadata.js" type="text/javascript"></script>
<%  

String rol = request.getParameter("rol");


%>
<style type="text/css">
#wizard2{
	background:#fff url(../images/h600.png) repeat scroll 0 0;
	border:5px solid #E9E9E9;
	font-size:12px;
	height:450px;
	float:right;
	width:95%;
	overflow:hidden;
	position: absolute;
	/* rounded corners for modern browsers */
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
}
/* scrollable items */
#wizard2 .items{
	width:20000em;
	clear:both;
	position:absolute;
}
/* single item */
#wizard2 .page{

	width:600px;
	float:left;
}
</style>
<title>Banco Ripley</title>
</head>
<body>
      <div id="wizard2">
         <div class="items" >       
          <div class="page">
            <form id="TablaCliente" action="../upcredito" method="post" >
          

    <label>INFORMACION CREDITO</label>
  <br></br>
    <li class="required double">  
  <% 
             credito.setRol(rol);
          
              List lcredito =credito.getCredito();
             
            beanCredito cred;
 
              for(int ik=0;ik<lcredito.size();ik++)  {
              cred=(beanCredito)lcredito.get(ik);

             %>    
             <input type="hidden" name="rolp"  value="<%=rol%>"> </input>
          <label>Rol
              <input type="text"  id="rol" name="rol" class="text"  value="<%=rol%>" disabled="disabled" maxlength="20"/>
         </label>            
         <label>Credito Hipotecario
           <input type="text"  id="chip" name="chip" class="text"value="<%=cred.getChipote() %>" maxlength="20"/>
         </label>
         <label>Operacion Hipotecario           
           <input type="text"  id="opehip" name="opehip" class="text" value="<%=cred.getNhip() %>" maxlength="20"/>
         </label>
         <label>Estado Hipotecario
         <label>
   <%   String esth =cred.getEstadohip();
      if(esth.equals("VENCIDO"))
       {   %>
            <select  name="esthipo" >
                <OPTION VALUE="VIGENTE"  >VIGENTE</OPTION> 
		<OPTION VALUE="VENCIDO" selected >VENCIDO</OPTION>
		<OPTION VALUE="CASTIGADO">CASTIGADO</OPTION>
            </select>
         
             <%}else if(esth.equals("VIGENTE"))
                         {%>          
                         <select  name="esthipo" >
                <OPTION VALUE="VIGENTE"  selected >VIGENTE</OPTION> 
		<OPTION VALUE="VENCIDO"  >VENCIDO</OPTION>
		<OPTION VALUE="CASTIGADO">CASTIGADO</OPTION>
            </select>              
           <% } else if (esth.equals("CASTIGADO"))  { %>          
                <select  name="esthipo" >
                <OPTION VALUE="VIGENTE"   >VIGENTE</OPTION> 
		<OPTION VALUE="VENCIDO"  >VENCIDO</OPTION>
		<OPTION VALUE="CASTIGADO" selected  >CASTIGADO</OPTION>
            </select>
             <% }else {
           %>
           <label>
           
           <input type="text" name="esthipo"  class="text" value="No Hay" maxlength="10"></input>
           </label>
           <%

}%> 
             </label>
         </label> 
             <label>
                 Credito Complementario
                 <input type="text"  name="ccomp"  class="text" value="<%=cred.getCcomple() %> " maxlength="20"></input>
                 
             </label>
              <label>Operacion Complementario           
           <input type="text"  id="opecomp" name="opecomp" class="text" value="<%=cred.getNcompl() %>" maxlength="20"/>
         </label>
             
             
           <label>
               <label> Estado Complementario
              <td>
               <%   String estcomp =cred.getEstadocomp();
      if(estcomp.equals("VENCIDO"))
       {   %>
       
       
        <select  name="estcomp" >


                <OPTION VALUE="VIGENTE"  >VIGENTE</OPTION> 
		<OPTION VALUE="VENCIDO" selected >VENCIDO</OPTION>
		<OPTION VALUE="CASTIGADO">CASTIGADO</OPTION>
		


            </select>
         
             <%}else if(estcomp.equals("VIGENTE"))
                         {%>
                         <select  name="estcomp" >
                <OPTION VALUE="VIGENTE"  selected >VIGENTE</OPTION> 
		<OPTION VALUE="VENCIDO"  >VENCIDO</OPTION>
		<OPTION VALUE="CASTIGADO">CASTIGADO</OPTION>
            </select>           
           <% } else if (estcomp.equals("CASTIGADO"))  { %>          
                <select  name="estcomp" >
                <OPTION VALUE="VIGENTE"   >VIGENTE</OPTION> 
		<OPTION VALUE="VENCIDO"  >VENCIDO</OPTION>
		<OPTION VALUE="CASTIGADO" selected  >CASTIGADO</OPTION>
            </select>
           <% }else {
           %>
           <label>
           
           <input type="text" name="estcomp" class="text"  value="No Hay" maxlength="10"></input>
           </label>
           <%

}%> 
           </label>         
         </label>
         <label> Precio Venta UF 
            <input type="text"  id="pventa"class="text" name="pventa" value="<%=cred.getPv() %>" maxlength="30"/>
         </label> 
         <label> Deuda Tasacion %
            <input type="text"  id="deudatasacion"class="text" name="deudatasacion" value="<%=cred.getDuedatasa() %>" maxlength="10"/>
         </label> 
         <label> Deuda Precio %
            <input type="text"  id="deudaprecio" name="deudaprecio" class="text" value="<%=cred.getDoprecio() %>" maxlength="10"/>
         </label>
         <label> Deuda Total Garantia %
            <input type="text"  id="deudatasacion" name="tgarantia"class="text"  value="<%=cred.getDtgarantia() %>"maxlength="10"/>
         </label> 
         <% }%>
          <li class="clearfix">
              <div align="right">
        <input type="Submit" value="Guardar" class="submitbutton" align="center" />
          </div>
          </li>
     
    </li>
        </form>
          </div>
        </div>
        <!--items-->
      </div>
      <!--wizard-->
    <div style="clear:both"></div>     
    <br></br>
    <br></br>
</body>
</html>
