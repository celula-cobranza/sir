
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page errorPage="paginaError.jsp" %>
<%@page import ="java.util.*"%>
<%@page import ="java.util.Collections"%>
<%@page import ="beans.*"%>
<jsp:useBean id="datoscliente" scope="request"   class="beans.clienteDAO" />

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<link rel="stylesheet" href="../assets/demo_blue.css" type="text/css" />
<link rel="stylesheet" type="text/css" media="screen" href="detalles.css" />

<script type="text/javascript" src="lib/jquery.js"></script>
<script type="text/javascript" src="jquery.validate.js"></script>
<script type="text/javascript">
        $(document).ready(function(){
            $("#TablaCliente").formToWizard({ submitButton: 'SaveAccount' })
        });
    </script>

<script type="text/javascript">
  
//valida rut
    function EsRut(texto)
    {
       var tmpstr = "";
       for ( i=0; i < texto.length ; i++ )
          if ( texto.charAt(i) != ' ' && texto.charAt(i) != '.' && texto.charAt(i) != '-' )
             tmpstr = tmpstr + texto.charAt(i);
       texto = tmpstr;
       largo = texto.length;

       if ( largo < 2 )
       {
          return false;
       }

       for (i=0; i < largo ; i++ )
       {
          if ( texto.charAt(i) !="0" && texto.charAt(i) != "1" && texto.charAt(i) !="2" && texto.charAt(i) != "3" && texto.charAt(i) != "4" && texto.charAt(i) !="5" && texto.charAt(i) != "6" && texto.charAt(i) != "7" && texto.charAt(i) !="8" && texto.charAt(i) != "9" && texto.charAt(i) !="k" && texto.charAt(i) != "K" )
          {
             return false;
          }
       }

       var invertido = "";
       for ( i=(largo-1),j=0; i>=0; i--,j++ )
          invertido = invertido + texto.charAt(i);
       var dtexto = "";
       dtexto = dtexto + invertido.charAt(0);
       dtexto = dtexto + '-';
       cnt = 0;

       for ( i=1,j=2; i<largo; i++,j++ )
       {
          if ( cnt == 3 )
          {
             dtexto = dtexto + '.';
             j++;
             dtexto = dtexto + invertido.charAt(i);
             cnt = 1;
          }
          else
          {
             dtexto = dtexto + invertido.charAt(i);
             cnt++;
          }
       }

       invertido = "";
       for ( i=(dtexto.length-1),j=0; i>=0; i--,j++ )
          invertido = invertido + dtexto.charAt(i);

       if ( revisarDigito(texto) )
          return true;

       return false;
    }

    function revisarDigito(componente)
    {
       var crut =  componente
       largo = crut.length;
       if ( largo < 2 )
       {
          return false;
       }
       if ( largo > 2 )
          rut = crut.substring(0, largo - 1);
       else
       rut = crut.charAt(0);
       dv = crut.charAt(largo-1);

       if ( dv != '0' && dv != '1' && dv != '2' && dv != '3' && dv != '4' && dv != '5' && dv != '6' && dv != '7' && dv != '8' && dv != '9' && dv != 'k'  && dv != 'K')
       {
          return false;
       }

       if ( rut == null || dv == null )
          return 0

       var dvr = '0'
       suma = 0
       mul  = 2

       for (i= rut.length -1 ; i >= 0; i--)
       {
          suma = suma + rut.charAt(i) * mul
          if (mul == 7)
             mul = 2
          else
             mul++
       }
       res = suma % 11
       if (res==1)
          dvr = 'k'
       else if (res==0)
          dvr = '0'
       else
       {
          dvi = 11-res
          dvr = dvi + ""
       }
       if ( dvr != dv)
       {
          return false
       }

       return true
    }
/*function esrut(rut){
		var cedula=EsRut(rut);
		if (cedula==false){
				alert("El RUT "+rut+" no es v�lido");
				document.form.rut.value=" ";
			}else{
									//******************************
				if (rut.length==9){
						var la=rut.substring(0,8);
						var digito=rut.charAt(8);
						document.form.rut.value=la+"-"+digito;
					}else{
						var la=rut.substring(0,8);
						var digito=rut.charAt(9);
						document.form.rut.value=la+"-"+digito;
					}
				}
	}*/
function esrut(rut){
		if (rut==0){
				alert("Debe ingresar un RUT");
				 document.getElementById('rut').value="";    //me posiciono en el campo nuevamente
                                                        document.getElementById('rut').focus(rut);//me posiciono en el campo nuevamente
			}else{
					if (EsRut(rut)==false){
							alert("El RUT "+rut+" no es valido");
							;//limpio el valor del campo rut
                                                        document.getElementById('rut').value="";    //me posiciono en el campo nuevamente
                                                        document.getElementById('rut').focus(rut);

                                        }else{
								if (rut.length==9){
						var la=rut.substring(0,8);
						var digito=rut.charAt(8);
						//document.form.rut.value=la+"-"+digito;
						//alert("El RUT "+la+"-"+digito+" SI es v�lido");
					}else{
						var la=rut.substring(0,8);
						var digito=rut.charAt(9);
						//document.form.rut.value=la+"-"+digito;
						//alert("El RUT "+la+"-"+digito+" SI es v�lido");
					}
							}
				}
	}


</script>
<script type="text/javascript">

$(function(){
	$('#TablaCliente').validate({
		rules: {
		'nombre': 'required',
		'app': 'required',
		'apm': 'required',
		'fono':'requerid',	
		'email': { required: true, email: true }

		},
		messages: {
			
		'rut': 'Debe ingresar el rut',
		'nombre': 'Debe ingresar el Nombre del cliente',
		'app': 'Debe ingresar el Apellido Paterno del cliente',
		'apm': 'Debe ingresar el Apellido Materno del cliente',	
		'fono':'Debe ingresar el Numero de Telefono',
		
		email: {
				required: "Ingrese Direccion de Correo Electronico",
				minlength: "Ingrese una Direccion de Correo Electronico Valida",
				remote: jQuery.format("{0} is already in use")
			},
       debug: true,
       /*errorElement: 'div',*/
       //errorContainer: $('#errores'),
       submitHandler: function(form){
           alert('El formulario ha sido validado correctamente!');
       }
				
		}



	});


});
</script>
<script >
      $(document).ready(function(){
      $("#TablaCliente").validate();
     });
    </script>

</head>
<% String rut=request.getParameter("rut")  ;
        
        %>

<body>

<div id="wrap">
<div id="main">

<h1 class="top bottom"><span><strong><h1>INFORMACION CLIENTE</h1></strong></span> 
<HR color="#220000"/>
</h1>
    <form  id="TablaCliente" action="../upcliente" method="post" >
<fieldset>
  <legend><h2>
          <font color="#990000"><strong>Detalle:</strong></font></h2></legend>
  <table width="704" border="" bordercolor="#FFFFFF" >   
   <% 
             
             datoscliente.setRut(rut);
          
              List lista=datoscliente.getLista();
             
            beanCliente cliente;
           
              
              for(int i=0;i<lista.size();i++)  {
              cliente=(beanCliente)lista.get(i);
              
              
             %>
         
      
      <tr>
         <td width="304" height="33">
         <div align="left"><strong>Rut:</strong></div>
         </td><td width="349" align="center">
             <input         type="text" width="274" id="rut" name="rut" maxlength="10"   value="<%=cliente.getRut() %>" class="required" tabindex="1" /> 
         </td>
         </tr>
         
    <tr>
        
        <td width="304" height="33">
            
         <Strong>Nombre:</Strong></td>
        <td width="349" align="center">
           
            <input    type="text" width="274" id="nombre" name="nombre" maxlength="50"  class="required" value="<%=cliente.getNombre()  %>"  disabled="disabled" tabindex="1" /></td>
         </tr>
         
      
 
         <tr>
      <%
if(cliente.getSexo().equals("M")) {     
%>
      
  <td width="304" height="33">    <Strong>Sexo:</Strong></td> <Strong><td width="349" align="center">M<input type="radio" checked="checked" disabled="disabled" name="sexo" value="M"> &nbsp;&nbsp;F<input type ="radio" name= "sexo" value="F" disabled="disabled"  />
</td>

</Strong>
   <%}else{%> 
       <td width="304" height="33">    <Strong>Sexo:</Strong></td> <Strong><td width="349" align="center">M<input type="radio"  disabled="disabled" name="sexo" value="M"> &nbsp;&nbsp;F<input type ="radio" name= "sexo" value="F" checked="checked" disabled="disabled"  />
</td>

</Strong> <% } %>    
        
 </tr> 
 <tr>
     <%  
if (cliente.getEstadocivil().equals("soltero"))
  {
%>
     
         <td width="304" height="33">
         <Strong>Estado Civil:</Strong></td><td width="349" align="center">
             <select  name="civil"  > 
         
                 <OPTION VALUE= "soltero"  selected="soltero">SOLTERO(A)</OPTION>
		<OPTION VALUE="casado">CASADO(A)</OPTION>
		<OPTION VALUE="separado">SEPARADO(A)</OPTION>
		<OPTION VALUE="viudo">VIUDO(A)</OPTION>

            </select>
            </td>
            <% }else if(cliente.getEstadocivil().equals("casado")) {%>
            <td width="304" height="33">
         <Strong>Estado Civil:</Strong></td><td width="349" align="center">
             <select  name="civil"  > 
         
                 <OPTION VALUE= "soltero"  >SOLTERO(A)</OPTION>
		<OPTION VALUE="casado" selected="casado">CASADO(A)</OPTION>
		<OPTION VALUE="separado">SEPARADO(A)</OPTION>
		<OPTION VALUE="viudo">VIUDO(A)</OPTION>

            </select>
            </td> <%}else if(cliente.getEstadocivil().equals("separado") ) {%>
             <td width="304" height="33">
         <Strong>Estado Civil:</Strong></td><td width="349" align="center">
             <select  name="civil"  > 
         
                 <OPTION VALUE= "soltero"  >SOLTERO(A)</OPTION>
		<OPTION VALUE="casado">CASADO(A)</OPTION>
		<OPTION VALUE="separado" selected="separado">SEPARADO(A)</OPTION>
		<OPTION VALUE="viudo">VIUDO(A)</OPTION>

            </select>
             </td> <% } else if (cliente.getEstadocivil().equals("viudo")){ %>
              <td width="304" height="33">
         <Strong>Estado Civil:</Strong></td><td width="349" align="center">
             <select  name="civil"  > 
         
                 <OPTION VALUE= "soltero"  >SOLTERO(A)</OPTION>
		<OPTION VALUE="casado">CASADO(A)</OPTION>
		<OPTION VALUE="separado">SEPARADO(A)</OPTION>
		<OPTION VALUE="viudo" selected="viudo">VIUDO(A)</OPTION>

            </select>
             </td> <% }%>
         </tr>
           <tr>
         <td width="304" height="33">
         <Strong>Correo:</Strong></td><td width="349" align="center"><input 
         type="text" width="274" id="email" name="email" maxlength="10" class="required" value="<%=cliente.getCorreo()  %>" tabindex="1"  /></td>
         </tr> 
          <tr>
         <td width="304" height="33">
         <Strong>Telefono:</Strong></td><td width="349" align="center">
             <input     type="text" width="274" id="fono" name="fono" maxlength="10" class="required" value="<%=cliente.getTelefono() %>" tabindex="1" /></td>
         </tr>     
         <% } 
            %>
  </table>
     </p>
     <div>
       <div align="right">
       
           <input  type="Submit" id="SaveAccount" value="Guardar" class="submitbutton" />
       </div>
     </div>
</fieldset>
<br />
<HR color="#220000"/>
<br />
</form>
</div>
</div>
</body>
</html>
