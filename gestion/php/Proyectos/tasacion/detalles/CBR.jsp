<%@page errorPage="paginaError.jsp" %>
<%@page import ="java.util.*"%>
<%@page import="java.sql.*" %> 
<%@page import ="java.util.Collections"%>
<%@page import ="beans.*"%>
<jsp:useBean id="datoscbr" scope="request" class="beans.cbrDAO" />


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
<%
String rol =request.getParameter("rol");

 %>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="stylesheet" type="text/css" href="style.css" />

<link rel="stylesheet" type="text/css" href="../css/calendario.css" />


    
</style>
<script src="jquery.js" type="text/javascript"></script>
<script src="jquery.validate.js" type="text/javascript"></script>
<script src="jquery.metadata.js" type="text/javascript"></script>

<style type="text/css">
#wizard2{
	background:#fff url(../images/h600.png) repeat scroll 0 0;
	border:5px solid #E9E9E9;
	font-size:12px;
	height:300px;
	float:right;
	width:95%;
	overflow:hidden;
	position: absolute;
	/* rounded corners for modern browsers */
	-moz-border-radius:5px;
	-webkit-border-radius:5px;
}
/* scrollable items */
#wizard2 .items{
	width:20000em;
	clear:both;
	position:absolute;
}
/* single item */
#wizard2 .page{

	width:600px;
	float:left;
}
</style>

<script type="text/javascript" src="js/calendario/calendario_dw.js"></script>
 <script src="js/fancybox/jquery.tools.min.js" type="text/javascript"></script>  
            <script type="text/javascript">
	$(document).ready(function(){
		$("#fcbr").calendarioDW();
	})
	</script>
  
<title>Banco Ripley</title>
</head>
<body>
      <div id="wizard2">
         <div class="items" >       
          <div class="page">
            <form  id="TablaCliente" action="../upcbr" method="post" >
                
                <label> INFORMACION CBR</label>
  <br></br>
  
  <li class="required double">  
       <% 
         
            // String r =datoscbr.getRol(rut);
          datoscbr.setRol(rol);
         
              List listacbr =datoscbr.getCBR();
             
            beanCBR cb ;
           
              
              for(int j=0;j<listacbr.size();j++)  {
              cb=(beanCBR)listacbr.get(j);
              
              
             %>
             <input type="hidden" name="rolprop" value="<%=rol%>"></input>
             <label>Rol
           <input type="text"  id="rol" name="rol" maxlength="10" class="text" tabindex="1"  value="<%=rol%>"disabled="disabled"/>
         </label>            
         <label>Fojas
           <input type="text" id="fojas" name="fojas" maxlength="20"  value="<%=cb.getCfojas() %>" class="text" tabindex="1" />
         </label>
         <label>Numero de Registro           
            <input type="text"  id="nregistro" name="nregistro" maxlength="20" value="<%=cb.getNregistro()%>" class="text" tabindex="1" />
         </label>
         <label>CBR Fecha
          <input type="text"  id="fcbr" name="fcbr" maxlength="20"  class="text" tabindex="1" value="<%=cb.getFecha() %> "/>
         </label> 
         <label> Numero de Repertorio 
             <input type="text"   id="nrptio" name="nrptio" maxlength="50" class="text" value="<%=cb.getNrptiof()%>" tabindex="1"  />
         </label> 
         
          <label> Direccion Cbr
             <input type="text"   id="dircbr" name="dircbr" maxlength="50" class="text" value="<%=cb.getDircbr()   %>" tabindex="1"  />
         </label> 
         <% }%>
          </li>
          <div align="right">
          <li class="clearfix">
        <input type="Submit" value="Guardar" class="submitbutton" align="center" />
                    </li>
          </div>
        </form>
          </div>
        </div>
        <!--items-->
      </div>
      <!--wizard-->
    <div style="clear:both"></div>     
    <br></br>
    <br></br>
</body>
</html>
