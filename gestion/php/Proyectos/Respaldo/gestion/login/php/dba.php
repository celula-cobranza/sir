<?php
	include("seguridad.php");
	if($privilegios < 1){
		header("location: ../");
		exit();
		}
		
 	

?>

<html>
<meta charset="utf8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<head>
		<title>Menu Desplegable</title>
		<style type="text/css">
  /* el menú en si mismo */
  .mi-menu  {
    border-radius: 5px;
    list-style-type: none;
    margin: 0 auto; /* si queremos centrarlo */
    padding: 0;
    /* la altura y su ancho dependerán de los textos */
    height: 40px; 
    width: 1300px;
    /* el color de fondo */
    background: #555;
    background: -moz-linear-gradient(#555,#222);
    background: -webkit-linear-gradient(#555,#222);
    background: -o-linear-gradient(#555,#222);
    background: -ms-linear-gradient(#555,#222);
    background: linear-gradient(#555,#222);
  }

  /* si es necesario, evitamos que Blogger de problemas con los saltos de línea cuando escribimos el HTML */
  .mi-menu  br { display:none; }

  /* cada item del menu */
  .mi-menu  li {
    display: block;
    float: left; /* la lista se ve horizontal */
    height: 40px;
    list-style: none;
    margin: 0;
    padding: 0;
    position: relative;
  }
  .mi-menu li a {
    border-left: 1px solid #000;
    border-right: 1px solid #666;
    color: #EEE;
    display: block;
    font-family: Tahoma;
    font-size: 13px;
    font-weight: bold;
    line-height: 28px;
    padding: 0 30px;
    margin: 6px 0;
    text-decoration: none;
    /* animamos el cambio de color de los textos */
    -webkit-transition: color .2s ease-in-out;
    -moz-transition: color .2s ease-in-out;
    -o-transition: color .2s ease-in-out;
    -ms-transition: color .2s ease-in-out;
    transition: color .2s ease-in-out;
  }
  /* eliminamos los bordes del primer y el último */
  .mi-menu li:first-child a { border-left: none; }
  .mi-menu li:last-child a{ border-right: none; }
  /* efecto hover cambia el color */
  .mi-menu li:hover > a { color: Crimson; }

  /* los submenús */
  .mi-menu ul {
    border-radius: 0 0 5px 5px;
    left: 0;
    margin: 0;
    opacity: 0; /* no son visibles */
    position: absolute;
    top: 40px; /* se ubican debajo del enlace principal */
    /* el color de fondo */
    background: #222;
    background: -moz-linear-gradient(#222,#555);
    background: -webkit-linear-gradient(#22,#555);
    background: -o-linear-gradient(#222,#555);
    background: -ms-linear-gradient(#222,#555);
    background: linear-gradient(#222,#555);
    /* animamos su visibildiad */
    -moz-transition: opacity .25s ease .1s;
    -webkit-transition: opacity .25s ease .1s;
    -o-transition: opacity .25s ease .1s;
    -ms-transition: opacity .25s ease .1s;
    transition: opacity .25s ease .1s;
  }
  /* son visibes al poner el cursor encima */
  .mi-menu li:hover > ul { opacity: 1; }

   /* cada un ode los items de los submenús */
  .mi-menu ul li {
    height: 0; /* no son visibles */
    overflow: hidden;
    padding: 0;
    /* animamos su visibildiad */
    -moz-transition: height .25s ease .1s;
    -webkit-transition: height .25s ease .1s;
    -o-transition: height .25s ease .1s;
    -ms-transition: height .25s ease .1s;
    transition: height .25s ease .1s;
  }
  .mi-menu li:hover > ul li {
    height: 36px; /* los mostramos */
    overflow: visible;
    padding: 0;
  }
  .mi-menu ul li a {
    border: none;
    border-bottom: 1px solid #111;
    margin: 0;
    /* el ancho dependerá de los textos a utilizar */
    padding: 5px 20px;
    width: 160px;
  }
  /* el último n otiene un borde */
  .mi-menu ul li:last-child a { border: none; }

</style>
			
		</style>
	</head>
	<ul class="mi-menu">
  <li>
	<a href="administrador.php"> GESTIONES INTERNAS </a>
		<ul>
      <li><a href="mostrar.php"> ATENCION PUBLICO </a></li>
      <li><a href="administrador.php"> GESTIONES CARTERA </a></li>
      <li><a href="#"> AGENDA </a></li>
	  <li><a href="#"> REPORTES INTERNOS</a></li>
    </ul>
    </li>
	
  <li>
    <a href="#"> GASTOS </a>
		<ul>
      <li><a href="#"> COSTAS </a></li>
      <li><a href="#"> EMPEX </a></li>
      <li><a href="#"> HONORARIOS ABOGADOS </a></li>
	  <li><a href="#"> HONORARIOS PAYBACK </a></li>
		</ul>
  </li>
  
  <li>
    <a href="#"> JUDUCIAL </a>
		<ul>
      <li><a href="#"> ALZAMIENTOS </a></li>
      <li><a href="#"> EXHORTOS </a></li>
      <li><a href="#"> TRAMITES </a></li>
	  <li><a href="#"> EMBARGOS </a></li>
	  <li><a href="#"> REPORTES VARIOS </a></li>
		</ul>
  </li>
  
   <li>
    <a href="#"> CIERRES MENSUALES </a>
		<ul>
      <li><a href="#"> CONVENIOS </a></li>
      <li><a href="#"> OP. CANCELADAS </a></li>
      <li><a href="#"> OP. CASTIGADAS </a></li>
	  <li><a href="#"> OP. MOROSAS </a></li>
		</ul>
  </li>
  
  <li>
    <a href="#"> REPORTERIA </a>
		<ul>
      <li><a href="#"> POR DEFINIR </a></li>
      <li><a href="#"> POR DEFINIR </a></li>
      <li><a href="#"> POR DEFINIR </a></li>
	  <li><a href="#"> POR DEFINIR </a></li>
		</ul>
  </li>
    <li>
    <a href="#"> POR DEFINIR </a>
		<ul>
      <li><a href="#"> POR DEFINIR </a></li>
      <li><a href="#"> POR DEFINIR </a></li>
      <li><a href="#"> POR DEFINIR </a></li>
	  <li><a href="#"> POR DEFINIR </a></li>
		</ul>
  </li>
    <li>
    <a href="#"> POR DEFINIR </a>
		<ul>
      <li><a href="#"> POR DEFINIR </a></li>
      <li><a href="#"> POR DEFINIR </a></li>
      <li><a href="#"> POR DEFINIR </a></li>
	  <li><a href="#"> POR DEFINIR </a></li>
		</ul>

  </li>
  <hr>
 
  
  
<form><center>
<input type="button" value="Cerrar Sesion" onClick="location.href = 'index.php' ">
<!-- Asi sucesivamente hasta tener el numero de botones deseados con sus respectivas URL -->
</form></center>

</html>

HOLA MUNDO