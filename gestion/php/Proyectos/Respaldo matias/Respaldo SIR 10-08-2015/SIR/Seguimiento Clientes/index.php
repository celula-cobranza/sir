<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf8_decode" />
<title>BANCO RIPLEY - Seguimiento Casos SIR</title>
<script src="js/jquery-1.3.1.min.js" type="text/javascript"></script>
<script src="js/jquery.functions.js" type="text/javascript"></script>
<link type="text/css" rel="stylesheet" href="css/estilo.css" />

<!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/small-business.css" rel="stylesheet">

</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">
                    <img src="Banco-ripley.jpg" alt="">
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="http://192.168.33.171/sir">Inicio</a>
                    </li>
                    <li>
                        <a href="http://192.168.33.171/sir/audiencias">Audiencias</a>
                    </li>
                    <li>
                        <a href="http://192.168.33.171/sir/seguimiento clientes">Seguimiento de clientes</a>
                    </li>
					<li>
                        <a href="http://192.168.33.171/sir/abogados">Abogados</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>


<h4 ALIGN=center>Seguimiento Casos SIR</h4>
<div id="contenedor">
    <div id="formulario" style="display:none;">
    </div>
    <div id="tabla">
    <?php include('consulta.php') ?>
    </div>
</div>
<span style="text-align:center;width:300px;margin:auto;display:block;margin-top:20px;">Banco Ripley</span>
</body>
</html>
