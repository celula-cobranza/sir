<?php
require('functions.php');
if(isset($_POST['submit'])){
	require('clases/cliente.class.php');
	$objCliente=new Cliente;
	
	$RUT_ABOGADO = htmlspecialchars(trim($_POST['RUT_ABOGADO']));
	$V_RECON_PASIVO = htmlspecialchars(trim($_POST['V_RECON_PASIVO']));
	$V_RENEGOCIACION = htmlspecialchars(trim($_POST['V_RENEGOCIACION']));
	$V_EJECUCION = htmlspecialchars(trim($_POST['V_EJECUCION']));
	$V_LIQ_VOLUNTARIA = htmlspecialchars(trim($_POST['V_LIQ_VOLUNTARIA']));
	
	if ( $objCliente->actualizar(array(
	$V_RECON_PASIVO,
	$V_RENEGOCIACION,
	$V_EJECUCION,
	$V_LIQ_VOLUNTARIA),
	$RUT_ABOGADO) == true){
	
	echo 'Datos actualizados';}
	
	else{
		echo 'Se produjo un error. Intente nuevamente';
	} 
}else{
	if(isset($_GET['id'])){
		
		require('clases/cliente.class.php');
		$objCliente=new Cliente;
		$consulta = $objCliente->mostrar_cliente($_GET['id']);
		$cliente = mysql_fetch_array($consulta);
	?>
	<form id="frmClienteActualizar" name="frmClienteActualizar" method="post" action="actualizar.php" onsubmit="ActualizarDatos(); return false">
    	
		<p align="center"> 
		<strong>Modificar Datos de Abogado SIR
		<br><br>
		</strong>
		</p> 	
		
		<p>
		<label>RUT ABOGADO (SIN DIGITO VERIFICADOR)<br />
		<input type="text" name="RUT_ABOGADO" id="RUT_ABOGADO" value="<?php echo $cliente['RUT_ABOGADO']?>" / disabled>
		</label>
		</p>
		
		<p>
		<label>NOMBRE ABOGADO<br />
		<input class="text" type="text" name="NOMBRE" id="NOMBRE" value="<?php echo $cliente['NOMBRE']?>" / disabled>
		</label>
	  </p>
	  <p>
		<label>VALOR RECONOCIMIENTO PASIVO<br />
		<input class="text" type="text" name="V_RECON_PASIVO" id="V_RECON_PASIVO" value="<?php echo $cliente['V_RECON_PASIVO']?>" />
		</label>
	  </p>
	  <p>
		<label>VALOR RENEGOCIACION<br />
		<input class="text" type="text" name="V_RENEGOCIACION" id="V_RENEGOCIACION" value="<?php echo $cliente['V_RENEGOCIACION']?>" />
		</label>
	  </p>
		
	  <p>
	  <label>VALOR EJECUCION<br />
	  <input class="text" type="text" name="V_EJECUCION" id="V_EJECUCION" value="<?php echo $cliente['V_EJECUCION']?>" />
	  </label>
	  </p>
	  
	  <p>
	  <label>VALOR LIQUIDACION VOLUNTARIA<br />
	  <input class="text" type="text" name="V_LIQ_VOLUNTARIA" id="V_LIQ_VOLUNTARIA" value="<?php echo $cliente['V_LIQ_VOLUNTARIA']?>" />
	  </label>
	  </p>


	  <p>
		<input type="submit" name="submit" id="button" value="Actualizar Datos" />
		<label></label>
		<input type="button" name="cancelar" id="cancelar" value="Cancelar" onclick="Cancelar()" />
	  </p>
	</form>
	<?php
	}
}
?>