<?php
require('functions.php');
if(isset($_POST['submit'])){
	require('clases/cliente.class.php');

	$FECHA_AUDIENCIA = htmlspecialchars(trim($_POST['FECHA_AUDIENCIA']));
	$IDSUBAUDIENCIA = htmlspecialchars(trim($_POST['IDSUBAUDIENCIA']));
	$RUT_CLIENTE = htmlspecialchars(trim($_POST['RUT_CLIENTE']));
	$CIUDAD = htmlspecialchars(trim($_POST['CIUDAD']));
	$RUT_ABOGADO = htmlspecialchars(trim($_POST['RUT_ABOGADO']));
	$ASISTENCIA = htmlspecialchars(trim($_POST['ASISTENCIA']));
	
	$objCliente=new Cliente;
	if ( $objCliente->insertar(array(
	$FECHA_AUDIENCIA,
	$IDSUBAUDIENCIA,
	$RUT_CLIENTE,
	$CIUDAD,
	$RUT_ABOGADO,
	$ASISTENCIA)) == true)
	{
		echo 'Datos guardados';
	}else{
		echo 'Se produjo un error. Intente nuevamente';
	} 
}else{
?>

<form id="frmClienteNuevo" name="frmClienteNuevo" method="post" action="nuevo.php" onsubmit="GrabarDatos(); return false">
  
		<p align="center"> 
		<strong>Ingresar Nueva Audiencia SIR
		<br><br>
		</strong>
		</p>
  
		<p>
		<label>FECHA AUDIENCIA<br />
		<input class="text" type="date" name="FECHA_AUDIENCIA" id="FECHA_AUDIENCIA" />
		</label>
		</p>
		
		<p>
		<label>TIPO AUDIENCIA<br />
		<input type="radio" name="IDSUBAUDIENCIA" id="EJECUCION" value="4" />
		EJECUCION</label><br>
		<label>
		<input type="radio" name="IDSUBAUDIENCIA" id="LIQUIDACION VOLUNTARIA" value="3" />
		LIQUIDACION VOLUNTARIA</label><br>
		<label>
		<input type="radio" name="IDSUBAUDIENCIA" id="RECONOCIMIENTO PASIVO" value="1" />
		RECONOCIMIENTO PASIVO</label><br>
		<label>
		<input type="radio" name="IDSUBAUDIENCIA" id="RENEGOCIACION" value="2" />
		RENEGOCIACION</label><br>
		</p>

		<p>
		<label>RUT CLIENTE (SIN DIGITO VERIFICADOR)<br />
		<input class="text" type="text" name="RUT_CLIENTE" id="RUT_CLIENTE" />
		</label>
		</p>
		 
		<p>
		<label>CIUDAD<br />
		<input class="text" type="text" name="CIUDAD" id="CIUDAD" />
		</label>
		</p>

		<p>
		<label>RUT ABOGADO (SIN DIGITO VERIFICADOR)<br />
		<input class="text" type="text" name="RUT_ABOGADO" id="RUT_ABOGADO" />
		</label>
		</p>
	  
		<p>
		<label>ASISTENCIA<br />
		<select class="selectpicker" name="ASISTENCIA" data-style="btn-primary" id="ASISTENCIA" >
		<option></option>
		<option>ASISTE</option>
		<option>NO ASISTE</option>

		</select>
		</label>
		</p>

		<p>
		<input type="submit" name="submit" id="button" value="Guardar" />
		<label></label>
		<input type="button" class="cancelar" name="cancelar" id="cancelar" value="Cancelar" onclick="Cancelar()" />
		</p>
</form>
<?php
}
?>