<?php
require('clases/cliente.class.php');
$objCliente=new Cliente;
$consulta=$objCliente->mostrar_clientes();
?>
<script type="text/javascript">
$(document).ready(function(){
	// mostrar formulario de actualizar datos
	$("table tr .modi a").click(function(){
		$('#tabla').hide();
		$("#formulario").show();
		$.ajax({
			url: this.href,
			type: "GET",
			success: function(datos){
				$("#formulario").html(datos);
			}
		});
		return false;
	});
	
	// llamar a formulario nuevo
	$("#nuevo a").click(function(){
		$("#formulario").show();
		$("#tabla").hide();
		$.ajax({
			type: "GET",
			url: 'nuevo.php',
			success: function(datos){
				$("#formulario").html(datos);
			}
		});
		return false;
	});
});

</script>
<span id="nuevo"><a href="nuevo.php"><img src="img/add.png" alt="Agregar dato" /></a></span>
	<table>
   		<tr>
   			<th></th>
            <th></th>
			<th>FECHA AUDIENCIA</th>
			<th>TIPO AUDIENCIA</th>
			<th>RUT CLIENTE</th>
			<th>DV</th>
			<th>NOMBRE CLIENTE</th>
			<th>CIUDAD</th>
			<th>ABOGADO</th>
			<th>RUT ABOGADO</th>
			<th>ASISTENCIA</th>



        </tr>
<?php
if($consulta) {
	while( $cliente = mysql_fetch_array($consulta) ){
	?>
	
		 <tr id="fila-<?php echo $cliente['ID_AUDIENCIA'] ?>">
				<td><span class="modi"><a href="actualizar.php?id=<?php echo $cliente['ID_AUDIENCIA'] ?>"><img src="img/database_edit.png" title="Editar" alt="Editar" /></a></span></td>
				<td><span class="dele"><a onClick="EliminarDato(<?php echo $cliente['ID_AUDIENCIA'] ?>); return false" href="eliminar.php?id=<?php echo $cliente['ID_AUDIENCIA'] ?>"><img src="img/delete.png" title="Eliminar" alt="Eliminar" /></a></span></td>
				<td><?php echo $cliente['FECHA_AUDIENCIA'] ?></td>
				<td><?php echo $cliente['TIPO_AUDIENCIA'] ?></td>
				<td><?php echo $cliente['RUT_CLIENTE'] ?></td>
				<td><?php echo $cliente['DV'] ?></td>
				<td><?php echo $cliente['NOMBRE_CLIENTE'] ?></td>
				<td><?php echo $cliente['CIUDAD'] ?></td>
				<td><?php echo $cliente['ABOGADO'] ?></td>
				<td><?php echo $cliente['RUT_ABOGADO'] ?></td>
				<td><?php echo $cliente['ASISTENCIA'] ?></td>

		  </tr>
	<?php
	}
}
?>
    </table>