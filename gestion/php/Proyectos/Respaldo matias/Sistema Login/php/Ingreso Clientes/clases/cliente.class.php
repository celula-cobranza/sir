<?php 
include_once("conexion.class.php");

class Cliente{
 //constructor	
 	var $con;
 	function Cliente(){
 		$this->con=new DBManager;
 	}

	function insertar($campos){
		if($this->con->conectar()==true){
			//print_r($campos);
			//echo "INSERT INTO cliente (nombres, ciudad, sexo, telefono, fecha_nacimiento) VALUES ('".$campos[0]."', '".$campos[1]."','".$campos[2]."','".$campos[3]."','".$campos[4]."')";
			return mysql_query("
			INSERT INTO clientes (
			FECHA_ADMISIBILIDAD,
			TIPO_AUDIENCIA,
			ROL,
			NOMBRE_CLIENTE,
			RUT_CLIENTE,
			DEUDA_INFORMADA_SIR,
			DEUDA_BANCO,
			DEUDA_CAR,
			PRIMERA_FECHA_AUDIENCIA,
			HORA_AUDIENCIA,
			DIRECCION_AUDIENCIA,
			CIUDAD,
			ESTADO_BOLETIN,
			FECHA_CARGA) 
			VALUES (
			'".$campos[0]."', 
			'".$campos[1]."',
			'".$campos[2]."',
			'".$campos[3]."',
			'".$campos[4]."',
			'".$campos[5]."',
			'".$campos[6]."',
			'".$campos[7]."',
			'".$campos[8]."',
			'".$campos[9]."',
			'".$campos[10]."',
			'".$campos[11]."',
			'".$campos[12]."',
			SYSDATE())");
		}
	}
	
	function actualizar($campos,$id){
		if($this->con->conectar()==true){
			//print_r($campos);
			return mysql_query("
			UPDATE clientes
			SET
			FECHA_ADMISIBILIDAD = '".$campos[0]."',
			TIPO_AUDIENCIA = '".$campos[1]."',
			ROL = '".$campos[2]."',
			DEUDA_INFORMADA_SIR = '".$campos[3]."',
			DEUDA_BANCO = '".$campos[4]."',
			DEUDA_CAR = '".$campos[5]."',
			PRIMERA_FECHA_AUDIENCIA = '".$campos[6]."',
			HORA_AUDIENCIA = '".$campos[7]."',
			DIRECCION_AUDIENCIA = '".$campos[8]."',
			CIUDAD = '".$campos[9]."',
			ESTADO_BOLETIN = '".$campos[10]."'
			WHERE RUT_CLIENTE = ".$id);
		}
	}
	
	function mostrar_cliente($id){
		if($this->con->conectar()==true){
			return mysql_query("
			SELECT * 
			FROM clientes 
			WHERE RUT_CLIENTE =".$id);
		}
	}

	function mostrar_clientes(){
		if($this->con->conectar()==true){
			return mysql_query("
			SELECT * 
			FROM vclientes 
			ORDER BY FECHA_ADMISIBILIDAD DESC");
		}
	}

	function eliminar($id){
		if($this->con->conectar()==true){
			return mysql_query("
			DELETE 
			FROM clientes
			WHERE RUT_CLIENTE=".$id);
		}
	}
}
?>