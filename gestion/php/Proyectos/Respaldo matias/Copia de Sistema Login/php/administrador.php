<?php
	include("seguridad.php");
	if($privilegios < 2){
		header("location: ../");
			
		exit();
		}

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Modern Business - Start Bootstrap Template</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/modern-business.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="php/administrador.php">BANCO RIPLEY</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="services.html" class="dropdown-toggle" data-toggle="dropdown">Gestiones Internas <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="atencionpublico.php">Atención Publico</a>
                            </li>
                            
                            <li>
                                <a href="levantamiento.html">Levantamientos del Área</a>
                            </li>

                        </ul>
                    </li>
					<li class="dropdown">
                        <a href="services.html" class="dropdown-toggle" data-toggle="dropdown">Gastos <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="reportes_area/index.html">Panel Control Gastos</a>
                            </li>							
							<li>
                                <a href="reportes_area/gastos.html">Panel Estadisticas Gastos</a>
                            </li>
                        </ul>
                    </li>
					<li class="dropdown">
                        <a href="services.html" class="dropdown-toggle" data-toggle="dropdown">Judicial <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="#">En construccion</a>
                            </li>

                        </ul>
                    </li>
					<li class="dropdown">
                        <a href="services.html" class="dropdown-toggle" data-toggle="dropdown">Cierres Mensuales <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="#">En construccion</a>
                            </li>

                        </ul>
                    </li>
					
					<li>
                        <a href="index.php" <font color= "#FF0101" > Salir </font></a>
                    </li>
					
					                            
                </ul>
                
                
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->
    <div class="container">

        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Control de Gestión
                    <small>"Judicial e Hipotecario"</small>
                </h1>
                <ol class="breadcrumb">
                    
                    </li>
                    <li class="active">Banco Ripley</li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

        <!-- Image Header -->
        <div class="row">
            <div class="col-lg-12">
                <img class="img-responsive" src="home/BancoRipley.jpg" alt="">
            </div>
        </div>
        <!-- /.row -->

        <!-- Service Panels -->
        <!-- The circle icons use Font Awesome's stacked icon classes. For more information, visit http://fontawesome.io/examples/ -->
        <!--<div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Services Panels</h2>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="panel panel-default text-center">
                    <div class="panel-heading">
                        <span class="fa-stack fa-5x">
                              <i class="fa fa-circle fa-stack-2x text-primary"></i>
                              <i class="fa fa-tree fa-stack-1x fa-inverse"></i>
                        </span>
                    </div>
                    <div class="panel-body">
                        <h4>Service One</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                        <a href="#" class="btn btn-primary">Learn More</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="panel panel-default text-center">
                    <div class="panel-heading">
                        <span class="fa-stack fa-5x">
                              <i class="fa fa-circle fa-stack-2x text-primary"></i>
                              <i class="fa fa-car fa-stack-1x fa-inverse"></i>
                        </span>
                    </div>
                    <div class="panel-body">
                        <h4>Service Two</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                        <a href="#" class="btn btn-primary">Learn More</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="panel panel-default text-center">
                    <div class="panel-heading">
                        <span class="fa-stack fa-5x">
                              <i class="fa fa-circle fa-stack-2x text-primary"></i>
                              <i class="fa fa-support fa-stack-1x fa-inverse"></i>
                        </span>
                    </div>
                    <div class="panel-body">
                        <h4>Service Three</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                        <a href="#" class="btn btn-primary">Learn More</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="panel panel-default text-center">
                    <div class="panel-heading">
                        <span class="fa-stack fa-5x">
                              <i class="fa fa-circle fa-stack-2x text-primary"></i>
                              <i class="fa fa-database fa-stack-1x fa-inverse"></i>
                        </span>
                    </div>
                    <div class="panel-body">
                        <h4>Service Four</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                        <a href="#" class="btn btn-primary">Learn More</a>
                    </div>
                </div>
            </div>
        </div> -->

        <!-- Service Tabs -->
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Nuestra Área</h2>
            </div>
            <div class="col-lg-12">

                <ul id="myTab" class="nav nav-tabs nav-justified">
                    <li class="active"><a href="#service-one" data-toggle="tab"><i class="fa fa-tree"></i> Hipotecario</a>
                    </li>
                    <li class=""><a href="#service-two" data-toggle="tab"><i class="fa fa-car"></i> Consumo</a>
                    </li>
                    <li class=""><a href="#service-three" data-toggle="tab"><i class="fa fa-support"></i> Judicial</a>
                    </li>
                    <li class=""><a href="#service-four" data-toggle="tab"><i class="fa fa-database"></i> Control de Gastos</a>
                    </li>
                </ul>

                <div id="myTabContent" class="tab-content">
                    <div class="tab-pane fade active in" id="service-one">
                        <h4>Hipotecario</h4>
                        
                        <p>A nivel de la organización somos el centro neuralgico del producto hipotecario, concentrando las consultas de la red de sucursales a nivel nacional, de los clientes y su atencion presencial en santiago.</p>
						<p>La cartera hipotecaria a la fecha se compone de 3.559 clientes por un monto total de MM$42.755</p>
					</div>
                    <div class="tab-pane fade" id="service-two">
                        <h4>CORFO</h4>
                        <p>También somos el centro neuralgico de los créditos CORFO, concentrando las consultas de la red de sucursales a nivel nacional, de los clientes y su atencion presencial en santiago.</p>
				
					</div>
                    <div class="tab-pane fade" id="service-three">
                        <h4>Judicial</h4>
                        <p>El area de Control de Gestion e Hipotecario dentro de su ambito de accion tiene la administracion y el control de la cobranza judicial que ejerce la empresa de cobranza externa a nivel nacional sobre productos Consumo e Hipotecario.</p>
                        <p></p>
                    </div>
                    <div class="tab-pane fade" id="service-four">
                        <h4>Control de Gastos</h4>
                        <p>En este ambito el area de Control de Gestion se encarga del control de los gastos de empresas externas en cuanto a las gestiones de cobranza Prejudicial y Judicial, entregando mensualmente detallados informes ejecutivos que explican en cuanto se deberá incurrir y las desviaciones existentes junto con su explicación.</p>
                        
                    </div>
                </div>

            </div>
        </div>

        <!-- Service List -->
        <!-- The circle icons use Font Awesome's stacked icon classes. For more information, visit http://fontawesome.io/examples/ -->
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">Labores Importantes del Aréa</h2>
            </div>
            <div class="col-md-4">
                <div class="media">
                    <div class="pull-left">
                        <span class="fa-stack fa-2x">
                              <i class="fa fa-circle fa-stack-2x text-primary"></i>
                              <i class="fa fa-tree fa-stack-1x fa-inverse"></i>
                        </span> 
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">CONTROL DE GASTOS</h4>
                        <p>Se controla y analiza los pagos al 100% de los proveedores de cobranza prejudicial y judicial.</p>
						<p>Gastos Empresas externas, Abogados, Costas Judiciales y Payback.</p>
                    </div>
                </div>
                <div class="media">
                    <div class="pull-left">
                        <span class="fa-stack fa-2x">
                              <i class="fa fa-circle fa-stack-2x text-primary"></i>
                              <i class="fa fa-car fa-stack-1x fa-inverse"></i>
                        </span> 
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">CONTROL GESTIÓN JUDICIAL</h4>
                        <p>Se definen estrategias y se controla la gestión de la red de 24 Abogados externos, esto a través de una coordinación con Payback.</p>
                    </div>
                </div>
                <div class="media">
                    <div class="pull-left">
                        <span class="fa-stack fa-2x">
                              <i class="fa fa-circle fa-stack-2x text-primary"></i>
                              <i class="fa fa-support fa-stack-1x fa-inverse"></i>
                        </span> 
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">ATENCION DE PUBLICO</h4>
                        <p>Atención de público stock MM$50.000.- (Vigente Hipotecario, Castigo), Más Corfo (Vigente y Castigo).</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="media">
                    <div class="pull-left">
                        <span class="fa-stack fa-2x">
                              <i class="fa fa-circle fa-stack-2x text-primary"></i>
                              <i class="fa fa-database fa-stack-1x fa-inverse"></i>
                        </span> 
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">SIR</h4>
                        <p>A contar de octubre  2014, el área e Control de Gestión e Hipotecario, asumió la administración de los casos presentados ante la SIR de CAR S.A. y BR. Esto implica el control de abogados, cálculo de honorarios,  coordinación a nivel nacional de las audiencias.</p>
                    </div>
                </div>
                <div class="media">
                    <div class="pull-left">
                        <span class="fa-stack fa-2x">
                              <i class="fa fa-circle fa-stack-2x text-primary"></i>
                              <i class="fa fa-bomb fa-stack-1x fa-inverse"></i>
                        </span> 
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">HIPOTECARIO</h4>
                        <p>Segmentación cartera Hipotecario, en esta unidad radica todo el control administrativo, la cobranza, la gestión judicial, la coordinación a nivel nacional, apoyo a sucursales y atención de clientes de este producto.</p>
						<p>Cobranza de cartera hipotecaria.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="media">
                    <div class="pull-left">
                        <span class="fa-stack fa-2x">
                              <i class="fa fa-circle fa-stack-2x text-primary"></i>
                              <i class="fa fa-bank fa-stack-1x fa-inverse"></i>
                        </span> 
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">CORFO</h4>
                        <p>Control  de la cobranza, renegociaciones, atención de público. Relación directa con CORFO, para implementación de nuevos convenios.</p>
                    </div>
                </div>          
            </div>
        </div>
		
		<div class="col-lg-12">
                <h2 class="page-header">Para su Información</h2>
            </div>
			
			
			        <div class="row">
            <div class="col-lg-12">
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Tema uno</a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse">
                            <div class="panel-body">
                                En construccion.
                            </div>
                        </div>
                    </div>
                    <!-- /.panel -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Tema dos</a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                            <div class="panel-body">
                                En construccion.
                            </div>
                        </div>
                    </div>
                    <!-- /.panel -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Tema tres</a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse">
                            <div class="panel-body">
                                En construccion.
                            </div>
                        </div>
                    </div>
                    <!-- /.panel -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">Tema cuatro</a>
                            </h4>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse">
                            <div class="panel-body">
                                En construccion
                            </div>
                        </div>
                    </div>
                    
                    <!-- /.panel -->
                </div>
                <!-- /.panel-group -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy;  2015</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>