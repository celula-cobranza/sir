<?php
require('functions.php');
if(isset($_POST['submit'])){
	require('clases/cliente.class.php');

	$FECHA_ADMISIBILIDAD = htmlspecialchars(trim($_POST['FECHA_ADMISIBILIDAD']));
	$TIPO_AUDIENCIA = htmlspecialchars(trim($_POST['TIPO_AUDIENCIA']));
	$ROL = htmlspecialchars(trim($_POST['ROL']));
	$NOMBRE_CLIENTE = htmlspecialchars(trim($_POST['NOMBRE_CLIENTE']));
	$RUT_CLIENTE = htmlspecialchars(trim($_POST['RUT_CLIENTE']));
	$DEUDA_INFORMADA_SIR = htmlspecialchars(trim($_POST['DEUDA_INFORMADA_SIR']));
	$DEUDA_BANCO = htmlspecialchars(trim($_POST['DEUDA_BANCO']));
	$DEUDA_CAR = htmlspecialchars(trim($_POST['DEUDA_CAR']));
	$PRIMERA_FECHA_AUDIENCIA = htmlspecialchars(trim($_POST['PRIMERA_FECHA_AUDIENCIA']));
	$HORA_AUDIENCIA = htmlspecialchars(trim($_POST['HORA_AUDIENCIA']));
	$DIRECCION_AUDIENCIA = htmlspecialchars(trim($_POST['DIRECCION_AUDIENCIA']));
	$CIUDAD = htmlspecialchars(trim($_POST['CIUDAD']));
	$ESTADO_BOLETIN = htmlspecialchars(trim($_POST['ESTADO_BOLETIN']));
	
	$objCliente=new Cliente;
	if ( $objCliente->insertar(array(
	$FECHA_ADMISIBILIDAD,
	$TIPO_AUDIENCIA,
	$ROL,
	$NOMBRE_CLIENTE,
	$RUT_CLIENTE,
	$DEUDA_INFORMADA_SIR,
	$DEUDA_BANCO,
	$DEUDA_CAR,
	$PRIMERA_FECHA_AUDIENCIA,
	$HORA_AUDIENCIA,
	$DIRECCION_AUDIENCIA,
	$CIUDAD,
	$ESTADO_BOLETIN)) == true)
	{
		echo 'Datos guardados';
	}else{
		echo 'Se produjo un error. Intente nuevamente';
	} 
}else{
?>

<form id="frmClienteNuevo" name="frmClienteNuevo" method="post" action="nuevo.php" onsubmit="GrabarDatos(); return false">
  
		<p align="center"> 
		<strong>Ingresar Nuevo Caso SIR
		<br><br>
		</strong>
		</p>
  
		<p>
		<label>RUT CLIENTE (SIN DIGITO VERIFICADOR)<br />
		<input class="text" type="text" name="RUT_CLIENTE" id="RUT_CLIENTE" />
		</label>
		</p>
		
		<p>
		<label>NOMBRE CLIENTE<br />
		<input class="text" type="text" name="NOMBRE_CLIENTE" id="NOMBRE_CLIENTE" />
		</label>
		</p>	
		
		<p>
		<label>FECHA_ADMISIBILIDAD<br />
		<input class="text" type="date" name="FECHA_ADMISIBILIDAD" id="FECHA_ADMISIBILIDAD" />
		</label>
		</p>
		
		<p>
		<label>TIPO AUDIENCIA<br />
		<input type="radio" name="TIPO_AUDIENCIA" id="ADMISIBILIDAD" value="ADMISIBILIDAD" />
		ADMISIBILIDAD</label>
		<label>
		<input type="radio" name="TIPO_AUDIENCIA" id="LIQUIDACION VOLUNTARIA" value="LIQUIDACION VOLUNTARIA" />
		LIQUIDACION VOLUNTARIA</label>
		</p>

		<p>
		<label>ROL<br />
		<input class="text" type="text" name="ROL" id="ROL" />
		</label>
		</p>
	  
		<p>
		<label>DEUDA INFORMADA SIR<br />
		<input class="text" type="text" name="DEUDA_INFORMADA_SIR" id="DEUDA_INFORMADA_SIR" />
		</label>
		</p>
	  
		<p>
		<label>DEUDA BANCO<br />
		<input class="text" type="text" name="DEUDA_BANCO" id="DEUDA_BANCO" />
		</label>
		</p>
		
		<p>
		<label>DEUDA CAR<br />
		<input class="text" type="text" name="DEUDA_CAR" id="DEUDA_CAR" />
		</label>
		</p>
	  
		<p>
		<label>PRIMERA FECHA AUDIENCIA<br />
		<input class="text" type="date" name="PRIMERA_FECHA_AUDIENCIA" id="PRIMERA_FECHA_AUDIENCIA" />
		</label>
		</p>
		
		<p>
		<label>HORA AUDIENCIA<br />
		<input class="text" type="time" name="HORA_AUDIENCIA" id="HORA_AUDIENCIA" />
		</label>
		</p>
		
		<p>
		<label>DIRECCION AUDIENCIA<br />
		<input class="text" type="text" name="DIRECCION_AUDIENCIA" id="DIRECCION_AUDIENCIA" />
		</label>
		</p>
		
		<p>
		<label>CIUDAD<br />
		<input class="text" type="text" name="CIUDAD" id="CIUDAD" />
		</label>
		</p>
		
		<p>
		<label>ESTADO BOLETIN<br />
		<select class="selectpicker" name="ESTADO_BOLETIN" data-style="btn-primary" id="ESTADO_BOLETIN" >
		<option></option>
		<option>ACUERDO DE RENEGOCIACION</option>
		<option>ACUERDO FORMALIZADO</option>
		<option>ADM.REVOCADA</option>
		<option>ADMISIBILIDAD SIN DEUDA RIPLEY</option>
		<option>AUDIENCIA DE RENEGOCIACION</option>
		<option>AUDIENCIA DETERMINACION PASIVO</option>
		<option>FINALIZADO PROCEDIMIENTO CONCURSAL DE RENEG.</option>
		<option>LIQUID. VOL .CON DEUDA</option>
		<option>LIQUID. VOLUNTARIA</option>
		</select>
		</label>
		</p>

		<p>
		<input type="submit" name="submit" id="button" value="Guardar" />
		<label></label>
		<input type="button" class="cancelar" name="cancelar" id="cancelar" value="Cancelar" onclick="Cancelar()" />
		</p>
</form>
<?php
}
?>