<?php
require('functions.php');
if(isset($_POST['submit'])){
	require('clases/cliente.class.php');
	$objCliente=new Cliente;
	
	$RUT_CLIENTE = htmlspecialchars(trim($_POST['RUT_CLIENTE']));
	$FECHA_ADMISIBILIDAD = htmlspecialchars(trim($_POST['FECHA_ADMISIBILIDAD']));
	$TIPO_AUDIENCIA = htmlspecialchars(trim($_POST['TIPO_AUDIENCIA']));
	$ROL = htmlspecialchars(trim($_POST['ROL']));
	$DEUDA_INFORMADA_SIR = htmlspecialchars(trim($_POST['DEUDA_INFORMADA_SIR']));
	$DEUDA_BANCO = htmlspecialchars(trim($_POST['DEUDA_BANCO']));
	$DEUDA_CAR = htmlspecialchars(trim($_POST['DEUDA_CAR']));
	$PRIMERA_FECHA_AUDIENCIA = htmlspecialchars(trim($_POST['PRIMERA_FECHA_AUDIENCIA']));
	$HORA_AUDIENCIA = htmlspecialchars(trim($_POST['HORA_AUDIENCIA']));
	$DIRECCION_AUDIENCIA = htmlspecialchars(trim($_POST['DIRECCION_AUDIENCIA']));
	$CIUDAD = htmlspecialchars(trim($_POST['CIUDAD']));
	$ESTADO_BOLETIN = htmlspecialchars(trim($_POST['ESTADO_BOLETIN']));
	
	if ( $objCliente->actualizar(array(
	$FECHA_ADMISIBILIDAD,
	$TIPO_AUDIENCIA,
	$ROL,
	$DEUDA_INFORMADA_SIR,
	$DEUDA_BANCO,
	$DEUDA_CAR,
	$PRIMERA_FECHA_AUDIENCIA,
	$HORA_AUDIENCIA,
	$DIRECCION_AUDIENCIA,
	$CIUDAD,
	$ESTADO_BOLETIN),
	$RUT_CLIENTE) == true){
	
	echo 'Datos actualizados';}
	
	else{
		echo 'Se produjo un error. Intente nuevamente';
	} 
}else{
	if(isset($_GET['id'])){
		
		require('clases/cliente.class.php');
		$objCliente=new Cliente;
		$consulta = $objCliente->mostrar_cliente($_GET['id']);
		$cliente = mysql_fetch_array($consulta);
	?>
	<form id="frmClienteActualizar" name="frmClienteActualizar" method="post" action="actualizar.php" onsubmit="ActualizarDatos(); return false">
    	
		<p align="center"> 
		<strong>Modificar Caso SIR
		<br><br>
		</strong>
		</p> 	
		
		<input type="hidden" name="RUT_CLIENTE" id="RUT_CLIENTE" value="<?php echo $cliente['RUT_CLIENTE']?>" />
       <p>
		<label>FECHA ADMISIBILIDAD<br />
		<input class="text" type="date" name="FECHA_ADMISIBILIDAD" id="FECHA_ADMISIBILIDAD" value="<?php echo $cliente['FECHA_ADMISIBILIDAD']?>" />
		</label>
	  </p>
	  <p>
		<label>TIPO AUDIENCIA<br />
		<input type="radio" name="TIPO_AUDIENCIA" id="ADMISIBILIDAD" value="ADMISIBILIDAD" <?php if($cliente['TIPO_AUDIENCIA']=="ADMISIBILIDAD") echo "checked=\"checked\""?> />
		ADMISIBILIDAD</label>
		<label>
		<input type="radio" name="TIPO_AUDIENCIA" id="LIQUIDACION VOLUNTARIA" value="LIQUIDACION VOLUNTARIA" <?php if($cliente['TIPO_AUDIENCIA']=="LIQUIDACION VOLUNTARIA") echo "checked=\"checked\""?> />
		LIQUIDACION VOLUNTARIA</label>
	  </p>
	  <p>
		<label>ROL<br />
		<input class="text" type="text" name="ROL" id="ROL" value="<?php echo $cliente['ROL']?>" />
		</label>
	  </p>
	  <p>
		<label>DEUDA INFORMADA SIR<br />
		<input class="text" type="text" name="DEUDA_INFORMADA_SIR" id="DEUDA_INFORMADA_SIR" value="<?php echo $cliente['DEUDA_INFORMADA_SIR']?>" />
		</label>
	  </p>
	  <p>
		<label>DEUDA BANCO<br />
		<input class="text" type="text" name="DEUDA_BANCO" id="DEUDA_BANCO" value="<?php echo $cliente['DEUDA_BANCO']?>" />
		</label>
	  </p>
	  <p>
		<label>DEUDA CAR<br />
		<input class="text" type="text" name="DEUDA_CAR" id="DEUDA_CAR" value="<?php echo $cliente['DEUDA_CAR']?>" />
		</label>
	  </p>
	  <p>
		<label>PRIMERA FECHA AUDIENCIA<br />
		<input class="text" type="date" name="PRIMERA_FECHA_AUDIENCIA" id="PRIMERA_FECHA_AUDIENCIA" value="<?php echo $cliente['PRIMERA_FECHA_AUDIENCIA']?>" />
		</label>
	  </p>
	  <p>
		<label>HORA AUDIENCIA<br />
		<input class="text" type="time" name="HORA_AUDIENCIA" id="HORA_AUDIENCIA" value="<?php echo $cliente['HORA_AUDIENCIA']?>" />
		</label>
	  </p>
	  <p>
		<label>DIRECCION AUDIENCIA<br />
		<input class="text" type="text" name="DIRECCION_AUDIENCIA" id="DIRECCION_AUDIENCIA" value="<?php echo $cliente['DIRECCION_AUDIENCIA']?>" />
		</label>
	  </p>
	  <p>
		<label>CIUDAD<br />
		<input class="text" type="text" name="CIUDAD" id="CIUDAD" value="<?php echo $cliente['CIUDAD']?>" />
		</label>
	  </p>
	  <p>
		<label>ESTADO BOLETIN<br />
		<select class="selectpicker" name="ESTADO_BOLETIN" data-style="btn-primary" id="ESTADO_BOLETIN" >
		<option><?php echo $cliente['ESTADO_BOLETIN']?></option>
		<option>ACUERDO DE RENEGOCIACION</option>
		<option>ACUERDO FORMALIZADO</option>
		<option>ADM.REVOCADA</option>
		<option>ADMISIBILIDAD SIN DEUDA RIPLEY</option>
		<option>AUDIENCIA DE RENEGOCIACION</option>
		<option>AUDIENCIA DETERMINACION PASIVO</option>
		<option>FINALIZADO PROCEDIMIENTO CONCURSAL DE RENEG.</option>
		<option>LIQUID. VOL .CON DEUDA</option>
		<option>LIQUID. VOLUNTARIA</option>

		</select>
		</label>
	  </p>

	  <p>
		<input type="submit" name="submit" id="button" value="Actualizar Datos" />
		<label></label>
		<input type="button" name="cancelar" id="cancelar" value="Cancelar" onclick="Cancelar()" />
	  </p>
	</form>
	<?php
	}
}
?>