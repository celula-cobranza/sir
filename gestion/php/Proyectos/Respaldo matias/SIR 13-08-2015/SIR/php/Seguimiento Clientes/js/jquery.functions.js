	function ActualizarDatos(){
		var RUT_CLIENTE = $('#RUT_CLIENTE').attr('value');
		var CONTROL_ESTADO = $('#CONTROL_ESTADO').attr('value');
		var GLOSA_OBSERVACION = $('#GLOSA_OBSERVACION').attr('value');
		

		$.ajax({
			url: 'actualizar.php',
			type: "POST",
			data: "submit=&CONTROL_ESTADO="+CONTROL_ESTADO+"&GLOSA_OBSERVACION="+GLOSA_OBSERVACION+"&RUT_CLIENTE="+RUT_CLIENTE,
			success: function(datos){
				alert(datos);
				ConsultaDatos();
				$("#formulario").hide();
				$("#tabla").show();
			}
		});
		return false;
	}
	
	function ConsultaDatos(){
		$.ajax({
			url: 'consulta.php',
			cache: false,
			type: "GET",
			success: function(datos){
				$("#tabla").html(datos);
			}
		});
	}
	

	function Cancelar(){
		$("#formulario").hide();
		$("#tabla").show();
		return false;
	}
	
	// funciones del calendario
	function update_calendar(){
		var month = $('#calendar_mes').attr('value');
		var year = $('#calendar_anio').attr('value');
	
		var valores='month='+month+'&year='+year;
	
		$.ajax({
			url: 'calendario.php',
			type: "GET",
			data: valores,
			success: function(datos){
				$("#calendario_dias").html(datos);
			}
		});
	}
	
	function set_date(date){
		$('#FECHA_ADMISIBILIDAD').attr('value',date);
		show_calendar();
	}
	
	function show_calendar(){
		$('#calendario').toggle();
	}
	