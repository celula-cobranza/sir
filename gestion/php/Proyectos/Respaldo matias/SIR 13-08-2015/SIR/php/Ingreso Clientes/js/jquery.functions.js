	function ActualizarDatos(){
		var RUT_CLIENTE = $('#RUT_CLIENTE').attr('value');
		var FECHA_ADMISIBILIDAD = $('#FECHA_ADMISIBILIDAD').attr('value');
		var TIPO_AUDIENCIA = $("input[@name='TIPO_AUDIENCIA']:checked").attr("value");
		var ROL = $("#ROL").attr("value");
		var DEUDA_INFORMADA_SIR = $("#DEUDA_INFORMADA_SIR").attr("value");
		var DEUDA_BANCO = $("#DEUDA_BANCO").attr("value");
		var DEUDA_CAR = $("#DEUDA_CAR").attr("value");
		var PRIMERA_FECHA_AUDIENCIA = $("#PRIMERA_FECHA_AUDIENCIA").attr("value");
		var HORA_AUDIENCIA = $("#HORA_AUDIENCIA").attr("value");
		var DIRECCION_AUDIENCIA = $("#DIRECCION_AUDIENCIA").attr("value");
		var CIUDAD = $("#CIUDAD").attr("value");
		var ESTADO_BOLETIN = $("#ESTADO_BOLETIN").attr("value");

		$.ajax({
			url: 'actualizar.php',
			type: "POST",
			data: "submit=&FECHA_ADMISIBILIDAD="+FECHA_ADMISIBILIDAD+"&TIPO_AUDIENCIA="+TIPO_AUDIENCIA+"&ROL="+ROL+"&DEUDA_INFORMADA_SIR="+DEUDA_INFORMADA_SIR+"&DEUDA_BANCO="+DEUDA_BANCO+"&DEUDA_CAR="+DEUDA_CAR+"&PRIMERA_FECHA_AUDIENCIA="+PRIMERA_FECHA_AUDIENCIA+"&HORA_AUDIENCIA="+HORA_AUDIENCIA+"&DIRECCION_AUDIENCIA="+DIRECCION_AUDIENCIA+"&CIUDAD="+CIUDAD+"&ESTADO_BOLETIN="+ESTADO_BOLETIN+"&RUT_CLIENTE="+RUT_CLIENTE,
			success: function(datos){
				alert(datos);
				ConsultaDatos();
				$("#formulario").hide();
				$("#tabla").show();
			}
		});
		return false;
	}
	
	function ConsultaDatos(){
		$.ajax({
			url: 'consulta.php',
			cache: false,
			type: "GET",
			success: function(datos){
				$("#tabla").html(datos);
			}
		});
	}
	
	function EliminarDato(RUT_CLIENTE){
		var msg = confirm("Desea eliminar este registro?")
		if ( msg ) {
			$.ajax({
				url: 'eliminar.php',
				type: "GET",
				data: "id="+RUT_CLIENTE,
				success: function(datos){
					alert(datos);
					$("#fila-"+RUT_CLIENTE).remove();
					window.location.reload();
				}
			});
		}
		return false;
	}
	
	function GrabarDatos(){
		var FECHA_ADMISIBILIDAD = $('#FECHA_ADMISIBILIDAD').attr('value');
		var TIPO_AUDIENCIA = $("input[@name='TIPO_AUDIENCIA']:checked").attr("value");
		var ROL = $("#ROL").attr("value");
		var NOMBRE_CLIENTE = $('#NOMBRE_CLIENTE').attr('value');
		var RUT_CLIENTE = $('#RUT_CLIENTE').attr('value');
		var DEUDA_INFORMADA_SIR = $("#DEUDA_INFORMADA_SIR").attr("value");
		var DEUDA_BANCO = $("#DEUDA_BANCO").attr("value");
		var DEUDA_CAR = $("#DEUDA_CAR").attr("value");
		var PRIMERA_FECHA_AUDIENCIA = $("#PRIMERA_FECHA_AUDIENCIA").attr("value");
		var HORA_AUDIENCIA = $("#HORA_AUDIENCIA").attr("value");
		var DIRECCION_AUDIENCIA = $("#DIRECCION_AUDIENCIA").attr("value");
		var CIUDAD = $("#CIUDAD").attr("value");
		var ESTADO_BOLETIN = $("#ESTADO_BOLETIN").attr("value");

		$.ajax({
			url: 'nuevo.php',
			type: "POST",
			data: "submit=&FECHA_ADMISIBILIDAD="+FECHA_ADMISIBILIDAD+"&TIPO_AUDIENCIA="+TIPO_AUDIENCIA+"&ROL="+ROL+"&NOMBRE_CLIENTE="+NOMBRE_CLIENTE+"&RUT_CLIENTE="+RUT_CLIENTE+"&DEUDA_INFORMADA_SIR="+DEUDA_INFORMADA_SIR+"&DEUDA_BANCO="+DEUDA_BANCO+"&DEUDA_CAR="+DEUDA_CAR+"&PRIMERA_FECHA_AUDIENCIA="+PRIMERA_FECHA_AUDIENCIA+"&HORA_AUDIENCIA="+HORA_AUDIENCIA+"&DIRECCION_AUDIENCIA="+DIRECCION_AUDIENCIA+"&CIUDAD="+CIUDAD+"&ESTADO_BOLETIN="+ESTADO_BOLETIN,
			success: function(datos){
				ConsultaDatos();
				alert(datos);
				$("#formulario").hide();
				$("#tabla").show();
			}
		});
		return false;
	}

	function Cancelar(){
		$("#formulario").hide();
		$("#tabla").show();
		return false;
	}
	
	// funciones del calendario
	function update_calendar(){
		var month = $('#calendar_mes').attr('value');
		var year = $('#calendar_anio').attr('value');
	
		var valores='month='+month+'&year='+year;
	
		$.ajax({
			url: 'calendario.php',
			type: "GET",
			data: valores,
			success: function(datos){
				$("#calendario_dias").html(datos);
			}
		});
	}
	
	function set_date(date){
		$('#FECHA_ADMISIBILIDAD').attr('value',date);
		show_calendar();
	}
	
	function show_calendar(){
		$('#calendario').toggle();
	}
	