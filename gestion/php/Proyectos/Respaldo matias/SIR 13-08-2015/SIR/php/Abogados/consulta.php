<?php
require('clases/cliente.class.php');
$objCliente=new Cliente;
$consulta=$objCliente->mostrar_clientes();
?>
<script type="text/javascript">
$(document).ready(function(){
	// mostrar formulario de actualizar datos
	$("table tr .modi a").click(function(){
		$('#tabla').hide();
		$("#formulario").show();
		$.ajax({
			url: this.href,
			type: "GET",
			success: function(datos){
				$("#formulario").html(datos);
			}
		});
		return false;
	});
	
	// llamar a formulario nuevo
	$("#nuevo a").click(function(){
		$("#formulario").show();
		$("#tabla").hide();
		$.ajax({
			type: "GET",
			url: 'nuevo.php',
			success: function(datos){
				$("#formulario").html(datos);
			}
		});
		return false;
	});
});

</script>
<span id="nuevo"><a href="nuevo.php"><img src="img/add.png" alt="Agregar dato" /></a></span>
	<table>
   		<tr>
   			<th></th>
            <th></th>
			<th>RUT ABOGADO</th>
			<th>DV</th>
			<th>NOMBRE ABOGADO</th>
			<th>VALOR RECONOCIMIENTO PASIVO</th>
			<th>VALOR RENEGOCIACION</th>
			<th>VALOR EJECUCION</th>
			<th>VALOR LIQUIDACION VOLUNTARIA</th>
        </tr>
<?php
if($consulta) {
	while( $cliente = mysql_fetch_array($consulta) ){
	?>
	
		 <tr id="fila-<?php echo $cliente['RUT_ABOGADO'] ?>">
				<td><span class="modi"><a href="actualizar.php?id=<?php echo $cliente['RUT_ABOGADO'] ?>"><img src="img/database_edit.png" title="Editar" alt="Editar" /></a></span></td>
				<td><span class="dele"><a onClick="EliminarDato(<?php echo $cliente['RUT_ABOGADO'] ?>); return false" href="eliminar.php?id=<?php echo $cliente['RUT_ABOGADO'] ?>"><img src="img/delete.png" title="Eliminar" alt="Eliminar" /></a></span></td>
				<td><?php echo $cliente['RUT_ABOGADO'] ?></td>
				<td><?php echo $cliente['DV'] ?></td>
				<td><?php echo $cliente['NOMBRE'] ?></td>
				<td><?php echo $cliente['V_RECON_PASIVO'] ?></td>
				<td><?php echo $cliente['V_RENEGOCIACION'] ?></td>
				<td><?php echo $cliente['V_EJECUCION'] ?></td>
				<td><?php echo $cliente['V_LIQ_VOLUNTARIA'] ?></td>

		  </tr>
	<?php
	}
}
?>
    </table>