<?php

// DataTables PHP library
include( "../../php/DataTables.php" );

// Alias Editor classes so they are easy to use
use
	DataTables\Editor,
	DataTables\Editor\Field,
	DataTables\Editor\Format,
	DataTables\Editor\Join,
	DataTables\Editor\Upload,
	DataTables\Editor\Validate;


/*
 * Example PHP implementation used for the joinLinkTable.html example
 */

Editor::inst( $db, 'audiencias','id_audiencia' )
	->field( 
		Field::inst( 'audiencias.fecha_audiencia' ),
		Field::inst( 'audiencias.rut_cliente' ),
		Field::inst( 'audiencias.idsubaudiencia' )
			->options( 'subaudiencias', 'idsubaudiencia', 'nomsubaudiencia' ),
		Field::inst( 'subaudiencias.nomsubaudiencia' )
	)
	->leftJoin( 'subaudiencias',     'subaudiencias.idsubaudiencia',          '=', 'audiencias.idsubaudiencia' )
	->process($_POST)
	->json();
