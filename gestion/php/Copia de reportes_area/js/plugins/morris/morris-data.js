// Morris.js Charts sample data for SB Admin template

$(function() {

    // Area Chart
    Morris.Area({
        element: 'totales',
        data: [{
            period: '2014-01',
            Totales:   167.92
           
            
        }, {
            period: '2014-02',
            Totales:   126.68
            
            
        }, {
            period: '2014-03',
            Totales:   143.39
            
            
        }, {
            period: '2014-04',
            Totales: 153.58
           
            
        }, {
            period: '2014-05',
            Totales: 162.15
           
            
        }, {
            period: '2014-06',
            Totales: 140.74
            
            
        }, {
            period: '2014-07',
            Totales: 155.08
          
            
        }, {
            period: '2014-08',
            Totales: 175.99
            
            
        }, {
            period: '2014-09',
            Totales: 162.37
           
            
        }, {
		    period: '2014-10',
            Totales: 181.29
            
            
        }, {	
		    period: '2014-11',
            Totales: 181.603
            
           
        }, {
            period: '2014-12',
            Totales: 185.36
            
            
        }],
        xkey: 'period',
        ykeys: ['Totales'],
        labels: ['Total Gastos'],
        pointSize: 2,
        hideHover: 'auto',
        resize: true
    });
	
	   // Area Chart
    Morris.Area({
        element: 'costas',
        data: [{
            period: '2014-01',
            Hipotecario:   753087,
            Consumo:   13995048
            
        }, {
            period: '2014-02',
            Hipotecario:   1056886,
            Consumo:   18822982
            
        }, {
            period: '2014-03',
            Hipotecario:   58289,
            Consumo:   6317578
            
        }, {
            period: '2014-04',
            Hipotecario: 710283,
            Consumo: 8231248
            
        }, {
            period: '2014-05',
            Hipotecario: 113000,
            Consumo: 12674357
            
        }, {
            period: '2014-06',
            Hipotecario: 251600,
            Consumo: 11904922
            
        }, {
            period: '2014-07',
            Hipotecario: 1191389,
            Consumo: 13354060
            
        }, {
            period: '2014-08',
            Hipotecario: 879229,
            Consumo: 16414446
            
        }, {
            period: '2014-09',
            Hipotecario: 367920,
            Consumo: 17604071
            
        }, {
		    period: '2014-10',
            Hipotecario: 810409,
            Consumo: 15068217
            
        }, {	
		    period: '2014-11',
            Hipotecario: 882167,
            Consumo: 18017707
           
        }, {
            period: '2014-12',
            Hipotecario: 826792,
            Consumo: 20429379
            
        }],
        xkey: 'period',
        ykeys: ['Hipotecario', 'Consumo'],
        labels: ['Hipotecario', 'Consumo'],
        pointSize: 2,
        hideHover: 'auto',
        resize: true
    });
	
	Morris.Area({
        element: 'empex',
        data: [{
            period: '2014-01',
            EmpexPrejudicial:   12116791
            
        }, {
            period: '2014-02',
            EmpexPrejudicial:   13090267
            
        }, {
            period: '2014-03',
            EmpexPrejudicial:   12473082
           
        }, {
            period: '2014-04',
            EmpexPrejudicial: 11560039
            
        }, {
            period: '2014-05',
            EmpexPrejudicial: 11668383
            
        }, {
            period: '2014-06',
            EmpexPrejudicial: 12675532
            
        }, {
            period: '2014-07',
            EmpexPrejudicial: 10943976
            
        }, {
            period: '2014-08',
            EmpexPrejudicial: 14198982
            
        }, {
            period: '2014-09',
            EmpexPrejudicial: 13973365
           
        }, {
		    period: '2014-10',
            EmpexPrejudicial: 15436653
            
        }, {	
		    period: '2014-11',
            EmpexPrejudicial: 13858114
            
        }, {
            period: '2014-12',
            EmpexPrejudicial: 13542081
            
        }],
        xkey: 'period',
        ykeys: ['EmpexPrejudicial'],
        labels: ['Empex Prejudicial'],
        pointSize: 2,
        hideHover: 'auto',
        resize: true
    });
	
	Morris.Area({
        element: 'honorario-abogado',
        data: [{
            period: '2014-01',
			GCSCastigo:  2032324,
            Hipotecario:   20939954,
            Consumo:   14679230
			
            
        }, {
            period: '2014-02',
			GCSCastigo:  784706,
            Hipotecario:   809247,
            Consumo:   9365452
			
           
        }, {
            period: '2014-03',
			GCSCastigo:  893907,
            Hipotecario:   8586332,
            Consumo:   13165917
			
          
        }, {
            period: '2014-04',
			GCSCastigo:  1260528,
            Hipotecario: 6399256,
            Consumo:   17548064
			
            
        }, {
            period: '2014-05',
			GCSCastigo:  1198710,
            Hipotecario: 7194300,
            Consumo: 20609354
			
           
        }, {
            period: '2014-06',
			GCSCastigo:   803655,
            Hipotecario: 1744435,
            Consumo: 19055369
			
            
        }, {
            period: '2014-07',
			GCSCastigo:  864799,
            Hipotecario: 4805951,
            Consumo: 20482359
			
            
        }, {
            period: '2014-08',
			GCSCastigo: 982665,
            Hipotecario: 9666198,
            Consumo: 26010789
			
            
        }, {
            period: '2014-09',
			GCSCastigo: 1085557,
            Hipotecario: 4011604,
            Consumo: 21964625
		
           
        }, {
		    period: '2014-10',
			GCSCastigo: 933980,
            Hipotecario: 12579612,
            Consumo: 23684908
			
            
        }, {	
		    period: '2014-11',
			GCSCastigo: 644402,
            Hipotecario: 13969686,
            Consumo: 24889475
			
            
        }, {
            period: '2014-12',
			GCSCastigo: 601660,
            Hipotecario: 10420586,
            Consumo: 26700639
			
            
        }],
        xkey: 'period',
        ykeys: ['GCSCastigo','Hipotecario', 'Consumo'],
        labels: ['GCS Castigo','Hipotecario', 'Consumo'],
        pointSize: 2,
        hideHover: 'auto',
        resize: true
    });
	
	Morris.Area({
        element: 'honorario-payback',
        data: [{
            period: '2014-01',
			SobreCapacity: 2421277,
			Campañas: 4623390,
			PaybackCastigo:   30786367,
            PaybackVigente:   65574663    
        }, {
            period: '2014-02',
            SobreCapacity: 2836353,
			Campañas: 0,
			PaybackCastigo:   24990972,
            PaybackVigente:   54943103 
        }, {
            period: '2014-03',
            SobreCapacity: 3018213,
			Campañas: 6719087,
			PaybackCastigo:   29062755,
            PaybackVigente:   63189422 
        }, {
            period: '2014-04',
            SobreCapacity: 3156572,
			Campañas: 13766180,
			PaybackCastigo:   28018335,
            PaybackVigente:   62932603 
        }, {
            period: '2014-05',
            SobreCapacity: 3364110,
			Campañas: 6074418,
			PaybackCastigo:   29962248,
            PaybackVigente:   69298298 
        }, {
            period: '2014-06',
            SobreCapacity: 3588942,
			Campañas: 0,
			PaybackCastigo:   27726919,
            PaybackVigente:   62997026 
        }, {
            period: '2014-07',
            SobreCapacity: 3346815,
			Campañas: 2160000,
			PaybackCastigo:   28402535,
            PaybackVigente:   67573011 
        }, {
            period: '2014-08',
            SobreCapacity: 3398699,
			Campañas: 10311715,
			PaybackCastigo:   25991599,
            PaybackVigente:   68144580 
        }, {
            period: '2014-09',
            SobreCapacity: 3692711,
			Campañas: 5593584,
			PaybackCastigo:   26221145,
            PaybackVigente:   67860821 
        }, {
		    period: '2014-10',
            SobreCapacity: 3744596,
			Campañas: 9562920,
			PaybackCastigo:   29998824,
            PaybackVigente:   69475879 
        }, {	
		    period: '2014-11',
            SobreCapacity: 3381404,
			Campañas: 10439427,
			PaybackCastigo:   26467523,
            PaybackVigente:   69053299 
        }, {
            period: '2014-12',
            SobreCapacity: 4869289,
			Campañas: 13117367,
			PaybackCastigo:   26830935,
            PaybackVigente:   68029184 
        }],
        xkey: 'period',
        ykeys: ['SobreCapacity', 'Campañas', 'PaybackCastigo','PaybackVigente'],
        labels: ['Sobre Capacity', 'Campañas', 'Payback Castigo','Payback Vigente'],
        pointSize: 2,
        hideHover: 'auto',
        resize: true
    });
	
	
	
    // Donut Chart
    Morris.Donut({
        element: 'morris-donut-chart',
        data: [{
            label: "Empex",
            value: 155.53
        }, {
            label: "H. Abogados",
            value: 339.28
        }, {
            label: "Costas",
            value: 182.59
	    }, {
            label: "H. Payback",
            value: 1246.71
		}, {
		    label: "GCS Castigo",
            value: 12.08
        }],
		
        resize: true
    });
	
	

    // Line Chart
    Morris.Line({
        // ID of the element in which to draw the chart.
        element: 'morris-line-chart',
        // Chart data records -- each entry in this array corresponds to a point on
        // the chart.
        data: [{
            d: '2014-01',
            visits: 167.92,
			ver: 52.88
        }, {
            d: '2014-02',
            visits: 126.68,
			presupuesto: 0
        }, {
            d: '2014-03',
            visits: 143.39,
			presupuesto: 0
        }, {
            d: '2014-04',
            visits: 153.58,
			presupuesto: 0
        }, {
            d: '2014-05',
            visits: 162.15,
			presupuesto: 0
        }, {
            d: '2014-06',
            visits: 140.74,
			presupuesto: 0
        }, {
            d: '2014-07',
            visits: 155.08,
			presupuesto: 0
        }, {
            d: '2014-08',
            visits: 175.99,
			presupuesto: 0
        }, {
            d: '2014-09',
            visits: 162.37,
			presupuesto: 0
        }, {
            d: '2014-10',
            visits: 181.29,
			presupuesto: 0
        }, {
            d: '2014-11',
            visits: 181.60,
			presupuesto: 0
        }, {
            d: '2014-12',
            visits: 185.36,
			presupuesto: 0
        }, ],
        // The name of the data record attribute that contains x-visitss.
        xkey: 'd',
        // A list of names of data record attributes that contain y-visitss.
        ykeys: ['visits','presupuesto'],
        // Labels for the ykeys -- will be displayed when you hover over the
        // chart.
        labels: ['Gasto Total','Presupuesto'],
        // Disables line smoothing
        smooth: false,
        resize: true
    });
	
	
	    // Bar Chart
    Morris.Bar({
        element: 'morris-bar-chart',
        data: [{
            device: 'GCS Castigo',
            geekbench: 12.08
		}, {
            device: 'Empex',
            geekbench: 155.53
        }, {
            device: 'Costas',
            geekbench: 182.59
        }, {
            device: 'H. Abogado',
            geekbench: 339.28
        }, {
            device: 'H. Payback',
            geekbench: 1246.71
        }],
        xkey: 'device',
        ykeys: ['geekbench'],
        labels: ['Gasto Total'],
        barRatio: 0.4,
        xLabelAngle: 35,
        hideHover: 'auto',
        resize: true
    });


});
