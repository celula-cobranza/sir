<?php
require('clases/cliente.class.php');
$objCliente=new Cliente;
$consulta=$objCliente->mostrar_clientes();
?>
<script type="text/javascript">
$(document).ready(function(){
	// mostrar formulario de actualizar datos
	$("table tr .modi a").click(function(){
		$('#tabla').hide();
		$("#formulario").show();
		$.ajax({
			url: this.href,
			type: "GET",
			success: function(datos){
				$("#formulario").html(datos);
			}
		});
		return false;
	});
	
	// llamar a formulario nuevo
	$("#nuevo a").click(function(){
		$("#formulario").show();
		$("#tabla").hide();
		$.ajax({
			type: "GET",
			url: 'nuevo.php',
			success: function(datos){
				$("#formulario").html(datos);
			}
		});
		return false;
	});
});

</script>
<span id="nuevo"><a href="nuevo.php"><img src="img/add.png" alt="Agregar dato" /></a></span>
	<table id="mitabla" class="table display compact" cellspacing="0" width="100%">
	<thead>
   		<tr>
   			<th></th>
            <th></th>
			<th>FECHA AUDIENCIA</th>
			<th>HORA AUDIENCIA</th>
			<th>TIPO AUDIENCIA</th>
			<th>RUT CLIENTE</th>
			<th>DV</th>
			<th>NOMBRE CLIENTE</th>
			<th>CIUDAD</th>
			<th>ABOGADO</th>
			<th>RUT ABOGADO</th>
			<th>ASISTENCIA</th>
        </tr>
    </thead>

<tfoot>
</tfoot>
    </table>
    <?php 
    	$data =  array();
    	if($consulta)
    		{
    			while( $cliente = mysql_fetch_array($consulta) ){
    				$row = array();
    				$row[] = '<span class="modi"><a href="actualizar.php?id='.$cliente['ID_AUDIENCIA'].'"><img src="img/database_edit.png" title="Editar" alt="Editar" /></a></span>';
    				$row[] = '<span class="dele"><a onClick="EliminarDato('.$cliente['ID_AUDIENCIA'].'); return false" href="eliminar.php?id='.$cliente['ID_AUDIENCIA'].'"><img src="img/delete.png" title="Eliminar" alt="Eliminar" /></a></span>';
    				$row[] = $cliente['FECHA_AUDIENCIA'];
    				$row[] = $cliente['HORA_AUDIENCIA'];
    				$row[] = $cliente['TIPO_AUDIENCIA'];
    				$row[] = $cliente['RUT_CLIENTE'];
    				$row[] = $cliente['DV'];
    				$row[] = $cliente['NOMBRE_CLIENTE'];
    				$row[] = $cliente['CIUDAD'];
    				$row[] = $cliente['ABOGADO'];
    				$row[] = $cliente['RUT_ABOGADO'];
    				$row[] = $cliente['ASISTENCIA'];
    				//$results[] =$cliente;
    				$results[] =$row;
    			}
    		}
		$data = $results;
		//var_dump($data);
    ?>



    <script type="text/javascript">
		var jsvar = <?php echo json_encode($data); ?>;
    	
    	//console.log(jsvar);
    	    		
    	$(document).ready( function () {
    		$('#mitabla').DataTable({
			    language: {
			        url :"js/Spanish.json"
			    },
			    data:jsvar, 
			    scrollY: "250px",
        		scrollCollapse: true
			});
		});
    </script>