	function ActualizarDatos(){
		var RUT_ABOGADO = $('#RUT_ABOGADO').attr('value');
		var V_RECON_PASIVO = $("#V_RECON_PASIVO").attr("value");
		var V_RENEGOCIACION = $("#V_RENEGOCIACION").attr("value");
		var V_EJECUCION = $("#V_EJECUCION").attr("value");
		var V_LIQ_VOLUNTARIA = $("#V_LIQ_VOLUNTARIA").attr("value");
		var CIUDAD = $("#CIUDAD").attr("value");

		$.ajax({
			url: 'actualizar.php',
			type: "POST",
			data: "submit=&V_RECON_PASIVO="+V_RECON_PASIVO+"&V_RENEGOCIACION="+V_RENEGOCIACION+"&V_EJECUCION="+V_EJECUCION+"&V_LIQ_VOLUNTARIA="+V_LIQ_VOLUNTARIA+"&CIUDAD="+CIUDAD+"&RUT_ABOGADO="+RUT_ABOGADO,
			success: function(datos){
				alert(datos);
				ConsultaDatos();
				$("#formulario").hide();
				$("#tabla").show();
			}
		});
		return false;
	}
	
	function ConsultaDatos(){
		$.ajax({
			url: 'consulta.php',
			cache: false,
			type: "GET",
			success: function(datos){
				$("#tabla").html(datos);
			}
		});
	}
	
	function EliminarDato(RUT_ABOGADO){
		var msg = confirm("Desea eliminar este registro?")
		if ( msg ) {
			$.ajax({
				url: 'eliminar.php',
				type: "GET",
				data: "id="+RUT_ABOGADO,
				success: function(datos){
					alert(datos);
					$("#fila-"+RUT_ABOGADO).remove();
					window.location.reload();
				}
			});
		}
		return false;
	}
	
	function GrabarDatos(){
		var RUT_ABOGADO = $('#RUT_ABOGADO').attr('value');
		var DV = $('#DV').attr('value');
		var NOMBRE = $('#NOMBRE').attr('value');
		var V_RECON_PASIVO = $("#V_RECON_PASIVO").attr("value");
		var V_RENEGOCIACION = $("#V_RENEGOCIACION").attr("value");
		var V_EJECUCION = $("#V_EJECUCION").attr("value");
		var V_LIQ_VOLUNTARIA = $("#V_LIQ_VOLUNTARIA").attr("value");
		var CIUDAD = $("#CIUDAD").attr("value");

		$.ajax({
			url: 'nuevo.php',
			type: "POST",
			data: "submit=&V_RECON_PASIVO="+V_RECON_PASIVO+"&V_RENEGOCIACION="+V_RENEGOCIACION+"&V_EJECUCION="+V_EJECUCION+"&V_LIQ_VOLUNTARIA="+V_LIQ_VOLUNTARIA+"&CIUDAD="+CIUDAD+"&RUT_ABOGADO="+RUT_ABOGADO+"&DV="+DV+"&NOMBRE="+NOMBRE,
			success: function(datos){
				ConsultaDatos();
				alert(datos);
				$("#formulario").hide();
				$("#tabla").show();
			}
		});
		return false;
	}

	function Cancelar(){
		$("#formulario").hide();
		$("#tabla").show();
		return false;
	}
	
	// funciones del calendario
	function update_calendar(){
		var month = $('#calendar_mes').attr('value');
		var year = $('#calendar_anio').attr('value');
	
		var valores='month='+month+'&year='+year;
	
		$.ajax({
			url: 'calendario.php',
			type: "GET",
			data: valores,
			success: function(datos){
				$("#calendario_dias").html(datos);
			}
		});
	}
	
	function set_date(date){
		$('#FECHA_ADMISIBILIDAD').attr('value',date);
		show_calendar();
	}
	
	function show_calendar(){
		$('#calendario').toggle();
	}
	