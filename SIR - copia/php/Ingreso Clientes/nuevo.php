<?php
require('functions.php');
if(isset($_POST['submit'])){
	require('clases/cliente.class.php');

	$FECHA_ADMISIBILIDAD = htmlspecialchars(trim($_POST['FECHA_ADMISIBILIDAD']));
	$TIPO_AUDIENCIA = htmlspecialchars(trim($_POST['TIPO_AUDIENCIA']));
	$ROL = htmlspecialchars(trim($_POST['ROL']));
	$NOMBRE_CLIENTE = htmlspecialchars(trim($_POST['NOMBRE_CLIENTE']));
	$RUT_CLIENTE = htmlspecialchars(trim($_POST['RUT_CLIENTE']));
	$DEUDA_INFORMADA_SIR = htmlspecialchars(trim($_POST['DEUDA_INFORMADA_SIR']));
	$DEUDA_BANCO = htmlspecialchars(trim($_POST['DEUDA_BANCO']));
	$DEUDA_CAR = htmlspecialchars(trim($_POST['DEUDA_CAR']));
	$FECHA_PRIMERA_AUDIENCIA = htmlspecialchars(trim($_POST['FECHA_PRIMERA_AUDIENCIA']));
	$HORA_PRIMERA_AUDIENCIA = htmlspecialchars(trim($_POST['HORA_PRIMERA_AUDIENCIA']));
	$FECHA_RENE = htmlspecialchars(trim($_POST['FECHA_RENE']));
	$HORA_RENE = htmlspecialchars(trim($_POST['HORA_RENE']));
	$DIRECCION_AUDIENCIA = htmlspecialchars(trim($_POST['DIRECCION_AUDIENCIA']));
	$CIUDAD = htmlspecialchars(trim($_POST['CIUDAD']));
	$ESTADO_BOLETIN = htmlspecialchars(trim($_POST['ESTADO_BOLETIN']));
	$OPERACION_CAR = htmlspecialchars(trim($_POST['OPERACION_CAR']));
	$OPERACION_BANCO = htmlspecialchars(trim($_POST['OPERACION_BANCO']));
	$ACUERDO_ESTADO_FIRMA = htmlspecialchars(trim($_POST['ACUERDO_ESTADO_FIRMA']));
	$ACUERDO_PRIMER_VECTO = htmlspecialchars(trim($_POST['ACUERDO_PRIMER_VECTO']));
	$GLOSA_OBS = htmlspecialchars(trim($_POST['GLOSA_OBS']));
	
	$objCliente=new Cliente;
	if ( $objCliente->insertar(array(
	$FECHA_ADMISIBILIDAD,
	$TIPO_AUDIENCIA,
	$ROL,
	$NOMBRE_CLIENTE,
	$RUT_CLIENTE,
	$DEUDA_INFORMADA_SIR,
	$DEUDA_BANCO,
	$DEUDA_CAR,
	$FECHA_PRIMERA_AUDIENCIA,
	$HORA_PRIMERA_AUDIENCIA,
	$FECHA_RENE,
	$HORA_RENE,
	$DIRECCION_AUDIENCIA,
	$CIUDAD,
	$ESTADO_BOLETIN,
	$OPERACION_CAR,
	$OPERACION_BANCO,
	$ACUERDO_ESTADO_FIRMA,
	$ACUERDO_PRIMER_VECTO,
	$GLOSA_OBS)) == true)
	{
		echo 'Datos guardados';
	}else{
		echo 'Se produjo un error. Intente nuevamente';
	} 
}else{
?>

<form id="frmClienteNuevo" name="frmClienteNuevo" method="post" action="nuevo.php" onsubmit="GrabarDatos(); return false">
  
		<p align="center"> 
		<strong>Ingresar Nuevo Caso SIR
		<br><br>
		</strong>
		</p>
  
		<p>
		<label>RUT CLIENTE (SIN DIGITO VERIFICADOR)<br />
		<input class="text" type="text" name="RUT_CLIENTE" id="RUT_CLIENTE" />
		</label>
		</p>
		
		<p>
		<label>NOMBRE CLIENTE<br />
		<input class="text" type="text" name="NOMBRE_CLIENTE" id="NOMBRE_CLIENTE" />
		</label>
		</p>	
		
		<p>
		<label>FECHA_ADMISIBILIDAD<br />
		<input class="text" type="date" name="FECHA_ADMISIBILIDAD" id="FECHA_ADMISIBILIDAD" />
		</label>
		</p>
		
		<p>
		<label>TIPO AUDIENCIA<br />
		<input type="radio" name="TIPO_AUDIENCIA" id="ADMISIBILIDAD" value="ADMISIBILIDAD" />
		ADMISIBILIDAD</label>
		<label>
		<input type="radio" name="TIPO_AUDIENCIA" id="LIQUIDACION VOLUNTARIA" value="LIQUIDACION VOLUNTARIA" />
		LIQUIDACION VOLUNTARIA</label>
		</p>

		<p>
		<label>ROL<br />
		<input class="text" type="text" name="ROL" id="ROL" />
		</label>
		</p>
	  
		<p>
		<label>DEUDA INFORMADA SIR<br />
		<input class="text" type="text" name="DEUDA_INFORMADA_SIR" id="DEUDA_INFORMADA_SIR" />
		</label>
		</p>
	  
		<p>
		<label>DEUDA BANCO<br />
		<input class="text" type="text" name="DEUDA_BANCO" id="DEUDA_BANCO" />
		</label>
		</p>
		
		<p>
		<label>DEUDA CAR<br />
		<input class="text" type="text" name="DEUDA_CAR" id="DEUDA_CAR" />
		</label>
		</p>
	  
		<p>
		<label>FECHA PRIMERA AUDIENCIA<br />
		<input class="text" type="date" name="FECHA_PRIMERA_AUDIENCIA" id="FECHA_PRIMERA_AUDIENCIA" />
		</label>
		</p>
		
		<p>
		<label>HORA PRIMERA AUDIENCIA<br />
		<input class="text" type="time" name="HORA_PRIMERA_AUDIENCIA" id="HORA_PRIMERA_AUDIENCIA" />
		</label>
		</p>
		
		<p>
		<label>FECHA RENE<br />
		<input class="text" type="date" name="FECHA_RENE" id="FECHA_RENE" />
		</label>
		</p>
		
		<p>
		<label>HORA RENE<br />
		<input class="text" type="time" name="HORA_RENE" id="HORA_RENE" />
		</label>
		</p>
		
		<p>
		<label>DIRECCION AUDIENCIA<br />
		<input class="text" type="text" name="DIRECCION_AUDIENCIA" id="DIRECCION_AUDIENCIA" />
		</label>
		</p>
		
		<p>
		<label>CIUDAD<br />
		<input class="text" type="text" name="CIUDAD" id="CIUDAD" />
		</label>
		</p>
		
		<p>
		<label>ESTADO BOLETIN<br />
		<select class="selectpicker" name="ESTADO_BOLETIN" data-style="btn-primary" id="ESTADO_BOLETIN" >
		<option></option>
		<option>ACUERDO DE RENEGOCIACION</option>
		<option>ACUERDO FORMALIZADO</option>
		<option>ADM.REVOCADA</option>
		<option>ADMISIBILIDAD SIN DEUDA RIPLEY</option>
		<option>AUDIENCIA DE RENEGOCIACION</option>
		<option>AUDIENCIA DETERMINACION PASIVO</option>
		<option>FINALIZADO PROCEDIMIENTO CONCURSAL DE RENEG.</option>
		<option>LIQUID. VOL .CON DEUDA</option>
		<option>LIQUID. VOLUNTARIA</option>
		</select>
		</label>
		</p>
		
		<p>
		<label>OPERACION CAR<br />
		<input class="text" type="text" name="OPERACION_CAR" id="OPERACION_CAR" />
		</label>
		</p>
		
		<p>
		<label>OPERACION BANCO<br />
		<input class="text" type="text" name="OPERACION_BANCO" id="OPERACION_BANCO" />
		</label>
		</p>
		
		<p>
		<label>ACUERDO: ESTADO DE LA FIRMA<br />
		<select class="selectpicker" name="ACUERDO_ESTADO_FIRMA" data-style="btn-primary" id="ACUERDO_ESTADO_FIRMA" >
		<option>SIN FIRMAR</option>
		<option>FIRMADO</option>
		</select>
		</label>
		</p>
		
		<p>
		<label>ACUERDO: FECHA PRIMER VENCIMIENTO<br />
		<input class="text" type="date" name="ACUERDO_PRIMER_VECTO" id="ACUERDO_PRIMER_VECTO" />
		</label>
		</p>
		
		<p>
		<label>GLOSA OBSERVACION<br />
		<textarea name="GLOSA_OBS" rows="20" cols="65" type="text" id="GLOSA_OBS" ></textarea>
		</label>
	  </p>

		<p>
		<input type="submit" name="submit" id="button" value="Guardar" />
		<label></label>
		<input type="button" class="cancelar" name="cancelar" id="cancelar" value="Cancelar" onclick="Cancelar()" />
		</p>
</form>
<?php
}
?>