	function ActualizarDatos(){
		var RUT_CLIENTE = $('#RUT_CLIENTE').attr('value');
		var FECHA_ADMISIBILIDAD = $('#FECHA_ADMISIBILIDAD').attr('value');
		var TIPO_AUDIENCIA = $("input[@name='TIPO_AUDIENCIA']:checked").attr("value");
		var ROL = $("#ROL").attr("value");
		var DEUDA_INFORMADA_SIR = $("#DEUDA_INFORMADA_SIR").attr("value");
		var DEUDA_BANCO = $("#DEUDA_BANCO").attr("value");
		var DEUDA_CAR = $("#DEUDA_CAR").attr("value");
		var FECHA_PRIMERA_AUDIENCIA = $("#FECHA_PRIMERA_AUDIENCIA").attr("value");
		var HORA_PRIMERA_AUDIENCIA = $("#HORA_PRIMERA_AUDIENCIA").attr("value");
		var FECHA_RENE = $("#FECHA_RENE").attr("value");
		var HORA_RENE = $("#HORA_RENE").attr("value");
		var DIRECCION_AUDIENCIA = $("#DIRECCION_AUDIENCIA").attr("value");
		var CIUDAD = $("#CIUDAD").attr("value");
		var ESTADO_BOLETIN = $("#ESTADO_BOLETIN").attr("value");
		var OPERACION_CAR = $("#OPERACION_CAR").attr("value");
		var OPERACION_BANCO = $("#OPERACION_BANCO").attr("value");
		var ACUERDO_ESTADO_FIRMA = $("#ACUERDO_ESTADO_FIRMA").attr("value");
		var ACUERDO_PRIMER_VECTO = $("#ACUERDO_PRIMER_VECTO").attr("value");
		var GLOSA_OBS = $("#GLOSA_OBS").attr("value");
		
		$.ajax({
			url: 'actualizar.php',
			type: "POST",
			data: "submit=&FECHA_ADMISIBILIDAD="+FECHA_ADMISIBILIDAD+"&TIPO_AUDIENCIA="+TIPO_AUDIENCIA+"&ROL="+ROL+"&DEUDA_INFORMADA_SIR="+DEUDA_INFORMADA_SIR+"&DEUDA_BANCO="+DEUDA_BANCO+"&DEUDA_CAR="+DEUDA_CAR+"&FECHA_PRIMERA_AUDIENCIA="+FECHA_PRIMERA_AUDIENCIA+"&HORA_PRIMERA_AUDIENCIA="+HORA_PRIMERA_AUDIENCIA+"&FECHA_RENE="+FECHA_RENE+"&HORA_RENE="+HORA_RENE+"&DIRECCION_AUDIENCIA="+DIRECCION_AUDIENCIA+"&CIUDAD="+CIUDAD+"&ESTADO_BOLETIN="+ESTADO_BOLETIN+"&OPERACION_CAR="+OPERACION_CAR+"&OPERACION_BANCO="+OPERACION_BANCO+"&ACUERDO_ESTADO_FIRMA="+ACUERDO_ESTADO_FIRMA+"&ACUERDO_PRIMER_VECTO="+ACUERDO_PRIMER_VECTO+"&GLOSA_OBS="+GLOSA_OBS+"&RUT_CLIENTE="+RUT_CLIENTE,
			success: function(datos){
				alert(datos);
				ConsultaDatos();
				$("#formulario").hide();
				$("#tabla").show();
			}
		});
		return false;
	}
	
	function ConsultaDatos(){
		$.ajax({
			url: 'consulta.php',
			cache: false,
			type: "GET",
			success: function(datos){
				$("#tabla").html(datos);
			}
		});
	}
	
	function EliminarDato(RUT_CLIENTE){
		var msg = confirm("Desea eliminar este registro?")
		if ( msg ) {
			$.ajax({
				url: 'eliminar.php',
				type: "GET",
				data: "id="+RUT_CLIENTE,
				success: function(datos){
					alert(datos);
					$("#fila-"+RUT_CLIENTE).remove();
					window.location.reload();
				}
			});
		}
		return false;
	}
	
	function GrabarDatos(){
		var FECHA_ADMISIBILIDAD = $('#FECHA_ADMISIBILIDAD').attr('value');
		var TIPO_AUDIENCIA = $("input[@name='TIPO_AUDIENCIA']:checked").attr("value");
		var ROL = $("#ROL").attr("value");
		var NOMBRE_CLIENTE = $('#NOMBRE_CLIENTE').attr('value');
		var RUT_CLIENTE = $('#RUT_CLIENTE').attr('value');
		var DEUDA_INFORMADA_SIR = $("#DEUDA_INFORMADA_SIR").attr("value");
		var DEUDA_BANCO = $("#DEUDA_BANCO").attr("value");
		var DEUDA_CAR = $("#DEUDA_CAR").attr("value");
		var FECHA_PRIMERA_AUDIENCIA = $("#FECHA_PRIMERA_AUDIENCIA").attr("value");
		var HORA_PRIMERA_AUDIENCIA = $("#HORA_PRIMERA_AUDIENCIA").attr("value");
		var FECHA_RENE = $("#FECHA_RENE").attr("value");
		var HORA_RENE = $("#HORA_RENE").attr("value");
		var DIRECCION_AUDIENCIA = $("#DIRECCION_AUDIENCIA").attr("value");
		var CIUDAD = $("#CIUDAD").attr("value");
		var ESTADO_BOLETIN = $("#ESTADO_BOLETIN").attr("value");
		var OPERACION_CAR = $("#OPERACION_CAR").attr("value");
		var OPERACION_BANCO = $("#OPERACION_BANCO").attr("value");
		var ACUERDO_ESTADO_FIRMA = $("#ACUERDO_ESTADO_FIRMA").attr("value");
		var ACUERDO_PRIMER_VECTO = $("#ACUERDO_PRIMER_VECTO").attr("value");
		var GLOSA_OBS = $("#GLOSA_OBS").attr("value");

		$.ajax({
			url: 'nuevo.php',
			type: "POST",
			data: "submit=&FECHA_ADMISIBILIDAD="+FECHA_ADMISIBILIDAD+"&TIPO_AUDIENCIA="+TIPO_AUDIENCIA+"&ROL="+ROL+"&NOMBRE_CLIENTE="+NOMBRE_CLIENTE+"&RUT_CLIENTE="+RUT_CLIENTE+"&DEUDA_INFORMADA_SIR="+DEUDA_INFORMADA_SIR+"&DEUDA_BANCO="+DEUDA_BANCO+"&DEUDA_CAR="+DEUDA_CAR+"&FECHA_PRIMERA_AUDIENCIA="+FECHA_PRIMERA_AUDIENCIA+"&HORA_PRIMERA_AUDIENCIA="+HORA_PRIMERA_AUDIENCIA+"&FECHA_RENE="+FECHA_RENE+"&HORA_RENE="+HORA_RENE+"&DIRECCION_AUDIENCIA="+DIRECCION_AUDIENCIA+"&CIUDAD="+CIUDAD+"&ESTADO_BOLETIN="+ESTADO_BOLETIN+"&OPERACION_CAR="+OPERACION_CAR+"&OPERACION_BANCO="+OPERACION_BANCO+"&ACUERDO_ESTADO_FIRMA="+ACUERDO_ESTADO_FIRMA+"&ACUERDO_PRIMER_VECTO="+ACUERDO_PRIMER_VECTO+"&GLOSA_OBS="+GLOSA_OBS,
			success: function(datos){
				ConsultaDatos();
				alert(datos);
				$("#formulario").hide();
				$("#tabla").show();
			}
		});
		return false;
	}

	function Cancelar(){
		$("#formulario").hide();
		$("#tabla").show();
		return false;
	}
	
	// funciones del calendario
	function update_calendar(){
		var month = $('#calendar_mes').attr('value');
		var year = $('#calendar_anio').attr('value');
	
		var valores='month='+month+'&year='+year;
	
		$.ajax({
			url: 'calendario.php',
			type: "GET",
			data: valores,
			success: function(datos){
				$("#calendario_dias").html(datos);
			}
		});
	}
	
	function set_date(date){
		$('#FECHA_ADMISIBILIDAD').attr('value',date);
		show_calendar();
	}
	
	function show_calendar(){
		$('#calendario').toggle();
	}
	