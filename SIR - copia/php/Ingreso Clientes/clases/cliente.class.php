<?php 
include_once("conexion.class.php");

class Cliente{
 //constructor	
 	var $con;
 	function Cliente(){
 		$this->con=new DBManager;
 	}

	function insertar($campos){
		if($this->con->conectar()==true){
			//print_r($campos);
			//echo "INSERT INTO cliente (nombres, ciudad, sexo, telefono, fecha_nacimiento) VALUES ('".$campos[0]."', '".$campos[1]."','".$campos[2]."','".$campos[3]."','".$campos[4]."')";
			return mysql_query("
			INSERT INTO clientes (
			FECHA_ADMISIBILIDAD,
			TIPO_AUDIENCIA,
			ROL,
			NOMBRE_CLIENTE,
			RUT_CLIENTE,
			DEUDA_INFORMADA_SIR,
			DEUDA_BANCO,
			DEUDA_CAR,
			FECHA_PRIMERA_AUDIENCIA,
			HORA_PRIMERA_AUDIENCIA,
			FECHA_RENE,
			HORA_RENE,
			DIRECCION_AUDIENCIA,
			CIUDAD,
			ESTADO_BOLETIN,
			OPERACION_CAR,
			OPERACION_BANCO,
			ACUERDO_ESTADO_FIRMA,
			ACUERDO_PRIMER_VECTO,
			GLOSA_OBS,
			FECHA_CARGA) 
			VALUES (
			'".$campos[0]."', 
			'".$campos[1]."',
			'".$campos[2]."',
			'".$campos[3]."',
			'".$campos[4]."',
			'".$campos[5]."',
			'".$campos[6]."',
			'".$campos[7]."',
			'".$campos[8]."',
			'".$campos[9]."',
			'".$campos[10]."',
			'".$campos[11]."',
			'".$campos[12]."',
			'".$campos[13]."',
			'".$campos[14]."',
			'".$campos[15]."',
			'".$campos[16]."',
			'".$campos[17]."',
			'".$campos[18]."',
			'".$campos[19]."',
			SYSDATE())");
		}
	}
	
	function actualizar($campos,$id){
		if($this->con->conectar()==true){
			//print_r($campos);
			return mysql_query("
			UPDATE clientes
			SET
			FECHA_ADMISIBILIDAD = '".$campos[0]."',
			TIPO_AUDIENCIA = '".$campos[1]."',
			ROL = '".$campos[2]."',
			DEUDA_INFORMADA_SIR = '".$campos[3]."',
			DEUDA_BANCO = '".$campos[4]."',
			DEUDA_CAR = '".$campos[5]."',
			FECHA_PRIMERA_AUDIENCIA = '".$campos[6]."',
			HORA_PRIMERA_AUDIENCIA = '".$campos[7]."',
			FECHA_RENE = '".$campos[8]."',
			HORA_RENE = '".$campos[9]."',
			DIRECCION_AUDIENCIA = '".$campos[10]."',
			CIUDAD = '".$campos[11]."',
			ESTADO_BOLETIN = '".$campos[12]."',
			OPERACION_CAR = '".$campos[13]."',
			OPERACION_BANCO = '".$campos[14]."',
			ACUERDO_ESTADO_FIRMA = '".$campos[15]."',
			ACUERDO_PRIMER_VECTO = '".$campos[16]."',
			GLOSA_OBS = '".$campos[17]."'
			WHERE RUT_CLIENTE = ".$id);
		}
	}
	
	function mostrar_cliente($id){
		if($this->con->conectar()==true){
			return mysql_query("
			SELECT * 
			FROM clientes 
			WHERE RUT_CLIENTE =".$id);
		}
	}

	function mostrar_clientes(){
		if($this->con->conectar()==true){
			return mysql_query("
			SELECT * 
			FROM vclientes 
			ORDER BY FECHA_ADMISIBILIDAD DESC");
		}
	}

	function eliminar($id){
		if($this->con->conectar()==true){
			return mysql_query("
			DELETE 
			FROM clientes
			WHERE RUT_CLIENTE=".$id);
		}
	}
}
?>