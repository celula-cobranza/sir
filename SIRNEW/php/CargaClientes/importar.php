<?php
	$tipo = $_FILES['archivo']['type'];
	$tamanio = $_FILES['archivo']['size'];
	$archivotmp = $_FILES['archivo']['tmp_name'];
	$respuesta = new stdClass();
	
	if( $tipo == 'text/plain'){
		
		$archivo = "archivos/carga.txt";
	
		if(move_uploaded_file($archivotmp, $archivo) ){
	 		$respuesta->estado = true;		
		} else {
    		$respuesta->estado = false;
			$respuesta->mensaje = "El archivo no se pudo subir al servidor";
		}
	
		if($respuesta->estado){
		
			$lineas = file('archivos/carga.txt');

			$respuesta->mensaje = "";
			$respuesta->estado = true;
			$conexion = new mysqli('localhost','root','','sir');
			
			$conexion->query("CREATE TABLE IF NOT EXISTS tbltransit (
							  RUT_CLIENTE int(8) NOT NULL,
							  NOMBRE_CLIENTE varchar(80) COLLATE latin1_spanish_ci NOT NULL,
							  ROL varchar(15) COLLATE latin1_spanish_ci NOT NULL COMMENT 'Rol de la causa SIR',
							  FECHA_ADMISIBILIDAD date NOT NULL,
							  TIPO_AUDIENCIA enum('ADMISIBILIDAD','LIQUIDACION VOLUNTARIA') COLLATE latin1_spanish_ci NOT NULL COMMENT 'Admisibilidad o Liquidacion voluntaria',
							  DEUDA_INFORMADA_SIR double unsigned NOT NULL,
							  DEUDA_CAR double unsigned NOT NULL,
							  DEUDA_BANCO double unsigned NOT NULL,
							  ESTADO_BOLETIN varchar(80) COLLATE latin1_spanish_ci NOT NULL COMMENT 'Estado que aparece en en voletin concursal',
							  FECHA_PRIMERA_AUDIENCIA date NOT NULL,
							  HORA_PRIMERA_AUDIENCIA time NOT NULL,
							  FECHA_RENE date NOT NULL,
							  HORA_RENE time NOT NULL,
							  DIRECCION_AUDIENCIA varchar(100) COLLATE latin1_spanish_ci NOT NULL,
							  CIUDAD varchar(20) COLLATE latin1_spanish_ci NOT NULL,
							  OPERACION_BANCO double NOT NULL,
							  OPERACION_CAR varchar(20) COLLATE latin1_spanish_ci NOT NULL,
							  ACUERDO_ESTADO_FIRMA enum('FIRMADO','SIN FIRMAR','') COLLATE latin1_spanish_ci NOT NULL DEFAULT 'SIN FIRMAR',
							  ACUERDO_PRIMER_VECTO date NOT NULL,
							  GLOSA_OBS varchar(1000) COLLATE latin1_spanish_ci NOT NULL,
							  PRIMARY KEY (RUT_CLIENTE)
							) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;");
			
						
			foreach ($lineas as $linea_num => $linea)
			
			{
				$datos = explode("	",$linea);
				$RUT_CLIENTE = trim($datos[0]);
				$NOMBRE_CLIENTE = trim($datos[1]);
				$ROL = trim($datos[2]);
				$FECHA_ADMISIBILIDAD = trim($datos[3]);
				$TIPO_AUDIENCIA = trim($datos[4]);
				$DEUDA_INFORMADA_SIR = trim($datos[5]);
				$DEUDA_CAR = trim($datos[6]);
				$DEUDA_BANCO = trim($datos[7]);
				$ESTADO_BOLETIN = trim($datos[8]);
				$FECHA_PRIMERA_AUDIENCIA = trim($datos[9]);
				$HORA_PRIMERA_AUDIENCIA = trim($datos[10]);
				$FECHA_RENE = trim($datos[11]);
				$HORA_RENE = trim($datos[12]);
				$DIRECCION_AUDIENCIA = trim($datos[13]);
				$CIUDAD = trim($datos[14]);
				$OPERACION_BANCO = trim($datos[15]);
				$OPERACION_CAR = trim($datos[16]);
				$ACUERDO_ESTADO_FIRMA = trim($datos[17]);
				$ACUERDO_PRIMER_VECTO = trim($datos[18]);
				$GLOSA_OBS = trim($datos[19]);
			
	    		$consulta = "INSERT INTO tbltransit (
							RUT_CLIENTE,
							NOMBRE_CLIENTE,
							ROL,
							FECHA_ADMISIBILIDAD,
							TIPO_AUDIENCIA,
							DEUDA_INFORMADA_SIR,
							DEUDA_CAR,
							DEUDA_BANCO,
							ESTADO_BOLETIN,
							FECHA_PRIMERA_AUDIENCIA,
							HORA_PRIMERA_AUDIENCIA,
							FECHA_RENE,
							HORA_RENE,
							DIRECCION_AUDIENCIA,
							CIUDAD,
							OPERACION_BANCO,
							OPERACION_CAR,
							ACUERDO_ESTADO_FIRMA,
							ACUERDO_PRIMER_VECTO,
							GLOSA_OBS
						)
						VALUES
							(
								'$RUT_CLIENTE',
								'$NOMBRE_CLIENTE',
								'$ROL',
								'$FECHA_ADMISIBILIDAD',
								'$TIPO_AUDIENCIA',
								'$DEUDA_INFORMADA_SIR',
								'$DEUDA_CAR',
								'$DEUDA_BANCO',
								'$ESTADO_BOLETIN',
								'$FECHA_PRIMERA_AUDIENCIA',
								'$HORA_PRIMERA_AUDIENCIA',
								'$FECHA_RENE',
								'$HORA_RENE',
								'$DIRECCION_AUDIENCIA',
								'$CIUDAD',
								'$OPERACION_BANCO',
								'$OPERACION_CAR',
								'$ACUERDO_ESTADO_FIRMA',
								'$ACUERDO_PRIMER_VECTO',
								'$GLOSA_OBS'
							);";			
				if(!$conexion->query($consulta)){			
					$respuesta->estado = false;
					$respuesta->mensaje .= "Los datos no se guardaron correctamente, verifique que la información a cargar cumpla con los formatos \n"; 				
				}
			}
		}
		
		$conexion->query("INSERT INTO clientes (
						RUT_CLIENTE,
						NOMBRE_CLIENTE,
						ROL,
						FECHA_ADMISIBILIDAD,
						TIPO_AUDIENCIA,
						DEUDA_INFORMADA_SIR,
						DEUDA_CAR,
						DEUDA_BANCO,
						ESTADO_BOLETIN,
						FECHA_PRIMERA_AUDIENCIA,
						HORA_PRIMERA_AUDIENCIA,
						FECHA_RENE,
						HORA_RENE,
						DIRECCION_AUDIENCIA,
						CIUDAD,
						OPERACION_BANCO,
						OPERACION_CAR,
						ACUERDO_ESTADO_FIRMA,
						ACUERDO_PRIMER_VECTO,
						GLOSA_OBS)
					SELECT
						tbltransit.RUT_CLIENTE,
						tbltransit.NOMBRE_CLIENTE,
						tbltransit.ROL,
						tbltransit.FECHA_ADMISIBILIDAD,
						tbltransit.TIPO_AUDIENCIA,
						tbltransit.DEUDA_INFORMADA_SIR,
						tbltransit.DEUDA_CAR,
						tbltransit.DEUDA_BANCO,
						tbltransit.ESTADO_BOLETIN,
						tbltransit.FECHA_PRIMERA_AUDIENCIA,
						tbltransit.HORA_PRIMERA_AUDIENCIA,
						tbltransit.FECHA_RENE,
						tbltransit.HORA_RENE,
						tbltransit.DIRECCION_AUDIENCIA,
						tbltransit.CIUDAD,
						tbltransit.OPERACION_BANCO,
						tbltransit.OPERACION_CAR,
						tbltransit.ACUERDO_ESTADO_FIRMA,
						tbltransit.ACUERDO_PRIMER_VECTO,
						tbltransit.GLOSA_OBS
					FROM
						tbltransit
					ON DUPLICATE KEY UPDATE
						clientes.NOMBRE_CLIENTE = VALUES (clientes.NOMBRE_CLIENTE),
						clientes.ROL = VALUES (clientes.ROL),
						clientes.FECHA_ADMISIBILIDAD = VALUES (clientes.FECHA_ADMISIBILIDAD),
						clientes.TIPO_AUDIENCIA = VALUES (clientes.TIPO_AUDIENCIA),
						clientes.DEUDA_INFORMADA_SIR = VALUES (clientes.DEUDA_INFORMADA_SIR),
						clientes.DEUDA_CAR = VALUES (clientes.DEUDA_CAR),
						clientes.DEUDA_BANCO = VALUES (clientes.DEUDA_BANCO),
						clientes.ESTADO_BOLETIN = VALUES (clientes.ESTADO_BOLETIN),
						clientes.FECHA_PRIMERA_AUDIENCIA = VALUES (clientes.FECHA_PRIMERA_AUDIENCIA),
						clientes.HORA_PRIMERA_AUDIENCIA = VALUES (clientes.HORA_PRIMERA_AUDIENCIA),
						clientes.FECHA_RENE = VALUES (clientes.FECHA_RENE),
						clientes.HORA_RENE = VALUES (clientes.HORA_RENE),
						clientes.DIRECCION_AUDIENCIA = VALUES (clientes.DIRECCION_AUDIENCIA),
						clientes.CIUDAD = VALUES (clientes.CIUDAD),
						clientes.OPERACION_BANCO = VALUES (clientes.OPERACION_BANCO),
						clientes.OPERACION_CAR = VALUES (clientes.OPERACION_CAR),
						clientes.ACUERDO_ESTADO_FIRMA = VALUES (clientes.ACUERDO_ESTADO_FIRMA),
						clientes.ACUERDO_PRIMER_VECTO = VALUES (clientes.ACUERDO_PRIMER_VECTO),
						clientes.GLOSA_OBS = VALUES (clientes.GLOSA_OBS);");
				
		if($respuesta->estado == true)
			$respuesta->mensaje = "Todos los registros se guardaron correctamente\n";
		//printf("Affected rows (INSERT): %d\n", $conexion->affected_rows);
	}
	else {
		$respuesta->mensaje = "Solo se admiten archivos .txt, vuelva a intentarlo\n";
	}
	
	$conexion->query("drop table if exists tbltransit;");
	
	echo json_encode($respuesta);
	
?>