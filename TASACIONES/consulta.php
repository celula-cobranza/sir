<?php
require('clases/cliente.class.php');
$objCliente=new Cliente;
$consulta=$objCliente->mostrar_clientes();
?>
<script type="text/javascript">
$(document).ready(function(){
	// mostrar formulario de actualizar datos
	$("table tr .modi a").click(function(){
		$('#tabla').hide();
		$("#formulario").show();
		$.ajax({
			url: this.href,
			type: "GET",
			success: function(datos){
				$("#formulario").html(datos);
			}
		});
		return false;
	});
	
	// llamar a formulario nuevo
	$("#nuevo a").click(function(){
		$("#formulario").show();
		$("#tabla").hide();
		$.ajax({
			type: "GET",
			url: 'nuevo.php',
			success: function(datos){
				$("#formulario").html(datos);
			}
		});
		return false;
	});
});

</script>
<span id="nuevo"><a href="nuevo.php"><img src="img/add.png" alt="Agregar dato" /></a></span>
<form action="buscar.php" method="post">
Buscar: <input name="palabra">
<input type="submit" name="buscador" value="Buscar">
</form>
	<table width="2000">
   		<tr>
   			<th width="17"></th>
            <th width="19"></th>
			<th width="200">Rut</th>
			<th width="264">Nombre</th>
			<th width="121">Rol</th>
			<th width="96">Tipo Vivienda</th>
			<th width="271">Estado Vivienda</th>
			<th width="109">Superficie Terreno (M2)</th>
			<th width="165">Superficie Construccion (M2)</th>
			<th width="159">Empresa</th>
			<th width="186">Valor (UF)</th>
            <th width="245">Tasador</th>
            <th width="51">Valor ($)</th>
            <th width="51">Tipo Subsidio</th>



      </tr>
<?php
if($consulta) {
	while( $cliente = mysql_fetch_array($consulta) ){
	?>
	
		 <tr id="fila-<?php echo $cliente['C_RUT'] ?>">
				<td><span class="modi"><a href="actualizar.php?id=<?php echo $cliente['C_RUT'] ?>"><img src="img/database_edit.png" title="Editar" alt="Editar" /></a></span></td>
				<td><span class="dele"><a onClick="EliminarDato(<?php echo $cliente['C_RUT'] ?>); return false" href="eliminar.php?id=<?php echo $cliente['C_RUT'] ?>"><img src="img/delete.png" title="Eliminar" alt="Eliminar" /></a></span></td>
				<td><?php echo $cliente['C_RUT'] ?></td>
				<td><?php echo $cliente['C_NOMBRE'] ?></td>
				<td><?php echo $cliente['C_ROL'] ?></td>
				<td><?php echo $cliente['C_TIPO'] ?></td>
				<td><?php echo $cliente['C_ESTADO'] ?></td>
				<td><?php echo $cliente['C_SUPTERRENO'] ?></td>
				<td><?php echo $cliente['C_SUPCONSTRUCCION'] ?></td>
				<td><?php echo $cliente['C_EMPRESA'] ?></td>
				<td><?php echo $cliente['C_VALORUF'] ?></td>
                <td><?php echo $cliente['C_TASADOR'] ?></td>
                <td><?php echo $cliente['C_VALORPESO'] ?></td>
                <td><?php echo $cliente['C_CLASE'] ?></td>

	  </tr>
	<?php
	}
}
?>
</table>