<?php 
include_once("conexion.class.php");

class Cliente{
 //constructor
 	var $con;
 	function Cliente(){
 		$this->con=new DBManager;
 	}

	function insertar($campos){
		if($this->con->conectar()==true){
			//print_r($campos);
			//echo "INSERT INTO cliente (nombres, ciudad, sexo, telefono, fecha_nacimiento) VALUES ('".$campos[0]."', '".$campos[1]."','".$campos[2]."','".$campos[3]."','".$campos[4]."')";
			return mysql_query("
			INSERT INTO TB_CLIENTE (
			C_RUT,
			C_NOMBRE,
			C_TELEFONO,
			C_CORREO,
			C_ESTADOCIVIL,
			C_SEXO,
			C_RGION,
			C_CIUDA,
			C_COMU,
			C_CALLE,
			C_ACTIVO)
			VALUES (
			'".$campos[0]."', 
			'".$campos[1]."',
			'".$campos[2]."',
			'".$campos[3]."',
			'".$campos[4]."',
			'".$campos[5]."',
			'".$campos[6]."',
			'".$campos[7]."',
			'".$campos[8]."',
			'".$campos[9]."',
			'".$campos[10]."')");
		}
	}
	
	function actualizar($campos,$id){
		if($this->con->conectar()==true){
			//print_r($campos);
			return mysql_query("
			UPDATE TB_CLIENTE
			SET
			C_RUT = '".$campos[0]."',
			C_NOMBRE = '".$campos[1]."',
			C_TELEFONO = '".$campos[2]."',
			C_CORREO = '".$campos[3]."',
			C_ESTADOCIVIL = '".$campos[4]."',
			C_SEXO = '".$campos[5]."'
			C_RGION = '".$campos[6]."'
			C_CIUDA = '".$campos[7]."'
			C_COMU = '".$campos[8]."'
			C_CALLE = '".$campos[9]."'
			C_ACTIVO = '".$campos[10]."'
			WHERE C_RUT =".$id);
		}
	}
	
	
	function mostrar_cliente($id){
		if($this->con->conectar()==true){
			return mysql_query("
			SELECT * 
			FROM TB_CLIENTE
			WHERE C_RUT =".$id);
		}
	}

	function mostrar_clientes(){
		if($this->con->conectar()==true){
			return mysql_query("
			SELECT a.C_RUT, a.C_NOMBRE, b.C_ROL, b.C_TIPO, b.C_ESTADO, b.C_SUPTERRENO, b.C_SUPCONSTRUCCION, c.C_EMPRESA, c.C_VALORUF, c.C_TASADOR, c.C_TASADOR, c.C_VALORPESO, d.C_CLASE
			FROM TB_CLIENTE a inner join TB_PROPIEDAD b
			on a.C_RUT=b.C_RUT inner join TB_TASACION c
			on b.C_ROL=c.C_ROL inner join TB_SUBSIDIO d
			on b.C_ROL=d.C_ROL
			ORDER BY a.C_RUT DESC");
		}
	}
	
	function buscar_clientes($id){
		if($this->con->conectar()==true){
			return mysql_query("
			SELECT a.C_RUT, a.C_NOMBRE, b.C_ROL, b.C_TIPO, b.C_ESTADO, b.C_SUPTERRENO, b.C_SUPCONSTRUCCION, c.C_EMPRESA, c.C_VALORUF, c.C_TASADOR, c.C_TASADOR, c.C_VALORPESO, d.C_CLASE
			FROM TB_CLIENTE a inner join TB_PROPIEDAD b
			on a.C_RUT=b.C_RUT inner join TB_TASACION c
			on b.C_ROL=c.C_ROL inner join TB_SUBSIDIO d
			on b.C_ROL=d.C_ROL
			WHERE a.C_RUT like '%$id%'");
		}
	}
	

	function eliminar($id){
		if($this->con->conectar()==true){
			return mysql_query("
			DELETE 
			FROM TB_CLIENTE 
			WHERE C_RUT =".$id);
		}
	}
}
?>