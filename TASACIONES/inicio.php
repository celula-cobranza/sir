<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>BANCO RIPLEY - TASACIONES</title>
	
	<!-- Icono BR -->
	<link href="favicon.ico" type="image/x-icon" rel="shortcut icon" />
	
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/small-business.css" rel="stylesheet">

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                    <img src="Banco-ripley.jpg" alt="">
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="inicio.php">Inicio</a>
                    </li>
                    <li>
                        <a href="resumen.php">Resumen</a>
                    </li>
                    <li>
                        <a href="inicio.php">Cliente</a>
                    </li>

					
                </ul>
				<ul class="nav navbar-nav navbar-right">
				
					<li>
                        <a href="index.php" <font color= "#FF0101" > <b>Salir</b> </font></a>
                    </li>
				</ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

        <!-- Page Content -->
    <div class="container">

        <!-- Heading Row -->
        <div class="row">
            <div class="col-md-8">
                <img src="poder-judicial.jpg" alt="" width="552" height="272" class="img-responsive img-rounded">
            </div>
            <!-- /.col-md-8 -->
            <div class="col-md-4">
                <h1>Tasaciones</h1>
              <p>La tasación es un cálculo realizado por el “Tasador” para obtener una aproximación lo más cercana a la realidad posible valor comercial de una propiedad.</p>
                <p> El tasador es un profesional calificado, que mediante su conocimiento del mercado inmobiliario, del valor del tipo de construcción y otras apreciaciones, determina el valor comercial de una propiedad.
                  
                  Las entidades financieras, se fijan en el valor de tasación antes de otorgar el monto de crédito solicitado. </p>
                <p>Esto último, se debe a que deben asegurarse, en caso que el deudor no cumpla, que la hipoteca constituida sobre la propiedad, sea cubierta por el efectivo valor de la misma.
                  
                  Es decir, el banco difícilmente prestará más de lo que la propiedad pueda costar.</p>
                
          </div>
            <!-- /.col-md-4 -->
        </div>
        <!-- /.row -->

        <hr>

        <!-- Content Row -->
        <div class="row"><!-- /.col-md-4 --><!-- /.col-md-4 --><!-- /.col-md-4 -->
        </div>
        <!-- /.row -->

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Banco Ripley 2016</p>
                </div>
            </div>
        </footer>

    </div>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>