	function ActualizarDatos(){
		var ID_AUDIENCIA = $('#ID_AUDIENCIA').attr('value');
		var FECHA_AUDIENCIA = $('#FECHA_AUDIENCIA').attr('value');
		var HORA_AUDIENCIA = $('#HORA_AUDIENCIA').attr('value');
		var IDSUBAUDIENCIA = $("input[@name='IDSUBAUDIENCIA']:checked").attr("value");
		var RUT_CLIENTE = $("#RUT_CLIENTE").attr("value");
		var CIUDAD = $("#CIUDAD").attr("value");
		var RUT_ABOGADO = $("#RUT_ABOGADO").attr("value");
		var ASISTENCIA = $("#ASISTENCIA").attr("value");

		$.ajax({
			url: 'actualizar.php',
			type: "POST",
			data: "submit=&FECHA_AUDIENCIA="+FECHA_AUDIENCIA+"&HORA_AUDIENCIA="+HORA_AUDIENCIA+"&IDSUBAUDIENCIA="+IDSUBAUDIENCIA+"&RUT_CLIENTE="+RUT_CLIENTE+"&CIUDAD="+CIUDAD+"&RUT_ABOGADO="+RUT_ABOGADO+"&ASISTENCIA="+ASISTENCIA+"&ID_AUDIENCIA="+ID_AUDIENCIA,
			success: function(datos){
				alert(datos);
				ConsultaDatos();
				$("#formulario").hide();
				$("#tabla").show();
			}
		});
		return false;
	}
	
	function ConsultaDatos(){
		$.ajax({
			url: 'consulta.php',
			cache: false,
			type: "GET",
			success: function(datos){
				$("#tabla").html(datos);
			}
		});
	}
	
	function EliminarDato(ID_AUDIENCIA){
		var msg = confirm("Desea eliminar este registro?")
		if ( msg ) {
			$.ajax({
				url: 'eliminar.php',
				type: "GET",
				data: "id="+ID_AUDIENCIA,
				success: function(datos){
					alert(datos);
					$("#fila-"+ID_AUDIENCIA).remove();
					window.location.reload();
				}
			});
		}
		return false;
	}
	
	function GrabarDatos(){
		var FECHA_AUDIENCIA = $('#FECHA_AUDIENCIA').attr('value');
		var HORA_AUDIENCIA = $('#HORA_AUDIENCIA').attr('value');
		var IDSUBAUDIENCIA = $("input[@name='IDSUBAUDIENCIA']:checked").attr("value");
		var RUT_CLIENTE = $("#RUT_CLIENTE").attr("value");
		var CIUDAD = $("#CIUDAD").attr("value");
		var RUT_ABOGADO = $("#RUT_ABOGADO").attr("value");
		var ASISTENCIA = $("#ASISTENCIA").attr("value");

		$.ajax({
			url: 'nuevo.php',
			type: "POST",
			data: "submit=&FECHA_AUDIENCIA="+FECHA_AUDIENCIA+"&HORA_AUDIENCIA="+HORA_AUDIENCIA+"&IDSUBAUDIENCIA="+IDSUBAUDIENCIA+"&RUT_CLIENTE="+RUT_CLIENTE+"&CIUDAD="+CIUDAD+"&RUT_ABOGADO="+RUT_ABOGADO+"&ASISTENCIA="+ASISTENCIA,
			success: function(datos){
				ConsultaDatos();
				alert(datos);
				$("#formulario").hide();
				$("#tabla").show();
			}
		});
		return false;
	}

	function Cancelar(){
		$("#formulario").hide();
		$("#tabla").show();
		return false;
	}
	
	// funciones del calendario
	function update_calendar(){
		var month = $('#calendar_mes').attr('value');
		var year = $('#calendar_anio').attr('value');
	
		var valores='month='+month+'&year='+year;
	
		$.ajax({
			url: 'calendario.php',
			type: "GET",
			data: valores,
			success: function(datos){
				$("#calendario_dias").html(datos);
			}
		});
	}
	
	function set_date(date){
		$('#FECHA_ADMISIBILIDAD').attr('value',date);
		show_calendar();
	}
	
	function show_calendar(){
		$('#calendario').toggle();
	}
	