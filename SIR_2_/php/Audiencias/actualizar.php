<?php
require('functions.php');
if(isset($_POST['submit'])){
	require('clases/cliente.class.php');
	$objCliente=new Cliente;
	
	$ID_AUDIENCIA = htmlspecialchars(trim($_POST['ID_AUDIENCIA']));
	$HORA_AUDIENCIA = htmlspecialchars(trim($_POST['HORA_AUDIENCIA']));
	$FECHA_AUDIENCIA = htmlspecialchars(trim($_POST['FECHA_AUDIENCIA']));
	$IDSUBAUDIENCIA = htmlspecialchars(trim($_POST['IDSUBAUDIENCIA']));
	$RUT_CLIENTE = htmlspecialchars(trim($_POST['RUT_CLIENTE']));
	$CIUDAD = htmlspecialchars(trim($_POST['CIUDAD']));
	$RUT_ABOGADO = htmlspecialchars(trim($_POST['RUT_ABOGADO']));
	$ASISTENCIA = htmlspecialchars(trim($_POST['ASISTENCIA']));
	
	if ( $objCliente->actualizar(array(
	$FECHA_AUDIENCIA,
	$HORA_AUDIENCIA,
	$IDSUBAUDIENCIA,
	$RUT_CLIENTE,
	$CIUDAD,
	$RUT_ABOGADO,
	$ASISTENCIA),
	$ID_AUDIENCIA) == true){
	
	echo 'Datos actualizados';}
	
	else{
		echo 'Se produjo un error. Intente nuevamente';
	} 
}else{
	if(isset($_GET['id'])){
		
		require('clases/cliente.class.php');
		$objCliente=new Cliente;
		$consulta = $objCliente->mostrar_cliente($_GET['id']);
		$cliente = mysql_fetch_array($consulta);
	?>
	<form id="frmClienteActualizar" name="frmClienteActualizar" method="post" action="actualizar.php" onsubmit="ActualizarDatos(); return false">
    	
		<p align="center"> 
		<strong>Modificar Audiencia SIR
		<br><br>
		</strong>
		</p> 	
		
		<input type="hidden" name="ID_AUDIENCIA" id="ID_AUDIENCIA" value="<?php echo $cliente['ID_AUDIENCIA']?>" />
       <p>
		<label>FECHA AUDIENCIA<br />
		<input class="text" type="date" name="FECHA_AUDIENCIA" id="FECHA_AUDIENCIA" value="<?php echo $cliente['FECHA_AUDIENCIA']?>" />
		</label>
	  </p>
	  
	  <p>
		<label>HORA AUDIENCIA<br />
		<input class="text" type="time" name="HORA_AUDIENCIA" id="HORA_AUDIENCIA" value="<?php echo $cliente['HORA_AUDIENCIA']?>" />
		</label>
	  </p>
	  
	  <p>
		<label>TIPO AUDIENCIA<br />
		<input type="radio" name="IDSUBAUDIENCIA" id="EJECUCION" value="4" <?php if($cliente['IDSUBAUDIENCIA']=="4") echo "checked=\"checked\""?> />
		EJECUCION</label><br>
		<label>
		<input type="radio" name="IDSUBAUDIENCIA" id="LIQUIDACION VOLUNTARIA" value="3" <?php if($cliente['IDSUBAUDIENCIA']=="3") echo "checked=\"checked\""?> />
		LIQUIDACION VOLUNTARIA</label><br>
		<label>
		<input type="radio" name="IDSUBAUDIENCIA" id="RECONOCIMIENTO PASIVO" value="1" <?php if($cliente['IDSUBAUDIENCIA']=="1") echo "checked=\"checked\""?> />
		RECONOCIMIENTO PASIVO</label><br>
		<label>
		<input type="radio" name="IDSUBAUDIENCIA" id="RENEGOCIACION" value="2" <?php if($cliente['IDSUBAUDIENCIA']=="2") echo "checked=\"checked\""?> />
		RENEGOCIACION</label><br>
	  </p>
	  <p>
		<label>RUT CLIENTE (SIN DIGITO VERIFICADOR)<br />
		<input class="text" type="text" name="RUT_CLIENTE" id="RUT_CLIENTE" value="<?php echo $cliente['RUT_CLIENTE']?>" / disabled>
		</label>
	  </p>
	  <p>
		<label>NOMBRE CLIENTE<br />
		<input class="text" type="text" name="NOMBRE_CLIENTE" id="NOMBRE_CLIENTE" value="<?php echo $cliente['NOMBRE_CLIENTE']?>" / disabled>
		</label>
	  </p>
	  <p>
		<label>CIUDAD<br />
		<input class="text" type="text" name="CIUDAD" id="CIUDAD" value="<?php echo $cliente['CIUDAD']?>" />
		</label>
	  </p>
	  <p>
		<label>RUT ABOGADO (SIN DIGITO VERIFICADOR)<br />
		<input class="text" type="text" name="RUT_ABOGADO" id="RUT_ABOGADO" value="<?php echo $cliente['RUT_ABOGADO']?>" />
		</label>
	  </p>
	 
	  <p>
		<label>ASISTENCIA<br />
		<select class="selectpicker" name="ASISTENCIA" data-style="btn-primary" id="ASISTENCIA" >
		<option><?php echo $cliente['ASISTENCIA']?></option>
		<option>ASISTE</option>
		<option>NO ASISTE</option>

		</select>
		</label>
	  </p>

	  <p>
		<input type="submit" name="submit" id="button" value="Actualizar Datos" />
		<label></label>
		<input type="button" name="cancelar" id="cancelar" value="Cancelar" onclick="Cancelar()" />
	  </p>
	</form>
	<?php
	}
}
?>