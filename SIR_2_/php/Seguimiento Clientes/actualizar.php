<?php
require('functions.php');
if(isset($_POST['submit'])){
	require('clases/cliente.class.php');
	$objCliente=new Cliente;
	
	$RUT_CLIENTE = htmlspecialchars(trim($_POST['RUT_CLIENTE']));
	$CONTROL_ESTADO = htmlspecialchars(trim($_POST['CONTROL_ESTADO']));
	$GLOSA_OBSERVACION = htmlspecialchars(trim($_POST['GLOSA_OBSERVACION']));
	
	if ( $objCliente->actualizar(array(
	$CONTROL_ESTADO,
	$GLOSA_OBSERVACION),
	$RUT_CLIENTE) == true){
	
	echo 'Datos actualizados';}
	
	else{
		echo 'Se produjo un error. Intente nuevamente';
	} 
}else{
	if(isset($_GET['id'])){
		
		require('clases/cliente.class.php');
		$objCliente=new Cliente;
		$consulta = $objCliente->mostrar_cliente($_GET['id']);
		$cliente = mysql_fetch_array($consulta);
	?>
	<form id="frmClienteActualizar" name="frmClienteActualizar" method="post" action="actualizar.php" onsubmit="ActualizarDatos(); return false">
    	
		<p align="center"> 
		<strong>Actualizar Caso SIR
		<br><br>
		</strong>
		</p> 	

	  <p>
		<label>NOMBRE CLIENTE<br />
		<input class="text" type="text" name="NOMBRE_CLIENTE" id="NOMBRE_CLIENTE" value="<?php echo $cliente['NOMBRE_CLIENTE']?>" /disabled >
		</label>
	  </p>
		<p>
		<label>RUT CLIENTE (SIN DIGITO VERIFICADOR)<br />
		<input type="text" name="RUT_CLIENTE" id="RUT_CLIENTE" value="<?php echo $cliente['RUT_CLIENTE']?>" /disabled>
		</label>
	  </p>
		
	  <p>
		<label>CONTROL ESTADO<br />
		<select class="selectpicker" name="CONTROL_ESTADO" data-style="btn-primary" id="CONTROL_ESTADO" >
		<option><?php echo $cliente['CONTROL_ESTADO']?></option>
		<option>ACUERDO FIRMADO</option>
		<option>ACUERDO PENDIENTE FIRMA</option>
		<option>EJECUCION PENDIENTE</option>
		<option>EJECUCION TERMINADA</option>
		<option>EN TRAMITE</option>
		<option>NO HAY ACUERDO</option>
		<option>ADMISIBILIDAD REVOCADA</option>
		<option>SIN DEUDA</option>
		<option>LIQUIDACION VOL TERMINADA</option>
		<option>LIQUIDACION VOL EN TRAMITE</option>
		<option>CUANTIA INSUFICIENTE</option>
		</select>
		</label>
	  </p>
	  
	  <p>
		<label>GLOSA OBSERVACION<br />
		<textarea name="GLOSA_OBSERVACION" rows="20" cols="65" type="text" id="GLOSA_OBSERVACION" ><?php echo $cliente['GLOSA_OBSERVACION']?></textarea>
		</label>
	  </p>

	  <p>
		<input type="submit" name="submit" id="button" value="Actualizar Datos" />
		<label></label>
		<input type="button" name="cancelar" id="cancelar" value="Cancelar" onclick="Cancelar()" />
	  </p>
	</form>
	<?php
	}
}
?>