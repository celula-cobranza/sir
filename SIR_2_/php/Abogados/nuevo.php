<?php
require('functions.php');
if(isset($_POST['submit'])){
	require('clases/cliente.class.php');

	$RUT_ABOGADO = htmlspecialchars(trim($_POST['RUT_ABOGADO']));
	$DV = htmlspecialchars(trim($_POST['DV']));
	$NOMBRE = htmlspecialchars(trim($_POST['NOMBRE']));
	$V_RECON_PASIVO = htmlspecialchars(trim($_POST['V_RECON_PASIVO']));
	$V_RENEGOCIACION = htmlspecialchars(trim($_POST['V_RENEGOCIACION']));
	$V_EJECUCION = htmlspecialchars(trim($_POST['V_EJECUCION']));
	$V_LIQ_VOLUNTARIA = htmlspecialchars(trim($_POST['V_LIQ_VOLUNTARIA']));
	$CIUDAD = htmlspecialchars(trim($_POST['CIUDAD']));
	
	$objCliente=new Cliente;
	if ( $objCliente->insertar(array(
	$RUT_ABOGADO,
	$DV,
	$NOMBRE,
	$V_RECON_PASIVO,
	$V_RENEGOCIACION,
	$V_EJECUCION,
	$V_LIQ_VOLUNTARIA,
	$CIUDAD)) == true)
	{
		echo 'Datos guardados';
	}else{
		echo 'Se produjo un error. Intente nuevamente';
	} 
}else{
?>

<form id="frmClienteNuevo" name="frmClienteNuevo" method="post" action="nuevo.php" onsubmit="GrabarDatos(); return false">
  
		<p align="center"> 
		<strong>Ingresar Nuevo Abogado SIR
		<br><br>
		</strong>
		</p>
  
		<p>
		<label>RUT ABOGADO<br />
		<input class="text" type="text" name="RUT_ABOGADO" id="RUT_ABOGADO" />
		</label>
		</p>
		
		<p>
		<label>DV<br />
		<input class="text" type="text" name="DV" id="DV" />
		</label>
		</p>
		 
		<p>
		<label>NOMBRE<br />
		<input class="text" type="text" name="NOMBRE" id="NOMBRE" />
		</label>
		</p>

		<p>
		<label>VALOR RECONOCIMIENTO PASIVO<br />
		<input class="text" type="text" name="V_RECON_PASIVO" id="V_RECON_PASIVO" />
		</label>
	  </p>
	  <p>
		<label>VALOR RENEGOCIACION<br />
		<input class="text" type="text" name="V_RENEGOCIACION" id="V_RENEGOCIACION" />
		</label>
	  </p>
		
	  <p>
	  <label>VALOR EJECUCION<br />
	  <input class="text" type="text" name="V_EJECUCION" id="V_EJECUCION" />
	  </label>
	  </p>
	  
	  <p>
	  <label>VALOR LIQUIDACION VOLUNTARIA<br />
	  <input class="text" type="text" name="V_LIQ_VOLUNTARIA" id="V_LIQ_VOLUNTARIA" />
	  </label>
	  </p>
	  
	  <p>
	  <label>CIUDAD<br />
	  <input class="text" type="text" name="CIUDAD" id="CIUDAD" />
	  </label>
	  </p>

		<p>
		<input type="submit" name="submit" id="button" value="Guardar" />
		<label></label>
		<input type="button" class="cancelar" name="cancelar" id="cancelar" value="Cancelar" onclick="Cancelar()" />
		</p>
</form>
<?php
}
?>