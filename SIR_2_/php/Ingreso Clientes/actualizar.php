<?php
require('functions.php');
if(isset($_POST['submit'])){
	require('clases/cliente.class.php');
	$objCliente=new Cliente;
	
	$RUT_CLIENTE = htmlspecialchars(trim($_POST['RUT_CLIENTE']));
	$FECHA_ADMISIBILIDAD = htmlspecialchars(trim($_POST['FECHA_ADMISIBILIDAD']));
	$TIPO_AUDIENCIA = htmlspecialchars(trim($_POST['TIPO_AUDIENCIA']));
	$ROL = htmlspecialchars(trim($_POST['ROL']));
	$DEUDA_INFORMADA_SIR = htmlspecialchars(trim($_POST['DEUDA_INFORMADA_SIR']));
	$DEUDA_BANCO = htmlspecialchars(trim($_POST['DEUDA_BANCO']));
	$DEUDA_CAR = htmlspecialchars(trim($_POST['DEUDA_CAR']));
	$FECHA_PRIMERA_AUDIENCIA = htmlspecialchars(trim($_POST['FECHA_PRIMERA_AUDIENCIA']));
	$HORA_PRIMERA_AUDIENCIA = htmlspecialchars(trim($_POST['HORA_PRIMERA_AUDIENCIA']));
	$FECHA_RENE = htmlspecialchars(trim($_POST['FECHA_RENE']));
	$HORA_RENE = htmlspecialchars(trim($_POST['HORA_RENE']));
	$DIRECCION_AUDIENCIA = htmlspecialchars(trim($_POST['DIRECCION_AUDIENCIA']));
	$CIUDAD = htmlspecialchars(trim($_POST['CIUDAD']));
	$ESTADO_BOLETIN = htmlspecialchars(trim($_POST['ESTADO_BOLETIN']));
	$OPERACION_CAR = htmlspecialchars(trim($_POST['OPERACION_CAR']));
	$OPERACION_BANCO = htmlspecialchars(trim($_POST['OPERACION_BANCO']));
	$ACUERDO_ESTADO_FIRMA = htmlspecialchars(trim($_POST['ACUERDO_ESTADO_FIRMA']));
	$ACUERDO_PRIMER_VECTO = htmlspecialchars(trim($_POST['ACUERDO_PRIMER_VECTO']));
	$GLOSA_OBS = htmlspecialchars(trim($_POST['GLOSA_OBS']));
	
	if ( $objCliente->actualizar(array(
	$FECHA_ADMISIBILIDAD,
	$TIPO_AUDIENCIA,
	$ROL,
	$DEUDA_INFORMADA_SIR,
	$DEUDA_BANCO,
	$DEUDA_CAR,
	$FECHA_PRIMERA_AUDIENCIA,
	$HORA_PRIMERA_AUDIENCIA,
	$FECHA_RENE,
	$HORA_RENE,
	$DIRECCION_AUDIENCIA,
	$CIUDAD,
	$ESTADO_BOLETIN,
	$OPERACION_CAR,
	$OPERACION_BANCO,
	$ACUERDO_ESTADO_FIRMA,
	$ACUERDO_PRIMER_VECTO,
	$GLOSA_OBS),
	$RUT_CLIENTE) == true){
	
	echo 'Datos actualizados';}
	
	else{
		echo 'Se produjo un error. Intente nuevamente';
	} 
}else{
	if(isset($_GET['id'])){
		
		require('clases/cliente.class.php');
		$objCliente=new Cliente;
		$consulta = $objCliente->mostrar_cliente($_GET['id']);
		$cliente = mysql_fetch_array($consulta);
	?>
	<form id="frmClienteActualizar" name="frmClienteActualizar" method="post" action="actualizar.php" onsubmit="ActualizarDatos(); return false">
    	
		<p align="center"> 
		<strong>Modificar Caso SIR
		<br><br>
		</strong>
		</p> 	
		
		<input type="hidden" name="RUT_CLIENTE" id="RUT_CLIENTE" value="<?php echo $cliente['RUT_CLIENTE']?>" />
       <p>
		<label>FECHA ADMISIBILIDAD<br />
		<input class="text" type="date" name="FECHA_ADMISIBILIDAD" id="FECHA_ADMISIBILIDAD" value="<?php echo $cliente['FECHA_ADMISIBILIDAD']?>" />
		</label>
	  </p>
	  <p>
		<label>TIPO AUDIENCIA<br />
		<input type="radio" name="TIPO_AUDIENCIA" id="ADMISIBILIDAD" value="ADMISIBILIDAD" <?php if($cliente['TIPO_AUDIENCIA']=="ADMISIBILIDAD") echo "checked=\"checked\""?> />
		ADMISIBILIDAD</label>
		<label>
		<input type="radio" name="TIPO_AUDIENCIA" id="LIQUIDACION VOLUNTARIA" value="LIQUIDACION VOLUNTARIA" <?php if($cliente['TIPO_AUDIENCIA']=="LIQUIDACION VOLUNTARIA") echo "checked=\"checked\""?> />
		LIQUIDACION VOLUNTARIA</label>
	  </p>
	  <p>
		<label>ROL<br />
		<input class="text" type="text" name="ROL" id="ROL" value="<?php echo $cliente['ROL']?>" />
		</label>
	  </p>
	  <p>
		<label>DEUDA INFORMADA SIR<br />
		<input class="text" type="text" name="DEUDA_INFORMADA_SIR" id="DEUDA_INFORMADA_SIR" value="<?php echo $cliente['DEUDA_INFORMADA_SIR']?>" />
		</label>
	  </p>
	  <p>
		<label>DEUDA BANCO<br />
		<input class="text" type="text" name="DEUDA_BANCO" id="DEUDA_BANCO" value="<?php echo $cliente['DEUDA_BANCO']?>" />
		</label>
	  </p>
	  <p>
		<label>DEUDA CAR<br />
		<input class="text" type="text" name="DEUDA_CAR" id="DEUDA_CAR" value="<?php echo $cliente['DEUDA_CAR']?>" />
		</label>
	  </p>
	  <p>
		<label>FECHA PRIMERA AUDIENCIA<br />
		<input class="text" type="date" name="FECHA_PRIMERA_AUDIENCIA" id="FECHA_PRIMERA_AUDIENCIA" value="<?php echo $cliente['FECHA_PRIMERA_AUDIENCIA']?>" />
		</label>
	  </p>
	  <p>
		<label>HORA PRIMERA AUDIENCIA<br />
		<input class="text" type="time" name="HORA_PRIMERA_AUDIENCIA" id="HORA_PRIMERA_AUDIENCIA" value="<?php echo $cliente['HORA_PRIMERA_AUDIENCIA']?>" />
		</label>
	  </p>
	  <p>
		<label>FECHA RENEGOCIACION<br />
		<input class="text" type="date" name="FECHA_RENE" id="FECHA_RENE" value="<?php echo $cliente['FECHA_RENE']?>" />
		</label>
	  </p>
	  <p>
		<label>HORA RENEGOCIACION<br />
		<input class="text" type="time" name="HORA_RENE" id="HORA_RENE" value="<?php echo $cliente['HORA_RENE']?>" />
		</label>
	  </p>
	  <p>
		<label>DIRECCION AUDIENCIA<br />
		<input class="text" type="text" name="DIRECCION_AUDIENCIA" id="DIRECCION_AUDIENCIA" value="<?php echo $cliente['DIRECCION_AUDIENCIA']?>" />
		</label>
	  </p>
	  <p>
		<label>CIUDAD<br />
		<input class="text" type="text" name="CIUDAD" id="CIUDAD" value="<?php echo $cliente['CIUDAD']?>" />
		</label>
	  </p>
	  <p>
		<label>ESTADO BOLETIN<br />
		<select class="selectpicker" name="ESTADO_BOLETIN" data-style="btn-primary" id="ESTADO_BOLETIN" >
		<option><?php echo $cliente['ESTADO_BOLETIN']?></option>
		<option>ACUERDO DE RENEGOCIACION</option>
		<option>ACUERDO FORMALIZADO</option>
		<option>ADM.REVOCADA</option>
		<option>ADMISIBILIDAD SIN DEUDA RIPLEY</option>
		<option>AUDIENCIA DE RENEGOCIACION</option>
		<option>AUDIENCIA DETERMINACION PASIVO</option>
		<option>FINALIZADO PROCEDIMIENTO CONCURSAL DE RENEG.</option>
		<option>LIQUID. VOL .CON DEUDA</option>
		<option>LIQUID. VOLUNTARIA</option>

		</select>
		</label>
	  </p>

	  <p>
		<label>OPERACION CAR<br />
		<input class="text" type="text" name="OPERACION_CAR" id="OPERACION_CAR" value="<?php echo $cliente['OPERACION_CAR']?>" />
		</label>
	  </p>
	  
	  <p>
		<label>OPERACION BANCO<br />
		<input class="text" type="text" name="OPERACION_BANCO" id="OPERACION_BANCO" value="<?php echo $cliente['OPERACION_BANCO']?>" />
		</label>
	  </p>
	  <p>
		<label>ACUERDO: ESTADO DE LA FIRMA<br />
		<select class="selectpicker" name="ACUERDO_ESTADO_FIRMA" data-style="btn-primary" id="ACUERDO_ESTADO_FIRMA" >
		<option><?php echo $cliente['ACUERDO_ESTADO_FIRMA']?></option>
		<option>FIRMADO</option>
		<option>SIN FIRMAR</option>
		</select>
		</label>
	  </p>
	  <p>
		<label>ACUERDO: FECHA PRIMER VENCIMIENTO<br />
		<input class="text" type="date" name="ACUERDO_PRIMER_VECTO" id="ACUERDO_PRIMER_VECTO" value="<?php echo $cliente['ACUERDO_PRIMER_VECTO']?>" />
		</label>
	  </p>
	  <p>
		<label>GLOSA OBSERVACION<br />
		<textarea name="GLOSA_OBS" rows="20" cols="65" type="text" id="GLOSA_OBS" ><?php echo $cliente['GLOSA_OBS']?></textarea>
		</label>
	  </p>
	  
	  <p>
		<input type="submit" name="submit" id="button" value="Actualizar Datos" />
		<label></label>
		<input type="button" name="cancelar" id="cancelar" value="Cancelar" onclick="Cancelar()" />
	  </p>
	</form>
	<?php
	}
}
?>